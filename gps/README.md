# gps

Satellite tracker

* Powers off 3.3V-5V. 
* Baud 9600.
* Lines terminate with \r\n (ASCII \r=13, \n=10)

Uses a DOG LCD, too. DogLcd library, and Neo6GM.

## GPRMC

Field 1: $GPRMC

Field 2: UTC of position hhmmss.ss

Field 10: dd/mm/yy



## References

* db05.168
* [gps-every](https://github.com/blippy/gps-every) - github 
* [GPGLL](https://docs.novatel.com/oem7/Content/Logs/GPGLL.htm) - decoding info
* [guide](https://randomnerdtutorials.com/guide-to-neo-6m-gps-module-with-arduino/)
