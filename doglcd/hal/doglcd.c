/*
 * doglcd.c
 *
 *  Created on: Oct 29, 2021
 *      Author: pi
 */

#include "main.h"
#include "doglcd.h"


#include <string.h>

#define CS	GPIOA, GPIO_PIN_4
#define RS	GPIOA, GPIO_PIN_6
#define SPI_PORT 1 // actually unused

extern SPI_HandleTypeDef hspi1;

// COMAPTABILITY LAYER

#define LOW RESET
#define HIGH SET

static inline void gpio_put(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, GPIO_PinState PinState)
{
	HAL_GPIO_WritePin(GPIOx, GPIO_Pin, PinState);
}

void digitalWrite(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, int val)
{
	if(val)
		gpio_put(GPIOx, GPIO_Pin, SET);
	else
		gpio_put(GPIOx, GPIO_Pin, RESET);
}

void sleep_ms(int ms)
{
	HAL_Delay(ms);
}

//spi_write_blocking(SPI_PORT, &val, 1);
int spi_write_blocking (int unused, uint8_t *src, size_t len)
{
	//HAL_SPI_Transmit(&hspi1,txData,,TIMEOUTVALUE);
	HAL_SPI_Transmit(&hspi1, src, len, 100);
	return 1;

}

// END COMAPTABILITY LAYER


/* rs_val = LOW to send command, HIGH to send ASCII char
*/
void doglcd_send_byte(int rs_val, uint8_t val)
{
	digitalWrite(RS, rs_val);
	digitalWrite(CS, LOW);
	spi_write_blocking(SPI_PORT, &val, 1);
	digitalWrite(CS, HIGH);
	//sleep_ms(60);
}

void doglcd_send_cmd(uint8_t cmd)
{
	doglcd_send_byte(LOW, cmd);
	HAL_Delay(1);
}
void doglcd_write_str(char *str)
{
	digitalWrite(RS, HIGH);
	digitalWrite(CS, LOW);
	spi_write_blocking(SPI_PORT, (unsigned char*) str, strlen(str));
	digitalWrite(CS, HIGH);

}

void doglcd_double_height(bool tall)
{
	// doesn't seem to work properly
	doglcd_send_cmd((1<<4) + (tall ? 0b1000 : 0));
}
void doglcd_home()
{
	// goes to top-left
	doglcd_send_cmd(1<<1);
}

void doglcd_cls()
{
	doglcd_send_cmd(1);
	doglcd_home();
}

// move cursor to position x,y (1st Row and 1st column is position 0,0)
// for a 163 display : x = 0..15   y = 0..2
// 3-line display assumed here. for others, see:
// https://forum.arduino.cc/t/library-for-electronic-assembly-dog-displays/36988

void doglcd_gotoxy(int x, int y) {
  int addr = 0x80;
  if (y<1) addr = 0x80+x;
  if (y==1)
	 addr = 0x90+x;
  else if (y>1) addr = 0xA0+x;
  doglcd_send_cmd(addr);
}

void doglcd_test()
{
	doglcd_cls();
	//doglcd_home();
	//doglcd_double_height(true);
	doglcd_gotoxy(0, 0);
	doglcd_write_str("LINE 0");
	doglcd_gotoxy(0, 1);
	doglcd_write_str("LINE 1");
	doglcd_gotoxy(0, 2);
	doglcd_write_str("LINE 2");
	/*
	doglcd_write_str("HELLO WORLD HOW ARE YOU?");
	doglcd_home();
	doglcd_write_str("LINE 2");
	*/
	//doglcd_double_height(true);
	//doglcd_write_str("DOUBLE HEIGHT");

	//uint8_t msg[] = {'H', 'E', 'L', 'L', 'O'};
	//for(int i=0; i<sizeof(msg); ++i) doglcd_send_byte(HIGH, msg[i]);
	sleep_ms(1000);
}


void doglcd_init()
{
	gpio_put(CS, 1);
	const int contrast = 0x70  | 0b1000; // from 0x7C
	const int display = 0b1111; // ori 0x0F
	uint8_t cmds[] = {0x39, 0x1D, 0x50, 0x6C, contrast , 0x38, display, 0x01, 0x06};
	for(int i=0; i<sizeof(cmds); ++i) doglcd_send_byte(LOW, cmds[i]);
}
