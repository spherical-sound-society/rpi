# doglcd.md

`
CubeMX:
* PA4, PA6 as output GPIO, output level high
* SPI1: Transmit Only Master. Hardware NSS Signal Disabled. Baud rate 500kBit/s is OK. Faster proved unreliable.


```
PA6 CN10:13 RS   C2
PA4 CN7:32  SS   C3
PA5 CN10:11 SCK  C4
PA7 CN10:15 MOSI C5
```

## Status

2021-10-30	Created. Working.
