import core.runtime;
import std.ascii;
import std.format;
import std.stdio;
import std.uni;

void main()
{
	string args[] = Runtime.args;
	string up = toUpper(args[1]);
	auto arr3 = ["BIN":2, "OCT":8, "DEC":10, "HEX":16 ];
	int radix  = arr3[up];

	int dec = 0; 
	string outstr;
	//char arr[] =  [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' ];
	string raw = args[2]; // input number
	foreach(c; raw) {
		int c1 = std.ascii.toUpper(c);
		if(isDigit(c1)) { 
			c1 = c1 - '0';
		} else if ('A' <= c1 && c1 <= 'F' ) {
			c1 = c1 - 'A' + 10;
		} else continue;

		if(c1 <  radix) dec = dec*radix + c1;
	}

	writeln(format("BIN %,4b", dec));
	writeln(format("OCT %o", dec));
	writeln(format("DEC %d", dec));
	writeln(format("HEX %x", dec));
	
}  
