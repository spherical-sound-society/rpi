#include "rotary.h"
#include "pi.h"


#if 0
Rotary::Rotary(uint sia, uint sib, uint sw) : m_sia(sia), m_sib(sib), m_sw(Debounce(sw))
{
	pi_gpio_init(sia, INPUT);
	pi_gpio_init(sib, INPUT);

}
#endif

void rotary_init(rotary_t* rot, int gpio_anti, int gpio_clock)
{
	pi_gpio_init(gpio_anti, INPUT_PULLUP);
        pi_gpio_init(gpio_clock, INPUT_PULLUP);

	rot->gpio_anti = gpio_anti;
	rot->gpio_clock = gpio_clock;
	rot->prev_state = 0;
	rot->cur_state = 0;
	rot->count_change = 0;
	rot->delay = 1;
	rot->later = make_timeout_time_ms(0);
}


#if 0
bool Rotary::sw_falling() { return m_sw.falling(); }
bool Rotary::sw_rising() { return m_sw.rising(); }


int Rotary::state()
{
	return (pi_gpio_is_high(m_sib) <<1) | pi_gpio_is_high(m_sia); 
}
#endif

int rotary_state(rotary_t* rot)
{
        return (pi_gpio_is_high(rot->gpio_clock) <<1) | pi_gpio_is_high(rot->gpio_anti);
}


#if 0
int Rotary::change()
{
	if(m_pulse.expired()) {
		m_cur_state = state();
		if((m_prev_state == 2) && (m_cur_state == 3)) {
			m_count_change++;
		}
		if((m_prev_state == 1) && (m_cur_state == 3)) {
			m_count_change--;
		}
		m_prev_state = m_cur_state;
	}
	auto tmp = m_count_change;
	m_count_change = 0;
	return tmp;
}
#endif

int rotary_change(rotary_t* rot)
{
	if(absolute_time_diff_us(get_absolute_time(), rot->later)<=0) {
		rot->later = make_timeout_time_ms(rot->delay);
		rot->cur_state = rotary_state(rot);
		if((rot->prev_state ==2) && ( rot->cur_state == 3))
			rot->count_change++;
		if((rot->prev_state ==1) && ( rot->cur_state == 3))
			rot->count_change--;
		rot->prev_state = rot->cur_state;
	}
	int tmp = rot->count_change;
	rot->count_change = 0;
	return tmp;
}
