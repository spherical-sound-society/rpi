#include <stdio.h>
//#include <string.h>
#include "pico/stdlib.h"
//#include "hardware/spi.h"
#include "hardware/gpio.h"

#include "debounce.h"


//#define BTN 14 // GPIO number, not physical pin
#define LED 25 // GPIO of built-in LED

void toggle()
{
	gpio_xor_mask(1<<LED); // toggle LED
}

int main() 
{
	stdio_init_all();
	puts("Debounce test");

	//Debounce button(20);
	debounce_t deb;
	debounce_init(&deb, 19, 4);


	gpio_init(LED);
	gpio_set_dir(LED, GPIO_OUT);

	int idx = 0;
	for(;;) {
		//if(button.falling()) toggle();
		if(debounce_falling(&deb)) {
			toggle();
			printf("%d: detected falling\n", idx++);
		}
		if(debounce_rising(&deb)){
		       //toggle();
			printf("%d: detected rising\n", idx++);

		}
	}

	return 0;
}

