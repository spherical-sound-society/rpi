#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
//#include "hardware/adc.h"
//#include "hardware/clocks.h"
//#include "hardware/flash.h"
#include "hardware/gpio.h"
//#include "hardware/irq.h"
//#include "hardware/pwm.h"
#include "hardware/spi.h"
// #include "tusb.h" // if you want to use tud_cdc_connected()

#include "pi.h"


#define SCK 	2
#define MOSI	3
#define CS	5

#define LED		25

int main() 
{
	stdio_init_all();
	pi_gpio_init(LED, OUTPUT);
	//pi_max7219_init();
	//pi_max7219_show_count(0);

	// set up spi
	int baud = 16'000'000; 
	spi_init(spi0, baud);
	spi_set_format(spi0, 16, SPI_CPOL_0 , SPI_CPHA_0, SPI_MSB_FIRST);
	gpio_set_function(SCK,  GPIO_FUNC_SPI);
	gpio_set_function(MOSI, GPIO_FUNC_SPI);
	gpio_set_function(CS, GPIO_FUNC_SPI);

	uint16_t count = 0;
	while(1) {
		//gpio_put(SLAVE_CS, 0);
		//uint16_t big_endian = __builtin_bswap16(count);
		spi_write16_blocking(spi0, &count, 1);
		//gpio_put(SLAVE_CS, 1);
		//count++;
		count += 4;
		//pi_gpio_toggle(LED);
		//sleep_ms(1000);
	}

}

