#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
//#include "hardware/adc.h"
//#include "hardware/clocks.h"
#include "hardware/gpio.h"
#include "hardware/irq.h"
#include "hardware/pwm.h"
//#include "hardware/spi.h"
// #include "tusb.h" // if you want to use tud_cdc_connected()
#include "math.h"

#include "pace.h"


using i32 = int32_t;

const auto TOP = 2045;
//const auto M = 64;
//const auto dm_scale = 1 << 5; // provide fractional parts
const auto f_w = 440; //frequency of the wave we want to produce
const auto f_s = 22'000; // sampling frequency; our carrier wave
static_assert(f_s *(TOP+1) < 125'000'000); // mustn't exceed clock hz
//constexpr i32 dm = dm_scale * M  * f_w / f_s; 

unsigned int slice_num;

#define SPK 13

void pwm_isr()
{
	const float pi2 = 2.0f * 3.14159;
	const float dx = pi2 * (float) f_w / (float) f_s;
	static float x = 0, y = 0;
	pwm_clear_irq(slice_num); // reset the irq so that it will fire in future

	pwm_set_gpio_level(SPK, y);

	x += dx;
	if(x>pi2) x -= pi2;
	y = (sin(x) +1.0) * TOP/2.0;


}

int main() 
{
	int err = pace_config_pwm_irq(&slice_num, SPK, f_s, TOP, pwm_isr);

	for(;;);
}

