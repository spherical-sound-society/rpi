#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
//#include "hardware/adc.h"
//#include "hardware/clocks.h"
#include "hardware/dma.h"
#include "hardware/gpio.h"
#include "hardware/timer.h"
#include "pico/time.h"
#include "hardware/gpio.h"
#include "hardware/spi.h"
// #include "tusb.h" // if you want to use tud_cdc_connected()

#include "pi.h"


//#define LED  25 // GPIO of built-in LED
#define LED1 15

#define spi             spi0

#define PIN_CS          5
#define PIN_SCK         6
#define PIN_MOSI        7

#define WORD(hi, lo) ((((uint16_t) hi) << 8) | lo)

uint chan; // for dma

void zseg_tfrn(uint16_t* data, int count)
{
	pi_gpio_high(LED1);
	dma_channel_set_trans_count(chan, count, false); // don't start yet
	dma_channel_set_read_addr(chan, data, true); // and start
	pi_gpio_low(LED1);
}

//uint16_t txbuf;

void zseg_init(void)
{


	int spi_speed = 1000 * 1000; // 1MHz OK
	spi_speed *= 10; // 10MHz also OK. Tfr time is 16us for counter. Great
	spi_init(spi, spi_speed);
	spi_set_format(spi, 16, 0, 0, SPI_MSB_FIRST);
	gpio_set_function(PIN_SCK,  GPIO_FUNC_SPI);
	gpio_set_function(PIN_MOSI, GPIO_FUNC_SPI);
	gpio_set_function(PIN_CS, GPIO_FUNC_SPI);

	chan = dma_claim_unused_channel(true);
	dma_channel_config cfg = dma_channel_get_default_config(chan);
	channel_config_set_transfer_data_size(&cfg, DMA_SIZE_16);
	channel_config_set_dreq(&cfg, spi_get_dreq(spi, true)); // yes, it's TX
	channel_config_set_write_increment(&cfg, false); // the spi register is fixed => no incrementing
	channel_config_set_read_increment(&cfg, true); // reading from a block => incrementing
	dma_channel_set_write_addr(chan, &spi_get_hw(spi)->dr, false);
	dma_channel_set_config(chan, &cfg, false);

	uint16_t setup[] = { 
		WORD(0x0F, 0x00),
		WORD(0x09, 0xFF), // Enable mode B
		WORD(0x0A, 0x0F), // set intensity (page 9)
		WORD(0x0B, 0x07), // use all pins
		WORD(0x0C, 0x01) // Turn on chip
	};
	static_assert(count_of(setup) == 5, "Wrong size");
	zseg_tfrn(setup, count_of(setup));
	dma_channel_wait_for_finish_blocking(chan);
}


void zseg_show_count(int count)
{
	uint16_t data[8];
	uint32_t num = count*1;
	for (uint8_t i = 0; i < 8; ++i)
	{
		uint8_t c = num % 10;
		num /= 10;
		uint8_t sep = 0; // thousands separator

		// add in thousands separators
		if ((i > 0) && (i % 3 == 0))
		{
			sep = 1 << 7;
		}

		// blank if end of number
		if ((c == 0) && (num == 0) && (i > 0))
		{
			sep = 0;
			c = 0b1111;
		}

		c |= sep;

		data[i] = WORD(i+1, c);
	}
	if(!dma_channel_is_busy(chan))
		zseg_tfrn(data, count_of(data));
}

int main() 
{
	//stdio_init_all();
	// while(!tud_cdc_connected()) sleep_ms(250); // wait for usb serial 

	pi_gpio_init(LED1, OUTPUT);
	zseg_init();

	uint32_t count = 0;
	while(1) {
		zseg_show_count(count++);
		//pi_gpio_toggle(LED);
		sleep_ms(1000);
	}

	return 0;
}

