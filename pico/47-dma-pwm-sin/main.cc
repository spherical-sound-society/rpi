#include <stdio.h>
#include <string.h>
//#define PARAM_ASSERTIONS_ENABLE_ALL 1
#include "pico/stdlib.h"
#include "pico/assert.h"
#include "hardware/clocks.h"
#include "hardware/gpio.h"
#include "hardware/irq.h"
#include "hardware/pwm.h"
#include "hardware/dma.h"
#include <math.h>
// #include "tusb.h" // if you want to use tud_cdc_connected()

//#include "pi.h"
//#include "pace.h"

#define SPK 15

//#define TOP 255
#define NSAMPS 256
const auto clk_hz = 125'000'000;
// 44100 will come out as 44107
// 22000 as 22003
const auto fs_raw = 0 ? 44100 : 22000;
const auto fs = clk_hz/(clk_hz/fs_raw); 
const auto fw = 440; // wave frequency
const auto TOP = 999; // clk_hz/fs-1;
static_assert(TOP <= 0xffff); // it's only a 16-bit register

unsigned int slice_num;
int dma_chan_a, dma_chan_b;

#define pinfo(x) printf("%s = %u\n", #x, x);

//volatile uint16_t val = 0;

#if 1
volatile uint16_t buf_a[NSAMPS] __attribute__((aligned(NSAMPS * sizeof(uint16_t))));
volatile uint16_t buf_b[NSAMPS] __attribute__((aligned(NSAMPS * sizeof(uint16_t))));
#else
uint16_t buf_a[NSAMPS] __attribute__((aligned(NSAMPS * sizeof(uint16_t))));
uint16_t buf_b[NSAMPS] __attribute__((aligned(NSAMPS * sizeof(uint16_t))));
#endif
//volatile uint16_t buf_a[NSAMPS];
//volatile uint16_t buf_b[NSAMPS];


constexpr uint log2(uint n)
{
	if(n>1)
		return 1 + log2(n>>1);
	else
		return 0;
}

static const uint size_bits = log2(NSAMPS * sizeof(uint16_t));  // 9, basically
static_assert((1<<size_bits) == NSAMPS * sizeof(uint16_t));

#define LEDA 10
#define LEDB 11

void gpio_toggle(uint pin)
{
	gpio_put(pin, gpio_get(pin) ? 0 : 1);
}

// From
// http://www.mclimatiano.com/faster-sine-approximation-using-quadratic-curve/
// // Low Precision version
// // doesn't seem better than normal sin

float Sin( float x )
{
	if (x < -3.14159265f)
		x += 6.28318531f;
	else if (x >  3.14159265f)
		x -= 6.28318531f;

	if ( x < 0 )
		return x * ( 1.27323954f + 0.405284735f * x );
	else
		return x * ( 1.27323954f - 0.405284735f * x );
}

void fill_buff(uint16_t* buf, uint pin)
{
	static volatile float x=0, y=0;
	const float pi2 = 6.27319;
	const float dx = pi2 * fw/fs;
	gpio_put(pin, 1);
	for(int i =0; i<NSAMPS; i++) {
		buf[i] = y;
		x+= dx;
		if(x > pi2) x-= pi2;
		//y = (Sin(x)+1.0)*TOP/2.0;
		y = (sin(x)+1.0)*TOP/2.0;
		//y = 0;
	}
	gpio_put(pin, 0);
}



void dma_handler()
{
	if(dma_hw->intr & (1u<<dma_chan_a)) { // channel a complete?
		dma_hw->ints0=1u<<dma_chan_a; // clear the interrupt request
		fill_buff((uint16_t*) buf_a, LEDA); // buf a transferred, so refill it
	}
	if(dma_hw->intr & (1u<<dma_chan_b)) { // channel b complete?
		dma_hw->ints0=1u<<dma_chan_b; // clear the interrupt request
		fill_buff((uint16_t*) buf_b, LEDB); // buf b transferred, so refill it
	}

}



void dma_setup_chan(int dma_chan, int dma_chan_chain, volatile uint16_t* buf)
{
	dma_channel_config cfg = dma_channel_get_default_config(dma_chan);
	channel_config_set_transfer_data_size(&cfg, DMA_SIZE_16);
	channel_config_set_read_increment(&cfg, true);
	channel_config_set_ring(&cfg, false, size_bits);
	channel_config_set_dreq(&cfg, DREQ_PWM_WRAP0 + slice_num); // write data at pwm frequency
	channel_config_set_chain_to(&cfg, dma_chan_chain); // start chan b when chan a completed
	//channel_config_set_irq_quiet(&cfg_a, false);
	dma_channel_configure(
			dma_chan,          // Channel to be configured
			&cfg,            // The configuration we just created
			//(volatile void*) write_address,           // The initial write address
			&pwm_hw->slice[slice_num].cc, // write address
			buf,           // The initial read address
			NSAMPS, // Number of transfers
			false           // Start immediately?
			);
}

int main() 
{
	stdio_init_all();
	// while(!tud_cdc_connected()) sleep_ms(250); // wait for usb serial 
	puts("\nwsine wave via dma and pwm and chaining");

	const int pwm_chan = pwm_gpio_to_channel(SPK);
	pinfo(pwm_chan); 

	auto led_mask =  (1<<LEDA) | (1<<LEDB);
	gpio_init_mask(led_mask);
	gpio_set_dir_out_masked(led_mask);

	// set up pwm
	pinfo(PWM_CH0_CC_A_LSB);
	pinfo(PWM_CH0_CC_B_LSB);
	//const int freq = 400 * NSAMPS; // 500Hz sawtooth
	//int status = pace_config_pwm(&slice_num, SPK, freq,255);
	gpio_set_function(SPK, GPIO_FUNC_PWM);
	slice_num = pwm_gpio_to_slice_num(SPK);
	//float divider = (float) clock_get_hz(clk_sys) /(TOP+1)/freq;
	int divider = clk_hz /(TOP+1)/fs;
	assert(clock_get_hz(clk_sys) == clk_hz);
	printf("divider=%f\n", divider); // comes out at 3
	assert(1.0f <= divider && divider < 256.0f);
	//pwm_set_clkdiv(slice_num, divider);
	pwm_set_clkdiv_int_frac(slice_num, divider, 0);
	pwm_set_wrap(slice_num, TOP);
	pwm_set_enabled(slice_num, true);
	//pwm_set_enabled(slice_num, false); // does disabling help? No.
	pinfo(slice_num); // apparently SPK 19 is slice 1
	//printf("GPIO on consistent channel?: %s\n", pwm_gpio_to_channel(SPK)  == 0 ? "GOOD" : "BAD");
	pwm_set_irq_enabled(slice_num, true); // Necessary? Yes

	// set up DMA ping-pong buffers
	dma_chan_a = dma_claim_unused_channel(true);
	dma_chan_b = dma_claim_unused_channel(true);
	dma_setup_chan(dma_chan_a, dma_chan_b, buf_a);
	dma_setup_chan(dma_chan_b, dma_chan_a, buf_b);
	irq_set_exclusive_handler(DMA_IRQ_0,dma_handler);
	dma_set_irq0_channel_mask_enabled((1<<dma_chan_a) | (1<<dma_chan_b), true);
	irq_set_enabled(DMA_IRQ_0,true);
	dma_channel_start(dma_chan_a); // seems to start something

	while(1) {
		tight_loop_contents();
	}
}

