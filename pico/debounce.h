#pragma once

#include "pico/time.h"

#ifdef __cplusplus
extern "C" {
#endif

#if 0
class Debounce {
	public:
		Debounce(uint gpio, uint delay = 4);
		bool falling();
		bool rising();
	private:
		void update();
		int _gpio;
		uint8_t _integrator = 0xFF;
		bool _falling = false;
		bool _rising = false;
		uint _delay;
		absolute_time_t later;
};
#endif

typedef struct {
	int gpio;
	uint8_t integrator;
	bool falling;
	bool rising;
	bool delay;
	absolute_time_t later;	
} debounce_t;

void debounce_init(debounce_t* deb, uint gpio, uint delay);
bool debounce_falling(debounce_t* deb);
bool debounce_rising(debounce_t* deb);

#ifdef __cplusplus
}
#endif

