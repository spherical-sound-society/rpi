#include "pico/stdlib.h"
#include "ssd1306.h"
#include <stdio.h>
#include "pi.h"

typedef uint32_t u32;
typedef uint64_t u64;

#define ALARM 0
#define DELAY (2*1000000)

static void alarm_0_irq() // unneeded
{
	pi_alarm_rearm(ALARM, DELAY);
	char msg[80];
	static volatile int i = 0;
	sprintf(msg, "Count %d\n", i++);
	//ssd1306_print(msg);
}

#define PIN 17
#define BUZZ 18

int main()
{
	int h = 32;
	if(1) h = 64;
	pi_i2c_init(4);
	i2c_set_baudrate(&i2c0_inst, 400000);
	init_display(h);
	fill_scr(0); // empty the screen


	pi_gpio_out(BUZZ); // doesn't seem effective on maker pi
	//GpioOut pin(17); // used to test speed of writing

	pi_alarm_init(ALARM, alarm_0_irq, DELAY);

	u64 prev_secs = 1;
	for(;;) {
		u64 secs = time_us_64()/ 1000000;
		u32 mins = secs/60;
		secs = secs % 60;
		pi_gpio_high(PIN);
		ssd1306_home();
		//ssd1306_printf("%02d:%02d", mins, secs);
		char str[20];
		sprintf(str, "%02d:%02d", mins, (u32) secs);
		print_str(str, 4);

		pi_gpio_low(BUZZ);
		if(prev_secs != secs) {
			if(secs == 0 || secs == 40 || secs == 45) {
				pi_gpio_high(BUZZ);
			}
		}
		prev_secs = secs;

		//ssd1306_display_cell();
		show_scr();
		pi_gpio_low(PIN);
		//pin.off();
		sleep_ms(400);
	}

	return 0;
}
