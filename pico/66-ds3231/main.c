#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"
// #include "tusb.h" // if you want to use tud_cdc_connected()

#include "pi.h"
#include "ds3231.h"
#include "ssd1306.h"

#define BUFF_MAX 128

volatile uint32_t tick = 0;
bool timer_callback(repeating_timer_t *rt) {
	tick++;
	int rem = tick % 1024;
	if(rem == 0) pi_gpio_high(LED_BUILTIN);
	if(rem == 100) pi_gpio_low(LED_BUILTIN);
	return true;
}
int main() 
{
	stdio_init_all();
	// while(!tud_cdc_connected()) sleep_ms(250); // wait for usb serial 
	pi_gpio_out(LED_BUILTIN);
	pi_i2c_init(4);
	i2c_set_baudrate(&i2c0_inst, 400000);
	DS3231_init(DS3231_CONTROL_INTCN, (uint32_t) &i2c0_inst);
	init_display(64, (uint32_t) &i2c0_inst);
	repeating_timer_t timer;
	add_repeating_timer_ms(-1, timer_callback, 0, &timer);

	struct ts t;
	uint32_t t1;

	for(;;) {
		DS3231_get_uk(&t);
		char buff[BUFF_MAX];
		snprintf(buff, BUFF_MAX, "%d.%02d.%02d %02d:%02d:%02d", t.year, t.mon, t.mday, t.hour, t.min, t.sec);
		printf("%s\n", buff);
		printf("Temp: %foC\n\n", DS3231_get_treg());
		ssd1306_printf_at(3, 0, 24, "%dC", (int) DS3231_get_treg()); 
		ssd1306_printf_at(2, 0, 0, "%02d:%02d:%02d", 
				t.hour, t.min, t.sec);
		//printf("Temp: %dC\n\n", (int) DS3231_get_treg());             
		ssd1306_printf_at(3, 0, 24, "%dC", (int) DS3231_get_treg()); 

		t1 = tick;
		show_scr(); // takes about 25ms at 400kHz
		t1 = tick - t1;
		//t1 = tick;
		uint32_t mins = tick / 1000/ 60;
		uint32_t hrs = mins / 60;
		mins = mins % 60;
		ssd1306_printf_at(1, 0, 56, "%dh%dm", hrs, mins); 

		sleep_ms(1000);		
	}

	return 0;
}

