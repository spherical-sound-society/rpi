#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
#include "hardware/spi.h"
#include "hardware/gpio.h"
#include <math.h>

#include "pi.h"


typedef uint8_t u8;
typedef uint16_t u16;

#define	PIN_SCK		10
#define	PIN_MOSI	11

#define	PIN_CS 		15


//#define LDAC		14 // doesn't seem to help

#define PULSE 18
#define ALARM 0


const int fs = 22000; // frequency sample
const int fw = 440; // frequency of wave

#define MHz 1000000
const uint64_t delay = 1*MHz / fs;



void pin_low(uint8_t pin)
{
	gpio_put(pin, 0);
}

void pin_high(uint8_t pin)
{
	gpio_put(pin, 1);
}


void mcp4921_init()
{

	pi_gpio_init(PIN_CS, OUTPUT);
	pin_high(PIN_CS);
	int spi_speed = 8 * MHz;
	spi_init(spi1, spi_speed);
	spi_set_format(spi1, 16, 0, 0, SPI_MSB_FIRST);
	gpio_set_function(PIN_SCK,  GPIO_FUNC_SPI);
	gpio_set_function(PIN_MOSI, GPIO_FUNC_SPI);
	//gpio_set_function(PIN_MISO, GPIO_FUNC_SPI);
}

void mcp4921_write(uint16_t vol)
{
	if(vol>4095) vol = 4095;
	//const uint16_t ctl =  0b0011 << 12; // A/B  BUF GA SHDN - original settings
	const uint16_t ctl =  0b0111 << 12; // A/B  BUF GA SHDN 
	vol |= ctl;
	//uint16_t hi = vol >>8, lo = vol & 0xFF;
	//uint8_t data[] = { (uint8_t)hi, (uint8_t)lo};
	//spi_set_baudrate(spi1, 1'500'000);
	pin_low(PIN_CS);
	//static int idx = 0;
	//u16 src = sines[idx];
	//uint16_t data = vol;
	spi_write16_blocking(spi1, &vol, 1);
	pin_high(PIN_CS);
}


void write_to_dac()
{
	const float pi2 = 6.283185307179586;
	const float dx = pi2 * fw / fs;
	static volatile float x = 0, vol = 0;
	//static int idx = 0;
	//u16 src = 0; // sines[idx];
	mcp4921_write(vol);
	x += dx;
	if(x>pi2) x -= pi2;
	vol = (sin(x) +1.0) * 4095.0/2.0;


	//idx++;
	//if(idx == SAMPLES) idx = 0;
}


static void alarm_0_irq() 
{
	pi_alarm_rearm(ALARM, delay);
#ifdef LDAC
	gpio_put(LDAC, 0);
	gpio_put(LDAC, 1);
#endif
	write_to_dac();
	pi_gpio_toggle(PULSE);
}


int main() 
{
	stdio_init_all();
	mcp4921_init();

#ifdef LDAC
	gpio_init(LDAC);
	gpio_set_dir(LDAC, GPIO_OUT);
	gpio_put(LDAC, 1);
#endif

	gpio_init(PULSE);
	gpio_set_dir(PULSE, GPIO_OUT);

	pi_alarm_init(ALARM, alarm_0_irq, delay);
	for(;;) tight_loop_contents();

	return 0;
}

