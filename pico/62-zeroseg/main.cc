#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
//#include "hardware/adc.h"
//#include "hardware/clocks.h"
//#include "hardware/flash.h"
#include "hardware/gpio.h"
#include "hardware/timer.h"
//#include "hardware/irq.h"
//#include "hardware/pwm.h"
//#include "hardware/spi.h"
// #include "tusb.h" // if you want to use tud_cdc_connected()

#include "pi.h"


#define LED  25 // GPIO of built-in LED



int main() 
{
	//stdio_init_all();
	// while(!tud_cdc_connected()) sleep_ms(250); // wait for usb serial 

	pi_gpio_init(LED, OUTPUT);
	pi_max7219_init();

	uint32_t count = 0;
	while(1) {
		pi_max7219_show_count(count++);
		pi_gpio_toggle(LED);
		sleep_ms(1000);
	}

	return 0;
}

