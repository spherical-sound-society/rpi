#include "hardware/adc.h"
#include "pico/stdlib.h"
#include "ssd1306.h"
#include <stdio.h>
#include "pi.h"

typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

#define BTN 17
#define ADC 26

int main()
{
	int h = 32;
	if(1) h = 64;
	pi_i2c_init(4);
	i2c_set_baudrate(&i2c0_inst, 400000);
	init_display(h);
	fill_scr(0); // empty the screen

	pi_gpio_init(BTN, INPUT_PULLUP);

	adc_init();
	adc_gpio_init(ADC);
	adc_select_input(0); // for gpio26

	for(;;) {
		u16 val = adc_read();
		ssd1306_printf("%d\n", val);
		show_scr();
		sleep_ms(1000);
		while(pi_gpio_is_low(BTN));
	}

	return 0;
}
