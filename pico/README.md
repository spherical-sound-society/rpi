# Pico

## Setup

Define the `PICOMC` environment variable. For example, add this
to `.bashrc`:
```
export PICOMC=/home/$USER/repos/rpi/pico
```

## Openocd

```
openocd -f interface/picoprobe.cfg -f target/rp2040.cfg -s tcl
```

If getting `LIBUSB_ERROR_ACCESS`:
```
Error: Can't find a picoprobe device! Please check device connections and permissions.
```

then add rule to `/etc/udev/rules.d/60-openocd.rules`:
```
# Raspberry Pi Picoprobe
ATTRS{idVendor}=="2e8a", ATTRS{idProduct}=="0004", MODE="660", GROUP="plugdev", TAG+="uaccess"
```

[Source](https://forums.raspberrypi.com/viewtopic.php?t=312867)


## Technical notes

* [boot](boot.md) - booting into program mode
* [dma](dma.md)
* [gpio](gpio.md)
* [interrupts](interrupts.txt)
* [pwm](pwm.md)
* [serial](serial.md)


## Also in this directory

* [baby](baby.md) 8 sequencer
* [kicad](kicad) - MCU layout for the KiCad electronic circuit design program


## Elsewhere in this repo

* [oled](../1306/pico-sdk)


## Projects

* [freq](freq) - fixed frequency generator using repeating\_timer\_callback
* [freq-adj](freq-adj) - frequency generator whose values can be 
set up over the USB serial port
* [thermo_lcd.py](thermo_lcd.py) - display on-board temperature to LCD

