message("picomc.cmake says hi")

set(PICO_PLATFORM rp2040)
#project(app C CXX ASM) # needs to be in the projects file, I think
set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)



include($ENV{PICO_SDK_PATH}/pico_sdk_init.cmake)
include_directories($ENV{PICOMC})
include_directories($ENV{RPI}/pot)
add_subdirectory($ENV{PICOMC} build)
#pico_sdk_init()

add_custom_target(flash DEPENDS app.uf2)
add_custom_command(TARGET flash
	USES_TERMINAL
	COMMAND cp app.uf2 /media/$ENV{USER}/RPI-RP2
	)
