#include <gpio.h>
#include <millis.h>

#define LED PC3

int main()
{
	init_millis();
	pinMode(LED_BUILTIN, OUTPUT);
	pinMode(LED, OUTPUT);

	while (1) {
		// on
		digitalWrite(LED, 1);
		digitalWrite(LED_BUILTIN, 0);
		delay_millis(500);

		// off
		digitalWrite(LED, 0);
		digitalWrite(LED_BUILTIN, 1);
		delay_millis(5000);
	}
}
