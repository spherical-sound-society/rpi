#pragma once

#include <inttypes.h>

/* These are adapters that you may have to implement yourself */
//void i2c_transfer7(uint32_t i2c, uint8_t addr, const uint8_t *w, size_t wn, uint8_t *r, size_t rn);
//int ledmat_bsp_write (uint8_t *data, int len);
//void ledmat_bsp_init(void);

#define LEDMAT_SID 0x70

void ledmat_set(uint8_t row, uint8_t col, int on);
void ledmat_init(uint32_t i2c_port);
void ledmat_show(void);
void led_set_row(uint8_t row, uint8_t value);
void ledmat_toggle(uint8_t row, uint8_t col);
void ledmat_copy_rows(uint8_t *rows);
void ledmat_copy_cols(uint8_t *cols);
void ledmat_clear(void);
void ledmat_set_col(uint8_t col, uint8_t value);
