#include <string.h>
#include "ledmat.h"

static uint32_t i2c;
extern void i2c_transfer7(uint32_t i2c, uint8_t addr, const uint8_t *w, size_t wn, uint8_t *r, size_t rn);
static uint8_t grid[16]; // seems to need to be 16 rather than 8 size

static void ledmat_send(uint8_t *data, int len)
{
	i2c_transfer7(i2c, LEDMAT_SID, data, len, 0, 0);
}

void ledmat_clear(void)
{
        for (int r = 0; r < 8; ++r)
		led_set_row(r, 0);
}
void ledmat_copy_rows(uint8_t *rows)
{
	//memcpy(grid, new_grid, sizeof(grid));
	for(int i = 0; i< 8; i++) 
		led_set_row(i, *(rows + i));
	//memcpy(grid, new_grid, 8);
}

void ledmat_copy_cols(uint8_t *cols)
{
	for (int c = 0; c < 8; ++c) {
		uint8_t col = *(cols + c);
		for (int r = 0; r < 8; ++r) {		
	                ledmat_set(r, c, col & (1<<r));
		}
	}			               	           	
}

void ledmat_set(uint8_t row, uint8_t col, int on)
{
	// the LED has a strange co-ordinate system
	int pos1 = col*2 +1; // fill in the odd-numbered indices. The even s/b 0 (they're for colour)
	int pos2 = (row + 7) %8; // the device seems to have a crazy grid system


	if(on)
		grid[pos1] |= (1<<pos2); // set bit
	else
		grid[pos1] &= ~(1<<pos2); // clear bit
}

void ledmat_toggle(uint8_t row, uint8_t col)
{
	// the LED has a strange co-ordinate system
	int pos1 = col*2 +1; // fill in the odd-numbered indices. The even s/b 0 (they're for colour)
	int pos2 = (row + 7) %8; // the device seems to have a crazy grid system
	int mask = 1 << pos2;
	if(grid[pos1] & mask)
		grid[pos1] &= ~mask; // clear bit
	else
		grid[pos1] |= mask; // set bit
}

void ledmat_init(uint32_t i2c_port)
{
	i2c = i2c_port;
	//ledmat_bsp_init();
	
	for(unsigned int i = 0; i<sizeof(grid); i++) grid[i] = 0;
	
	uint8_t cmds[] = {
			0x20 | 1, // turn on oscillator
			0x81, // display on
			0xe0 | 0 // brightness to minimum
	};
	for(unsigned int i =0; i < sizeof(cmds); i++) {
		ledmat_send(cmds+i, 1);
	}
}


void ledmat_show(void)
{
	ledmat_send(grid, sizeof(grid));
}

void led_set_row(uint8_t row, uint8_t value)
{
        for (int c = 0; c < 8; ++c)
                ledmat_set(row, c, value & (1<<c));
}

// this looks suspiciously wrong
void ledmat_set_col(uint8_t col, uint8_t value)
{
	for(int r=0; r<8; r++)
		ledmat_set(r, 7-col, value & (1<<(7-r)));
}
