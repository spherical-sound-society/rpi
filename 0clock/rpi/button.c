#include <signal.h>
#include <gpiod.h>
#include <stdio.h>
#include <unistd.h>


struct gpiod_chip* chip;
struct gpiod_line *buzzer;

void exit_function()
{
	puts("button exiting");
	gpiod_line_set_value(buzzer, 0);
	gpiod_line_release(buzzer);
	gpiod_chip_close(chip);
}


void intHandler(int dummy)
{
        exit(0);
}

int main()
{
        signal(SIGINT, intHandler);
        signal(SIGTERM, intHandler);
        signal(SIGQUIT, intHandler);
        atexit(exit_function);
	puts("button starting");

	chip = gpiod_chip_open_by_name("gpiochip0");

	buzzer = gpiod_chip_get_line(chip, 16);
	gpiod_line_request_output(buzzer, "clock", 0);
	gpiod_line_set_value(buzzer, 1);

	printf("press any key to exit");
	getchar();

	return 0;
}
