#include <assert.h>
#include <inttypes.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>



#if 0
#include <pthread.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include <asm/ioctl.h>
#include <assert.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <poll.h>
#endif

#include "bcm2835.h"
#include "ssd1306.h"
#include "MQTTClient.h"

#define ADDRESS         "tcp://localhost:1883"
#define CLIENTID        "MYCLOCK"


void i2c_transfer7(uint32_t i2c, uint8_t addr, const uint8_t *w, size_t wn, uint8_t *r, size_t rn)
{
	// incomplete
	assert(rn ==0);
	if(wn) bcm2835_i2c_write(w, wn);
}



uint32_t millis() { return bcm2835_st_read()/1000; }
uint32_t ticks;
uint32_t timer_is_running = 0;
uint32_t timer_start_time;
uint32_t timer_elapsed = 0; // in ticks

enum modes {
	wall, // normal mode where we just show time
	timing_unexpired,
	timing_expired,
	alarming
};

enum modes mode = wall;

//#define SID 0x3C


void delay_ms(int ms) { usleep(ms*1000); }

void stop_alarm(void);
void buz_task(int n);

void start_wall()
{
	mode = wall;
	timer_is_running =0;
	buz_task(0);
}

void start_timing(void)
{
	puts("start_timing");
	mode = timing_unexpired;
	timer_is_running = 1;
	timer_start_time = ticks;
	buz_task(1);
}

void stop_timing(void)
{
	start_wall();
}

void timer_task(void)
{
	uint32_t timer_duration = 1000*60*30; // ms
	if(timer_is_running==0 ) return; 
	timer_elapsed = ticks -timer_start_time;
	if(mode == timing_unexpired && timer_elapsed >= timer_duration) {
		mode = timing_expired;
		buz_task(2);
	}
}

const uint8_t buz_pin = 16;
void buz_off(void) {bcm2835_gpio_clr(buz_pin);}
void buz_on(void) {bcm2835_gpio_set(buz_pin);}

void buz_pattern(int pattern, uint32_t interval)
{
	static int prev = 0;

	int on = 0;
	switch(pattern) {
		case 0:
			break;
		case 2:
			on = (1000 < interval) && (interval<1500);
			//fallthrough
		case 1:
			if(interval < 500) on = 1;
			break;
		case 3:
			on = (interval < 200) 
				|| (400 < interval && interval<600)
				|| (800 < interval && interval<1000);
	}

	if(on && (prev==0)) buz_on();
	if((on==0) && prev) buz_off();
	prev = on;

}



/* value of n:
 * -1 : update buzzer
 *  0 : stop buzzer
 *  1 : one beat every 5se
 */
void buz_task(int n)
{
	static uint32_t buz_start; // initialising unecessary as it is off anyway
	static int running = 0;


	if(n==-1) {
		uint32_t interval = (ticks - buz_start)%5000;
		buz_pattern(running, interval);
		return;
	}

	running = n;
	buz_start = ticks;
}

void btn_pressed(void)
{		
	switch(mode) {
		case wall:
			start_timing();
			break;
		case timing_unexpired:
		case timing_expired:
			stop_timing();
			break;
		case alarming:
			stop_alarm();
			break;
	}			
}

int btn_task(void)
{
	static uint8_t pin = 23;
	static int inited = 0;
	if(inited==0) {
		inited = 1;
		bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_INPT);
		bcm2835_gpio_set_pud(pin, BCM2835_GPIO_PUD_UP);
	}

	int pressed = 0; // return value
	const int MAX = 8;
	static int count = 0, prev = 0;
	static int press_num = 0;
	if(bcm2835_gpio_lev(pin))
		count++;
	else
		count--;
	if(count<0) count = 0;
	if(count>MAX) count = MAX;
	if(count == 0 && prev != count) {
		pressed = 1;
		printf("btn_task: Press detected:%d\n", press_num++);
		btn_pressed();
	}

	prev = count;
	return pressed;
}

#if 0
void print_char(char c, int scale, int xoff)
{
	ssd1306_home();
	int x = xoff;
	for(int col=0; col < 6; col++) {
		for(int row=0; row < 8; row++) {
			//printf("start setting:%c, %d\n", c, c);

			int setting = font_pixel(c, col, row);
			//puts("done setting");
			for(int x1 = 0; x1<scale; x1++) {
				for(int y1=0; y1<scale; y1++) {
					draw_pixel(x + x1, row*scale + y1, setting);
				}
			}
		}
		x += scale;
	}
}

void print_str(char *str, int scale)
{
	int xoff = 0;
	while(*str) {
		print_char(*str, scale, xoff);
		str++;
		xoff += 6*scale;
	}
}

void show_str(char* str)
{
	setCursorx(0);
	print_str(str, 4);
	//show_scr();
}
#endif

void oled_task(void )
{
#define LEN 6

	char buf[LEN];
	time_t curtime = time (NULL);
	switch(mode) {
		case wall: {
				   // show normal time
				   struct tm *loc_time = localtime (&curtime);
				   char fmt[] = "%H:%M";
				   fmt[2] = loc_time->tm_sec % 2 == 0 ? ':' : ' ';
				   strftime(buf, LEN, fmt, loc_time);
			   }
			   break;
		case timing_unexpired:
		case timing_expired: {
					     // show the stopwatch
					     uint32_t elapsed = timer_elapsed/1000;
					     int secs = elapsed % 60;
					     int mins = elapsed / 60;
					     sprintf(buf, "%02d.%02d", mins, secs);
				     }
				     break;
		case alarming:
				     sprintf(buf, "ALARM");
				     break;
	} 

	ssd1306_home();
	show_str(buf);
	//ssd1306_puts(buf);
	//show_scr();
	//ssd1306_display_cell();
	//delay_ms(20);
}


void start_alarm(void)
{
	mode = alarming;
	buz_task(3);
}

void stop_alarm(void)
{
	start_wall();
}

MQTTClient mqtt_client;

int on_mqtt_message(void *context, char *topicName, int topicLen, MQTTClient_message *message) {
	char* payload = message->payload;
	printf("Received operation %s\n", payload);

	if(payload[0] == '3') {
		puts("mqtt command 3");
		start_alarm();
	}

	MQTTClient_freeMessage(&message);
	MQTTClient_free(topicName);
	return 1;
}
void mqtt_init(void)
{
	MQTTClient_create(&mqtt_client, ADDRESS, CLIENTID, MQTTCLIENT_PERSISTENCE_NONE, NULL);
	MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
	//conn_opts.username = "<<tenant_ID>>/<<username>>";
	//conn_opts.password = "<<password>>";

	MQTTClient_setCallbacks(mqtt_client, NULL, NULL, on_mqtt_message, NULL);

	int rc;
	if ((rc = MQTTClient_connect(mqtt_client, &conn_opts)) != MQTTCLIENT_SUCCESS) {
		printf("Failed to connect, return code %d\n", rc);
		exit(-1);
	}
	MQTTClient_subscribe(mqtt_client, "myclock", 0);
}

void mqtt_deinit(void)
{
	MQTTClient_disconnect(mqtt_client, 1000);
	MQTTClient_destroy(&mqtt_client);

}
void exit_function()
{
	puts("exit_function:called");
	buz_off();
	mqtt_deinit();
}

void intHandler(int dummy)
{
	//exit_function();
	exit(0);
}



int main()
{
	int status;
	signal(SIGINT, intHandler);
	atexit(exit_function);
	bcm2835_init();
	bcm2835_i2c_begin();
	bcm2835_i2c_setSlaveAddress(0x3c);



	init_display(32, 0);
	ssd1306_puts("clock starting");
	show_scr();
	bcm2835_gpio_fsel(buz_pin, BCM2835_GPIO_FSEL_OUTP);

	printf("sizeof(time_t):%d\n", sizeof(time_t)); // 4 -- holds seconds since epoch
	//mqtt_init(); // seems to cause failure from 2022-04-05
	start_wall();

#if 1
	//pthread_create(&t1, NULL, &btn_task, NULL);
	//pthread_create(&t2, NULL, &buz_task, NULL);
	//pthread_create(&t3, NULL, &oled_task, NULL);
#endif

	while(1) {
		while(millis() == ticks) {
			usleep(250);
		}
		ticks = millis();


		if(ticks%4==0) {
			timer_task();
			btn_task();
		}
		if(ticks%20==1) oled_task();
		if(ticks%4==2) buz_task(-1);
		if(ticks%4==3) ssd1306_display_cell();
		//if(ticks%1000 ==0) printf("ticks = %d\n", ticks);
	}

	return 0;
}
