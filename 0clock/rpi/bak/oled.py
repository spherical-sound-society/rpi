# Basic example of clearing and drawing pixels on a SSD1306 OLED display.
# This example and library is meant to work with Adafruit CircuitPython API.
# Author: Tony DiCola
# License: Public Domain

import datetime
import time

from board import SCL, SDA
import busio

import adafruit_ssd1306
from font6_8 import font6_8
import clock

# Create the I2C interface.
i2c = busio.I2C(SCL, SDA)

# Create the SSD1306 OLED class.
# The first two parameters are the pixel width and pixel height.  Change these
# to the right size for your display!
display = adafruit_ssd1306.SSD1306_I2C(128, 32, i2c)
# Alternatively you can change the I2C address of the device with an addr parameter:
#display = adafruit_ssd1306.SSD1306_I2C(128, 32, i2c, addr=0x31)

# Clear the display.  Always call show after changing pixels to make the display
# update visible!
display.fill(0)

curx = 0


def print_char(c, scale = 1):  
	global curx 
	c = ord(c)
	idx = (c-32)*6 +4
	charmap = font6_8[idx:idx+6]
	for col in charmap:
		for row in range(8):
			setting = 1 if col & 1 else 0
			for x1 in range(scale):
				for y1 in range(scale):
					display.pixel(curx + x1, row*scale + y1, setting)
			col = col >> 1
		curx += scale

def print_str(s, scale = 1): 
	for c in s: print_char(c, scale)


def show_str(s):
	global curx
	curx = 0
	print_str(s, scale = 4)
	display.show()


def show_clock():
	now = clock.get_now()
	sep = ":" if now.second % 2 == 0 else " "
	fmt = "%H" + sep + "%M"
	s = now.strftime(fmt)
	show_str(s)

elapsed_secs = 0
def show_elapsed():
	global elapsed_secs
	#secs = int(elapsed / 1000)
	#secs = int(elapsed )
	secs = elapsed_secs
	mins = int(secs / 60)
	secs = secs % 60
	s = "{:02d}.{:02d}".format(mins, secs)
	show_str(s)

def show_action():
	if clock.is_showing_clock():
		show_clock()
	else:
		show_elapsed()

def main():
	global elapsed
	started = get_now()
	while True:
		if False:
			show_clock()
		else:
			now = get_now()
			elapsed = now - started
			elapsed = elapsed.seconds
			show_elapsed()
		time.sleep(0.2)

if __name__ == "__main__":
        main()

