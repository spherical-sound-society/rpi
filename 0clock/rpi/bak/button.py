import digitalio
import board
import time
import threading

btn = digitalio.DigitalInOut(board.D23)
btn.direction = digitalio.Direction.INPUT
btn.pull = digitalio.Pull.UP

#while True:
#	print(btn.value)
#	time.sleep(1)

class Every:
	def __init__(self, fn, delay):
		self.fn = fn
		self.delay = delay
		self.more = True
		self.t1 = threading.Thread(target=self.loop)
		self.t1.start()

	def loop(self):
		while self.more:
			self.fn()
			time.sleep(self.delay)

	def __del__(self):
		print("Deleting an Every")
		self.more = False
		self.t1.join()


prev = 0
count = 0
def do_button():
	global btn, count, prev
	val = 1 if btn.value else -1
	count += val
	MAX = 8
	if count > MAX: count = MAX
	if count < 0: count = 0
	if prev == count: return False
	prev = count
	if count == 0:	return True
	return False

def main():
	print("button.py started")
	#def loop():
	#	while True:
	#		do_button()
	#		time.sleep(1)

	#t1 = threading.Thread(target=loop)
	#try:
	#	t1.start()
	#finally:
	#	t1.join()

	#xx = Every(do_button, 0.004)
	#input()
	while True:
		if do_button():
			print("Button down")
		time.sleep(0.004)


if __name__ == "__main__":
	main()
