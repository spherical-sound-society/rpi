# clock

Fancy clock timer

**CPU usage** 36% on both Pi0 and Pi3


## Alarms

To set clock into alarm mode, issue a command like:
```
mosquitto_pub -h localhost -t myclock -m "3"
```

To automate an alarm using `at`:
```
at 21:00
mosquitto_pub -h localhost -t myclock -m "3"
^D
```

To list the pending alarms: **`atq`**



## Setup

```
sudo apt install at # optional, helps with alarms
```

## Links 

* [Paho MQTT C Client Library ](https://www.eclipse.org/paho/files/mqttdoc/MQTTClient/html/_m_q_t_t_client_8h.html)

## Status

2021-12-25	Added alarm capabilities

2021-12-24	Confirmed working



