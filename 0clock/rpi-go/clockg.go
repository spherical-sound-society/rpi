package main

import (
	//"/home/pi/rpi/0clock/rpi-go/display"
	//"display"

	"fmt"
	"os"
	"os/signal"
	"syscall"
	"strings"
	//"strconv"
	"time"
	"log"
	//"math"

	"periph.io/x/conn/gpio"
	//"periph.io/x/conn/gpio/gpioreg"
	//"periph.io/x/host/v3/bcm283x"
	//"periph.io/x/host/v3/bcm283x"
	"periph.io/x/host"
	"periph.io/x/host/rpi"

)

//var buz_pin = gpioreg.ByName("GPIO16");
var buzzer =  rpi.P1_36 // BCM16


func SetupCloseHandler() {
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		fmt.Println("\r- Ctrl+C pressed in Terminal")
		//DeleteFiles()
		DisplayDeinit()
		buzzer.Out(gpio.Low);
		os.Exit(0)
	}()
}


/* mare - a function which can kill a process
*/
func mare(c chan bool, n time.Duration)  (bool) {
	c1  := make(chan bool)
	go  func() {
		time.Sleep(n);
		c1 <- false
	}()

	select {
	case  <-c1:
		return false
	case v1, ok := <-c:
		if !ok { return true }
		return v1
	}
	return false
}

var clock_chan = make(chan bool)

func DisplayClock() {
	for {
		dt := time.Now()
		str := fmt.Sprintf("%02d:%02d", dt.Hour(), dt.Minute())
		if(dt.Second() % 2 == 0) {str = strings.Replace(str, ":", " ", 1)}
		Displaystr(str)
		if mare(clock_chan, 250 *time.Millisecond)  { return;}
	}
}


var beep_chan = make(chan bool)

func buzz_on() { buzzer.Out(gpio.High) }
func buzz_off() { buzzer.Out(gpio.Low) }

func BeepSingle() {
	defer func() {
		buzz_off()
		beep_chan <- true
	}()

	for {
		buzz_on()
		if mare(beep_chan, 500 * time.Millisecond) { return }
		buzz_off()
		if mare(beep_chan, 4500 * time.Millisecond) { return }
	}
}
func BeepDouble() {
	defer func() {
		buzz_off()
		beep_chan <- true
	}()

	for {
		buzz_on()
		if mare(beep_chan, 200 * time.Millisecond) { return }
		buzz_off()
		if mare(beep_chan, 200 * time.Millisecond) { return }
		buzz_on()
		if mare(beep_chan, 200 * time.Millisecond) { return }
		buzz_off()
		if mare(beep_chan, 4400 * time.Millisecond) { return }
	}
}

var counter_chan = make(chan bool)
func Counter() {
	defer func() {
		//buzz_off()
		beep_chan <- true // signal closure
		<-beep_chan // confirm closure
		//mode_change <- true
		counter_chan <- true // wave good-bye
	}()
	started := time.Now()
	var single_beeping = true
	go BeepSingle()
	for {
		now := time.Now()
		diff := now.Sub(started)
		var secs int = int(diff.Seconds())
		mins := secs / 60
		secs =  secs % 60
		str := fmt.Sprintf("%02d.%02d", mins, secs)
		//str = "66.67"
		Displaystr(str)
		if mare(counter_chan, 250 *time.Millisecond)  { return;}

		if (diff.Minutes() > 30) && single_beeping {
			single_beeping = false
			beep_chan <- true // singal closure of single-beeping
			<-beep_chan // wait for closure
			go BeepDouble()
		}
	}
}

func main() {
	fmt.Println("ssd test");
	SetupCloseHandler()
	if _, err := host.Init(); err != nil { log.Fatal(err) }
	go Debounce()
	go DisplayClock()
	go ButtonChange()

	//i := 0
	for {
		time.Sleep(5 * time.Second)
	}
}

var mode_change = make(chan bool)

func ButtonChange() {
	for {
		<-mode_change
		clock_chan <- true // tell clock to stop displaying
		go Counter() // start counter

		<-mode_change
		counter_chan <- true // tell counter to stop
		<-counter_chan // wait for stop
		go DisplayClock() // start clock display again
	}
}


func ButtonPressed() {
	fmt.Println("ButtonPressed called");
	mode_change <- true
}


func Debounce() {
	var button = rpi.P1_16 // BCM23
	button.In(gpio.PullUp, gpio.NoEdge)
	count := 0
	prev := 0
	max := 8
	press_num := 0
	for {
		if button.Read() {
			count++
			if count > max { count = max }
		} else {
			count--
			if count < 0 { count = 0 }
		}
		if count == 0 && prev != count {
			//fmt.Println("Debounce: press detected num", press_num)
			ButtonPressed()
			press_num++
		}
		prev = count
		time.Sleep(4* time. Millisecond)
	}
}

