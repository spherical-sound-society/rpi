package main

import (
	"fmt"
	"image"
	"log"

	"periph.io/x/conn/i2c"
	"periph.io/x/conn/i2c/i2creg"
	"periph.io/x/devices/ssd1306"
	"periph.io/x/devices/ssd1306/image1bit"
	//"periph.io/x/host"
	"github.com/nfnt/resize"

	//<F7>  "golang.org/x/image/font"
	"golang.org/x/image/font"
	"golang.org/x/image/font/basicfont"
	//"golang.org/x/image/font/gofont/gomono"
	"golang.org/x/image/math/fixed"
)

var  initialised = false
//var img *image1bit.VerticalLSB
var dev *ssd1306.Dev
//var drawer font.Drawer
var bus i2c.BusCloser

func initialise() {
	initialised = true
	var err error
	bus, err = i2creg.Open("")
	if err != nil { log.Fatal(err) }
	var opts = ssd1306.DefaultOpts;
	opts.H = 32;
	opts.Sequential = true; // fix for half the pixels missing

	dev, err = ssd1306.NewI2C(bus, &opts)
	if err != nil { log.Fatal(err) }

}

func Displaystr(str string) {
	if(!initialised) {initialise()}
	img := image1bit.NewVerticalLSB(dev.Bounds())
	f := basicfont.Face7x13
	drawer := font.Drawer{
		Dst:  img,
		Src:  &image.Uniform{image1bit.On},
		Face: f,
		//Dot:  fixed.P(0, img.Bounds().Dy()-1-f.Descent),
		Dot:  fixed.P(0, 9),

	}
	drawer.Dot = fixed.P(0, 9)
	drawer.DrawString(str)
	img1 := resize.Resize(448, 108, img, resize.Lanczos3)

	if err := dev.Draw(dev.Bounds(), img1, image.Point{}); err != nil {
		log.Fatal(err)
	}

}

func DisplayDeinit() {
	fmt.Println("DisplayDeinit:called");
	initialised = false
	bus.Close()
}
