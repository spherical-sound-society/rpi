#include <stdio.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <string.h>
#include "ledmat.h"


static uint8_t  pattern1[] = { 
	0b10000001,
	0b01000010,
	0b00100100,
	0b00010000,
	0b00001000,
	0b00100100,
	0b01000010,
	0b10000001
};

static uint8_t  pattern2[] = { // the letter P, with some orientation frills
	0b11110001,
	0b10001000,
	0b10001000,
	0b11110000,

	0b10000000,
	0b10000000,
	0b10000001,
	0b10000010
};



int main(int argc, char* argv[])
{
	uint8_t* pattern = pattern1;
	//puts(argv[1]);
	if(argc ==2 && strcmp(argv[1],"2")==0) {
		puts("Using pattern 2");
		pattern = pattern2;
	} else {
		puts("Using pattern 1");
	}

	wiringPiSetup();
	ledmat_init();

	for(int r=0; r<8; r++) {
		uint8_t row = pattern[r];
		for(int c=0; c<8; c++) {
			ledmat_set(r,c, row >> 7);
			row <<=1;
		}
	}
	ledmat_show();

	return 0;
}
