#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <sys/ioctl.h>
#include <asm/ioctl.h>
#include <assert.h>
//#include <i2c/smbus.h>
#include <unistd.h>
#include <fcntl.h>


#include "ledmat.h"

#define I2C_SMBUS_WRITE 0
#define I2C_SMBUS_BYTE              1

//#define SID 0x70 // Slave ID


#define I2C_SMBUS_BLOCK_MAX 32 // mcarter guess (but should be good)

int fd;

union i2c_smbus_data
{
	uint8_t  byte ;
	uint16_t word ;
	uint8_t  block [I2C_SMBUS_BLOCK_MAX + 2] ;    // block [0] is used for length + one more for PEC
} ;

struct i2c_smbus_ioctl_data
{
	char read_write ;
	uint8_t command ;
	int size ;
	union i2c_smbus_data *data ;
} ;



// note that ioctl() can write only a max of 32 bytes
int ledmat_bsp_write (uint8_t *data, int len)
{
	assert(len<=33);

#define I2C_SMBUS       0x0720  /* SMBus-level access */

	struct my_i2c_smbus_ioctl_data
	{
		char read_write ;
		uint8_t command ;
		int size ;
		uint8_t *data ;
	} ;


	//union i2c_smbus_data args ;
	struct my_i2c_smbus_ioctl_data args ;

	args.read_write = I2C_SMBUS_WRITE;
	args.command    = data[0] ;
	args.size       = len;
	args.data = data+1;
	//args.data       = (uint8_t*) (data+1);
	//union i2c_smbus_data d1;
	//d1.block = (data+1);
	//args.data    = &d1;

	return ioctl (fd, I2C_SMBUS, &args) ;
	//return ioctl (fd, I2C_SMBUS, I2C_SMBUS_WRITE, data[0], size, data+1);
}


void ledmat_bsp_init(void)
{
	fd = wiringPiI2CSetup(LEDMAT_SID);
}
