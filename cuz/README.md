Blinky light (BCM pin 2, board pin 3) example using Zig. Tested against Zig 0.8.0

Works on a Pi 1 (of some flavour), and should work on a Pi 0. Won't work on most other flavours.

Install Zig from [ziglang](https://ziglang.org/). I recommend a binary release as
your repo (e.g. Debian Stable as at 2021-12-29) might not have an up-to-date llvm
that is required to compile from soure. Ziglang is capable of cross-compiling software,
so download the package for your host machine rather than for the target machine.

To build the sketch, type `make`, insert SD card, then `make install` on Linux.
On other platforms, transfer `kernel.img` manually to hard drive.

## Status

2021-12-29	Started recently. Confirmed working.
