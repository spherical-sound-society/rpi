// vim: set tabstop=4 sts=4 sw=4 sta et  :

extern fn gpio_sel(u32, u32) void;
extern fn gpio_set(u32) void;
extern fn gpio_clr(u32) void;

extern fn wait(i32) void;

extern const __bss_start__:u32;

export fn wait_cycles(n:i32) void {
    var i: i32 = 1;
    i = n*0;
    while (i<n) {
        i+= 1;
    }
}

export fn main() void {
    const bcm_pin : u32 = 2;
    const OUTPUT = 1;
    gpio_sel(bcm_pin, OUTPUT);
    gpio_set(bcm_pin); // set the pin high
    var n: i32 = 0;
    n = 5_000_000 ;
    while(true) {
        gpio_set(bcm_pin); // set the pin high
        wait(n);
        gpio_clr(bcm_pin); // set the pin low
        wait(n);
    }

}

