#include <xtimer.h>
#include <periph/gpio.h>
#include <periph/i2c.h>

#include <ledmat.h>

#pragma GCC diagnostic ignored "-Wunused-variable"

//gpio_t usr_btn = GPIO_PIN(PORT_C, 13); // user button
gpio_t led = GPIO_PIN(PORT_A, 5); // builtin-LED
i2c_t dev;

void delayish(int ms)
{
	xtimer_msleep(ms);
}

static uint8_t pattern1[] = {
	0b10000001,
	0b01000010,
	0b00100100,
	0b00010000,
	0b00001000,
	0b00100100,
	0b01000010,
	0b10000001
};

static uint8_t pattern2[] = { // the letter P, with some orientation frills
	0b11110001,
	0b10001000,
	0b10001000,
	0b11110000,
	0b10000000,
	0b10000000,
	0b10000001,
	0b10000010
};


void random_pattern(void)
{
#if 0
	while(1) {
		uint16_t rc = rand16();
		uint8_t r = rc, c = rc >> 8;
		ledmat_toggle(r, c);
		ledmat_show();
	}
#endif
}

void scrolling_pattern(void)
{
	uint8_t* pattern = pattern1;
	while(1) {
		static int offset = 0; // for scrolling purposes
		for (int r = 0; r < 8; ++r) {
			int r1 = (r + offset) %8;
			led_set_row(r1, pattern[r]);
		}
		offset++;
		ledmat_show();
		delayish(100);
	}
}

int main(void)
{

	i2c_init(dev);
	//i2c1_init();
	ledmat_init(dev);

	if(2)
		random_pattern();
	else
		scrolling_pattern();
}
#if 0
int main(void)
{
	xtimer_init();
	i2c_init(dev);
	//i2c_init_pins(dev);

	send_cmd(0x20 | 1); // turn on oscillator
	send_cmd(0x81); // display on
	send_cmd(0xE0 | 0); // brightness to dimmest (but you should probably set it)

	while (1) {
		for (int i = 0; i < 8; ++i) {
			u8 row = pattern[i];
			write_row(i, row);
			row = ((row & 0x01) ? 0x80 : 0x00) | (row >> 1); // rotate the row for fun
			pattern[i] = row;
		}
		xtimer_msleep(1);
		//delay(100);
	}


	gpio_init(led, GPIO_OUT);
	while(1) {
		gpio_set(led);
		xtimer_msleep(100);
		gpio_clear(led);
		xtimer_msleep(900);
	}
	return 0;
}
#endif
