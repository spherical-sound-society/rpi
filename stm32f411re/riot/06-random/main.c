#include <random.h>
#include <xtimer.h>
#include <periph/gpio.h>

gpio_t pin = GPIO_PIN(PORT_C, 13);

int method = 2;

/* time how long a random number takes */
void method1(void)
{

	while(1) {
		//uint32_t val = 
		random_uint32();
		gpio_toggle(pin);
	}
}

void method2(void)
{
	int count = 0;
	while(1) {		
		uint32_t val = random_uint32();
		printf("Value #%d=%ld\n", count++, val);
		gpio_set(pin);
		xtimer_msleep(100);
		gpio_clear(pin);
		xtimer_msleep(1900);
	}
}

int main(void)
{
	xtimer_init();
	gpio_init(pin, GPIO_OUT);

	random_init(0); // don't think it is necessary
	if(method == 1)
		method1();
	else
		method2();
	return 0;
}
