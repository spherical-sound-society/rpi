#include <periph/uart.h>



int main(void)
{
	puts("I will now echo characters to screen");
	int count = 0;
	while(1) {
		int c = getchar();
		if(c<0) puts("received char is negative. Hmmm");
		printf("Count:%d, '%c', dec:%d\n", count++, c, c);
	}
}
