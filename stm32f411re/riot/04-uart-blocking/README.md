# uart-blocking read

How to read and write to a uart (tests on an Nucleo F411). The readin is blocking.

In `Makefile`:
```
USEMODULE += stdio_uart_rx
```

In your code, do something like:
```
#include <periph/uart.h>

int main(void)
{
        puts("I will now echo chaaracters to scrren");
        int count = 0;
        while(1) {
                int c = getchar();
                if(c<0) puts("received char is negative. Hmmm");
                printf("Count:%d, '%c', dec:%d\n", count++, c, c);
        }
}
```


## Status

2021-11-15	Started. Working
