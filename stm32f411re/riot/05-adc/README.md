# ADC

In Makefile:
```
USEMODULE += periph_adc
```

The ADC "line numbers" are as follows:
```
static const adc_conf_t adc_config[] = {
    {GPIO_PIN(PORT_A, 0), 0, 0},  // line 0 PA0 cn7:28
    {GPIO_PIN(PORT_A, 1), 0, 1},  // line 1 PA1 cn7:30
    {GPIO_PIN(PORT_A, 4), 0, 4},  // line 3 PA4 cn7:32
    {GPIO_PIN(PORT_B, 0), 0, 8},  // line 3 PB0 cn7:34
    {GPIO_PIN(PORT_C, 1), 0, 11}, // line 4 PC1 cn7:36
    {GPIO_PIN(PORT_C, 0), 0, 10}, // line 5 PC0 cn7:38
};
```
... as defined in riot/boards/nucleo-f411re/include/periph\_conf.h



PA0 confirmed working, but not the other ADCs



The example uses line 0 (PA0).

## Status

2021-11-15	Started. Working.
