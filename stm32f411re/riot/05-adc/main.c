#include <periph/adc.h>
#include <xtimer.h>
#include <periph/gpio.h>

gpio_t pin = GPIO_PIN(PORT_C, 13);

int main(void)
{
	xtimer_init();
	gpio_init(pin, GPIO_OUT);

	adc_t adc_line = 0;
	int status = adc_init(adc_line);
	printf("adc_init status: %s\n", status == 0 ? "OK" : "FAIL");
	int count = 0;
	while(1) {
		int sample = adc_sample(adc_line, ADC_RES_8BIT);
		if(sample == -1) puts("adc_sample resolution not applicable");
		printf("ADC sample %d value: %d\n", count++, sample);
		gpio_set(pin);
		xtimer_msleep(100);
		gpio_clear(pin);
		xtimer_msleep(900);
	}
	return 0;
}
