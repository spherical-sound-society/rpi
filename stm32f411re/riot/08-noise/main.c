#include <random.h>
#include <xtimer.h>
#include <periph/gpio.h>
#include <periph/uart.h>

gpio_t pin = GPIO_PIN(PORT_C, 12);

char blink_thread_stack[THREAD_STACKSIZE_MAIN];

volatile int interval = US_PER_SEC/5000;

void *blink_thread(void *arg)
{
	(void) arg;
	xtimer_ticks32_t last_wakeup = xtimer_now();

	while(1) {
		int on = random_uint32() & 1;
		gpio_write(pin, on);
		//gpio_toggle(pin);
		xtimer_periodic_wakeup(&last_wakeup, interval);
	}
	return NULL;
}


int main(void)
{
	xtimer_init();
	gpio_init(pin, GPIO_OUT);
	puts("Type '+' to inc cutoff frequency, '-' to decrease");

	thread_create(blink_thread_stack, sizeof(blink_thread_stack),
			THREAD_PRIORITY_MAIN - 1, THREAD_CREATE_STACKTEST,
			blink_thread, NULL, "blink_thread");
	
	int freqs[] = {500, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 16000};
	int num = sizeof(freqs)/sizeof(int);
	int idx = 0;
	while(1) {
		int freq = freqs[idx];
		interval = US_PER_SEC/freq;
		printf("Cutoff freqency = %d\n", freq);
		int c = getchar();
		if(c == '-') idx--;
		if(idx<0) idx = num - 1;
		if(c == '+') idx++;
		if(idx>=num) idx = 0;

		//xtimer_msleep(1000);
	}
}
