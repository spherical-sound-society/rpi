#include <xtimer.h>
#include <periph/gpio.h>

gpio_t led1 = GPIO_PIN(PORT_C, 13); // blackpill led
gpio_t led2 = GPIO_PIN(PORT_A, 5); // nucleo led
gpio_t led3 = GPIO_PIN(PORT_B, 3); // l432 led

int main(void)
{
	xtimer_init();
	gpio_init(led1, GPIO_OUT);
	gpio_init(led2, GPIO_OUT);
	gpio_init(led3, GPIO_OUT);
	while(1) {
		gpio_set(led1);
		gpio_set(led2);
		gpio_set(led3);
		xtimer_msleep(100);
		gpio_clear(led1);
		gpio_clear(led2);
		gpio_clear(led3);
		xtimer_msleep(900);
	}
	return 0;
}
