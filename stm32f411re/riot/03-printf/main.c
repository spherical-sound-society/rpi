#include <xtimer.h>
#include <periph/gpio.h>

#if 1
#define INTERVAL 500000 // half a second
#else
#define INTERVAL US_PER_SEC/40000 // 40kHz
#endif

gpio_t pin = GPIO_PIN(PORT_C, 13);

char blink_thread_stack[THREAD_STACKSIZE_MAIN];

void *blink_thread(void *arg)
{
	(void) arg;
	xtimer_ticks32_t last_wakeup = xtimer_now();

	while(1) {
		gpio_toggle(pin);
		xtimer_periodic_wakeup(&last_wakeup, INTERVAL);
	}
	return NULL;
}


int main(void)
{
	xtimer_init();
	gpio_set(pin);
	gpio_init(pin, GPIO_OUT);
	/*
	thread_create(blink_thread_stack, sizeof(blink_thread_stack),
			THREAD_PRIORITY_MAIN - 1, THREAD_CREATE_STACKTEST,
			blink_thread, NULL, "blink_thread");
	*/

	int count = 0;
	while(1) {
		printf("Count: %d\n", count++);
		xtimer_sleep(2);
	}
}
