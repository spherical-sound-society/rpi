#include <xtimer.h>
#include <periph/gpio.h>

gpio_t usr_btn = GPIO_PIN(PORT_C, 13); // user button
//gpio_t led = GPIO_PIN(PORT_A, 5); // builtin-LED

/* LOW = 0, HIGH = 8192 */

void debounce(void)
{
	static volatile int highs = 0, prev = 0, press_count = 0;

	if(gpio_read(usr_btn)) {
		highs ++;
		if(highs > 8) highs =8;
	} else {
		highs --;
		if(highs<0) highs = 0;
		if(highs == 0 && prev != 0)
			printf("User button pressed, %d\n", press_count++);
	}
	prev = highs;
}

char ms_thread_stack[THREAD_STACKSIZE_MAIN];

/* execute every ms */
volatile uint32_t tick = 0;
void *ms_thread(void *arg)
{
	(void) arg;
	xtimer_ticks32_t last_wakeup = xtimer_now();

	while(1) {
		if(tick % 4 == 0) {
			debounce();
		}

		tick++;
		xtimer_periodic_wakeup(&last_wakeup, 1000);
	}
	return NULL;
}

int main(void)
{
	xtimer_init();
	//gpio_init(led, GPIO_OUT);
	gpio_init(usr_btn, GPIO_IN_PU);
	
	thread_create(ms_thread_stack, sizeof(ms_thread_stack),
			THREAD_PRIORITY_MAIN - 1, THREAD_CREATE_STACKTEST,
			ms_thread, NULL, "ms_thread");

	while(1) {
		//gpio_set(led);
		xtimer_msleep(100);
		//gpio_clear(led);
		xtimer_msleep(900);
		//printf("iuser_btn = %d\n", gpio_read(usr_btn));
	}
	return 0;
}
