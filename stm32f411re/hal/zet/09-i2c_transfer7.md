# i2c\_transfer7

```
void i2c_transfer7(uint32_t i2c, uint8_t addr, const uint8_t *w, size_t wn, uint8_t *r, size_t rn)
{
	//assert((wn == 0) || (rn == 0));
	if(rn>0) HAL_I2C_Master_Receive((I2C_HandleTypeDef *)i2c, addr<<1, r, rn, 100);
	if(wn>0) HAL_I2C_Master_Transmit((I2C_HandleTypeDef *)i2c, addr<<1, (uint8_t *)w, wn, 100);

}
```

Used in branch `ds3231`

STM32F411 and STM32L432 can transfer max 65535 data items.


## DMA

Following was use in branch `ds3231` for f411:

```
volatile uint32_t wait_time =0; // turns out to be 0
void i2c_transfer7(uint32_t i2c, uint8_t addr, const uint8_t *w, size_t wn, uint8_t *r, size_t rn)
{
	addr <<= 1;
	I2C_HandleTypeDef *phi2c = (I2C_HandleTypeDef *) i2c;
	HAL_I2C_StateTypeDef state;
	wait_time = HAL_GetTick();
	wait:
		state = HAL_I2C_GetState(phi2c);
		if(state == HAL_I2C_STATE_BUSY) goto wait;
		if(state == HAL_I2C_STATE_BUSY_TX) goto wait;
		if(state == HAL_I2C_STATE_BUSY_RX) goto wait;
		if(state != HAL_I2C_STATE_READY) goto wait; // probably only one needed
	wait_time = HAL_GetTick() - wait_time;
	//for(;;) {
	//	HAL_I2C_StateTypeDef state = HAL_I2C_GetState(phi2c);
	//	if((state & HAL_I2C_STATE_BUSY) == 0) break;
	//}
	//assert((wn == 0) || (rn == 0));
	if(rn>0) {
		HAL_I2C_Master_Receive(phi2c, addr, r, rn, 100);
	}
	HAL_StatusTypeDef status;
	if(wn>0) {
		//HAL_I2C_Master_Transmit((I2C_HandleTypeDef *)i2c, addr<<1, (uint8_t *)w, wn, 100);
		status = HAL_I2C_Master_Transmit_DMA(phi2c, addr, (uint8_t*) w, wn);
	}
	(void)status;

}
```

CubeMX:
* Set up DMA section. For ds3231 project, I had to Use Fifo, Threshold Full, Data Width Byte, Byrst Size Single
* In NVIC, enable I2C evt IRQ (!)

See also: db07.060

