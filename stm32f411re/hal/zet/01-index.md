gpio [07](07-gpio.md)\
* low-level [07a](07a-gpio-ll.md)

`HAL_TIM_PWM_PulseFinishedCallback()` [04](04-pwm.md) \
`HAL_TIM_PWM_PulseFinishedHalfCpltCallback()` [04](04-pwm.md)

`i2c_transfer7` [09](09-i2c_transfer7.md)


pwm [04](04-pwm.md)
