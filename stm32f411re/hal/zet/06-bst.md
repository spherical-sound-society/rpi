#bst

Clocks go forward to BST on last Sun of March, 
and back again to GMT on last Sun of Oct.

| Year | Mar | Oct |
|------|-----|-----|
| 2021 |  28 |  31 |
| 2022 |  27 |  30 |

