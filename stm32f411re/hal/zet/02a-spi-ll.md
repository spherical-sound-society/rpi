Assuming transmit-only master

Assuming NSS under hardware control, for one value only:
```
	LL_SPI_Enable(SPI1);
	while (!(SPI1->SR & SPI_SR_TXE));
	LL_SPI_TransmitData16(SPI1, val);
	while (SPI1->SR & SPI_SR_BSY);
	LL_SPI_Disable(SPI1);
```
For manual NSS, which seems to work fine:

In main: `LL_SPI_Enable(SPI1);`

And to transmit:

```
	LL_GPIO_SetOutputPin(GPIOA, GPIO_PIN_4 << 16);
	while (!(SPI1->SR & SPI_SR_TXE));
	LL_SPI_TransmitData16(SPI1, val);
	while (SPI1->SR & SPI_SR_BSY);
	LL_GPIO_SetOutputPin(GPIOA, GPIO_PIN_4);
```
