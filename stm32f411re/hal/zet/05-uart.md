see branch uart

## Output

Enter key is ASCII 13 (\r).

```
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

int output(const char *format, ...)
{
	char str[200];
	va_list ap;
	va_start(ap, format);
	int ret = vsnprintf(str, sizeof(str), format, ap);
	//vsprintf(msg, fmt, ap);
	va_end(ap);
	HAL_UART_Transmit(&huart2, (unsigned char*) str, strlen(str), 100);
	return ret;
}




HAL_StatusTypeDef HAL_UART_Transmit(UART_HandleTypeDef *huart, 
	uint8_t *pTxData, uint16_t Size, uint32_t Timeout);
```

## IRQ on RX

CubeMX:
* USART2 PA2 (TX), PA3 (RX) is Nucleo USB pins
* Mode: Asynchronous
* Enable USART2 global interrupt (for input)

Echoing input:
```
uint8_t rx;

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{

	if(huart->Instance==USART2)
	{
		output("Input: %c %d\r\n", rx, rx);
		HAL_UART_Receive_IT (&huart2, &rx, 1);
	}
}
```

In `main`:
```
	HAL_UART_Receive_IT (&huart2, &rx, 1);
```
