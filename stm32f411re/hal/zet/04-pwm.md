# PWM

1. [Mode](#Mode)
2. [Starting](#Starting)
3. [IRQ](#IRQ)
4. [DMA](#DMA)

## Mode

Mode 1 is "normal mode"

Mode 2 inverts the output (being on instead of off, and vice versa)



## Starting

In main:
```
  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
```

Low-level:
```
  LL_TIM_EnableCounter(TIM3);
  LL_TIM_CC_EnableChannel(TIM3, LL_TIM_CHANNEL_CH3);
```

## IRQ

Enable IRQ in GUI

In main:
```
  HAL_TIM_PWM_Start_IT(&htim2, TIM_CHANNEL_1);
```

Low-level:
```
  LL_TIM_EnableIT_UPDATE(TIM3);
```
  

ISR:
```
void HAL_TIM_PWM_PulseFinishedCallback(TIM_HandleTypeDef *htim)
{
	//static uint32_t tim2_vol = 0;
	if (htim->Instance == TIM2) {
		//__disable_irq();
		static uint32_t tim2_vol;
		cb_read(&tim2_vol);
		//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, SET);
		static int count;
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, ++count%16000 <  1000);
		//__enable_irq();
		//HAL_TIM_PWM_Start_DMA(&htim2, TIM_CHANNEL_1, &tim2_vol, 1);
		TIM2->CCR1 = 33; // set a new duty cycle, if required. "1" refers to channel number in this case

	}
}
```

NB No need to re-enable the ISR each time.

Low-level:
```
void TIM3_IRQHandler(void)
{
  /* USER CODE BEGIN TIM3_IRQn 0 */
	LL_GPIO_SetOutputPin(GPIOA, GPIO_PIN_7);

  /* USER CODE END TIM3_IRQn 0 */
  /* USER CODE BEGIN TIM3_IRQn 1 */
	LL_GPIO_SetOutputPin(GPIOA, GPIO_PIN_7 << 16);
	LL_TIM_ClearFlag_UPDATE(TIM3);
  /* USER CODE END TIM3_IRQn 1 */
}
```

## <a name="DMA">DMA</a>

CubeMX:

* Pinout view: PA0 `TIM5_CH1`
* Timers: TIM5:

  * Check Internal Clock
  * Channel1 -  PWM Generation CH1
  * Parameter Settings:

    * Prescaler - set it
    * Counter Period (AutoReload Register) - set it
    * auto-reload preload -  Enable
    * Pulse - doesn't need to be set. That's what DMA is for
  * DMA:

    * Direction: Memory to peripheral
    * Mode Circular, Use Fifo, Threshold Full, Burst Single (prolly unneeded)
    * Set data width according to data structure used

main() init:

```
  HAL_TIM_PWM_Start_DMA(&htim5, TIM_CHANNEL_1, (uint32_t *)pwmData,  LEN);
```

See branch:
* `pwm-dma` (2022-05-12 DMA direction is likely to be wrong, though)
* `pwm_dma_sin`

**Example 1:** Generate a 500Hz square wave "the hard way"

In CubeMX, follow the instructions above for pin PA0, 
setting Prescaler to 0, and Counter Period to 1000-1. 

Use the formula `X/an = f`, where X is the clock frequency of the timer, 
a is ARR+1, n is the number of elements in the array,
and f is the output frequency. 
Rearrange to give `X = fan` (assuming prescaler is 0). 

Using timing provided by `HAL_TIM_PWM_PulseFinishedCallback()`,
I determined that f = 500Hz when a = 1000, n = 64, giving X =32MHz.
This is very odd, because I was expecting it to be 100MHz.

Add the following to main():

```
#define LEN 64
	__attribute__ ((aligned (32)))
	volatile uint32_t  pwmData[LEN]; // dunno if volatile req'd

	for(uint32_t i = 0; i<LEN; i++) {
		pwmData[i] = 0;
	  	if(i>LEN/2) pwmData[i] = 1000; // ARR+1
	}

	HAL_TIM_PWM_Start_DMA(&htim4, TIM_CHANNEL_1, (uint32_t *)pwmData,  LEN);
```

**Example 2:** As example 1, but modulating the wave

To produce a ramp effect, you might choose to modify the loop to the following:

```
	int a = 32000000/LEN/500; // we want freq of 500Hz
	TIM4->ARR = a -1;
        for(uint32_t i = 0; i<LEN; i++) {
		float v = (float) i/ (float) LEN * (float) a;
                pwmData[i] = v;
        }

```

**DMA callbacks**

Write the following routines in main.c to perform actions when the transfer is complete:
```
void HAL_TIM_PWM_PulseFinishedCallback(TIM_HandleTypeDef* htim)
{
	// whatever
}
```
or half-complete:
```
void HAL_TIM_PWM_PulseFinishedHalfCpltCallback(TIM_HandleTypeDef* htim)
{
	// whatever
}
```

Updated: 2022-05-13
