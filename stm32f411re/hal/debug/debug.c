//#include "stm32f4xx_hal.h"
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "debug.h"

//extern SPI_HandleTypeDef hspi1;

extern UART_HandleTypeDef huart2;

typedef uint32_t u32;

void put32(u32 addr, u32 val)
{
	*(volatile u32*) addr = val;
}

u32 get32(u32 addr)
{
	return *(volatile u32*) addr;
}


int output(const char *format, ...)
{
	char str[200];
	va_list ap;
	va_start(ap, format);
	int ret = vsnprintf(str, sizeof(str), format, ap);
	//vsprintf(msg, fmt, ap);
	va_end(ap);
	HAL_UART_Transmit(&huart2, (unsigned char*) str, strlen(str), 100);
	return ret;
}

RCC_TypeDef rcc_old, rcc_new;
TIM_TypeDef tim3_old, tim3_new;

void dump_struct(uint32_t struct_addr, int size)
{
	uint32_t off = 0;
	output("DUMP\n");
	while(off < size) {
		uint32_t contents = get32(struct_addr+off);
		output("%x %x\n", off, contents);
		off += 4;
	}
}


void bin_out(uint32_t v)
{
	for(int i = 0; i< 32; i++) {
		char b = v & (1<<31) ? '1' : '0';
		output("%c", b);
		v <<= 1;
	}
}
void dump_nz(char* id, uint32_t start_addr, int size)
{
	uint32_t off = 0;
	output("DNZ\n%s\n", id);
	output("%4s 3 2         1         0\n", "");
	output("%4s 10987654321098765432109876543210\n", "");
	while(off < size) {
		uint32_t contents = get32(start_addr+off);
		if(contents) {
			output("%4x ", off);
			bin_out(contents);
			output("\n");
		}
		off += 4;
	}
	output(".\n");
}

void dump_many(void)
{
	output("\nDUMPMANY\n");
	dump_nz("GPIOA", (uint32_t) GPIOA, sizeof(GPIO_TypeDef));
	dump_nz("GPIOB", (uint32_t) GPIOB, sizeof(GPIO_TypeDef));
	dump_nz("GPIOC", (uint32_t) GPIOC, sizeof(GPIO_TypeDef));
	dump_nz("EXTI", (uint32_t) EXTI, sizeof(EXTI_TypeDef));
	//dump_nz("NVIC", (uint32_t) NVIC, 0x14); // it's trickier than that
	dump_nz("RCC", (uint32_t) RCC, sizeof(RCC_TypeDef));
	dump_nz("TIM3", (uint32_t) TIM3, sizeof(TIM_TypeDef));
	output(".\n");
}

void dump_struct_diff(char* id, uint32_t struct_new_addr, uint32_t struct_old_addr, int size)
{
	uint32_t off = 0;
	output("DIFF\n");
	output("%s\n", id);
	while(off < size) {
		uint32_t new_contents = get32(struct_new_addr+off);
		uint32_t old_contents = get32(struct_old_addr+off);
		if(new_contents != old_contents )
			output("%x %x %x\n", off, new_contents, old_contents);
		off += 4;
	}
}
