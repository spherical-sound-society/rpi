// vim: set tabstop=4 sts=4 sw=4 sta et  :

const regs = @import("registers");

const SystemCoreClock:u32 = 16_000_000; // MHz

export fn main() void {
    // PB5 is on TIM3, CH2 (channel), ALT 2

    // set up PB5 for PWM
    regs.RCC.AHB1ENR.modify(.{ .GPIOBEN = 1}); // Enable GPIOC clock
    regs.GPIOB.MODER.modify(.{ .MODER5 = 0b10});
    regs.GPIOB.AFRL.modify(.{ .AFRL5 = 2}); // PWM is alt fn 2 for PB5

    // set up the timer and channel
    regs.RCC.APB1ENR.modify(.{ .TIM3EN = 1}); // Enable TIM3 clock
    regs.TIM3.CR1.modify(.{ .ARPE = 1, .CEN = 1}); // enable auto-reload and counter
    const scale:u32 = 1_000_000;
    regs.TIM3.PSC.modify(.{ .PSC = SystemCoreClock/scale-1}); // once every 1us
    const freq = 500; // Hz
    const arr = scale/freq;
    regs.TIM3.ARR.modify(.{ .ARR_L= arr-1}); // autoreload value (NB some timers use ARR instead of ARR_L)
    regs.TIM3.CCR2.modify(.{ .CCR2_L = arr/2}); // NB CCR2_L likewise
    regs.TIM3.CCMR1_Output.modify(.{ .OC2PE = 1, .OC2M = 0b110});// enable preload channel 2, and pwm mode 1
    regs.TIM3.CCER.modify(.{ .CC2E = 1}); // enable Capture/Compare for channel 2

    while (true) {}
}

