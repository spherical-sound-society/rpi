// vim: set tabstop=4 sts=4 sw=4 sta et  :

const regs = @import("registers");

const GPIOA_baseAddr = 0x40020000;
const GPIOC_baseAddr = 0x40020800;

const Pin = struct {
    base_addr: u32,
    num: u32,
};

const PC13 = Pin{.base_addr = GPIOC_baseAddr, .num = 13};

const led = PC13;

fn put32(comptime addr: u32, value: u32) void {
    const ptr = @intToPtr(*volatile u32, addr);
    ptr.* = value;
}

fn gpio_clr(comptime pin: Pin) void {
    //var bsrr : *volatile u32 = @intToPtr(*volatile u32, pin.base_addr + 0x18);
    const bits: u32 = @intCast(u32, 1) <<  @intCast(u5, pin.num + 16);
    //bsrr.* = bits;
    put32(pin.base_addr + 0x18, bits);
}

fn gpio_set(comptime pin: Pin) void {
    //const bsrr = @intToPtr(*volatile u32, pin.base_addr + 0x18);
    //const bsrr = @intToPtr(*u32, pin.base_addr + 0x18);
    const bits: u32 = @intCast(u32, 1) <<  @intCast(u5, pin.num);
    //bsrr.* = bits;
    put32(pin.base_addr + 0x18, bits);
}

fn pause(ticks:u32) void {
        var i: u32 = 0;
        while (i < ticks) {
            asm volatile ("nop");
            i += 1;
        }
}

export fn main() void {
    regs.RCC.AHB1ENR.modify(.{ .GPIOCEN = 1 }); // Enable GPIOC port

    // Set pin 12/13/14/15 mode to general purpose output
    regs.GPIOC.MODER.modify(.{ .MODER12 = 0b01, .MODER13 = 0b01, .MODER14 = 0b01, .MODER15 = 0b01 });

    regs.GPIOC.BSRR.modify(.{ .BS12 = 1, .BS14 = 1 }); // Set pin 12 and 14 high

    while (true) {
        var leds_state = regs.GPIOC.ODR.read(); // Read the LED state
        // Set the LED output to the negation of the currrent output
        regs.GPIOC.ODR.modify(.{
                .ODR12 = ~leds_state.ODR12,
                //.ODR13 = ~leds_state.ODR13,
                .ODR14 = ~leds_state.ODR14,
                .ODR15 = ~leds_state.ODR15,
                });
        gpio_set(led);
        pause(1500000);
        gpio_clr(led);
        pause(10000);
    }
}

