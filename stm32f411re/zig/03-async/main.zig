// vim: set tabstop=4 sts=4 sw=4 sta et  :

const regs = @import("registers");

var bsrr_c = regs.GPIOC.BSRR;

var ticks: i32 = 0;

export fn sysTickHandler() void {
    ticks += 1;
}


var awaken_pc13: i32 = 0;

fn blink_pc13() void {
    while(true) {
        bsrr_c.modify(.{ .BS13 = 1 });
        awaken_pc13 = ticks + 950;
        suspend {}
        bsrr_c.modify(.{ .BR13 = 1 });
        awaken_pc13 = ticks + 50;
        suspend {}
    }
}

var awaken_pc14: i32 = 0;

fn blink_pc14() void {
    while(true) {
        bsrr_c.modify(.{ .BS14 = 1 });
        awaken_pc14 = ticks + 250;
        suspend {}
        bsrr_c.modify(.{ .BR14 = 1 });
        awaken_pc14 = ticks + 250;
        suspend {}
    }
}

export fn main() void {

    // set systick to fire every 1ms
    regs.STK.LOAD.modify(.{.RELOAD = 16_000_000/1000 -1}); // default clock is 16MHz
    regs.STK.VAL.modify(.{.CURRENT = 0});
    regs.STK.CTRL.modify(.{.CLKSOURCE = 1, .TICKINT = 1, .ENABLE = 1});

    // set PC13 and PC14 for output
    regs.RCC.AHB1ENR.modify(.{ .GPIOCEN = 1 }); // enable GPIOC port
    regs.GPIOC.MODER.modify(.{ .MODER13 = 0b01, .MODER14 = 0b01 });


    var frame13  = async blink_pc13();
    var frame14  = async blink_pc14();

    while(true) {
        if(ticks >= awaken_pc13) resume frame13; 
        if(ticks >= awaken_pc14) resume frame14; 
    }
        
}

