# Zig

To build an example project and flash:
```
cd 01-blink
zig build flash
```

If you don't want to flash straight away, you can do:
```
zig build # or
zig build bin
```
