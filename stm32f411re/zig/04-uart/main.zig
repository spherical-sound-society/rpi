// vim: set tabstop=4 sts=4 sw=4 sta et  :

const regs = @import("registers");
const SystemCoreClock: u32 = 16_000_000;

var ticks: i32 = 0;

export fn sysTickHandler() void {
    ticks += 1;
}

fn ugetchar() u8 {
    while(true) {
        var sr = regs.USART2.SR.read();
        if(sr.RXNE == 1) break;
    }
    var c = regs.USART2.DR.read();
    return @intCast(u8, c.DR);
}



fn uputchar(c : u8) void {
    while(true) {
        var sr = regs.USART2.SR.read();
        if(sr.TXE == 1) break;
    }
    regs.USART2.DR.modify(.{.DR = c});

}

fn uputs(str : []const u8) void {
    for (str) |c| {
        uputchar(c);
    }
    uputchar('\n');
}


export fn main() void {

    // set systick to fire every 1ms
    regs.STK.LOAD.modify(.{.RELOAD = SystemCoreClock/1000 -1}); // default clock is 16MHz
    regs.STK.VAL.modify(.{.CURRENT = 0});
    regs.STK.CTRL.modify(.{.CLKSOURCE = 1, .TICKINT = 1, .ENABLE = 1});

    // set pins PA2 (TX) PA3 (RX) for serial communication
    regs.RCC.AHB1ENR.modify(.{ .GPIOAEN = 1 });
    regs.GPIOA.MODER.modify(.{ .MODER2 = 0b10, .MODER3 = 0b10 }); // set PA2 and PA3 to alt mode
    regs.GPIOA.AFRL.modify(.{ .AFRL2 = 7, .AFRL3 = 7 }); // PA2 and PA3 to alt fn 7 for UART
    regs.RCC.APB1ENR.modify(.{ .USART2EN = 1 }); // enable USART2
    const uartdiv: u16 = SystemCoreClock/115200; // baud rate 115200
    regs.USART2.BRR.modify(.{ .DIV_Mantissa = uartdiv/16, .DIV_Fraction = uartdiv%16 });
    regs.USART2.CR1.modify(.{ .RE = 1, .TE = 1, .UE = 1 }); // receive, transmit, enable

    uputs("UART test - if you can read this, then it should be working!");
    uputs("Now type something, and I will echo it to serial");

    while(true) {
        var c : u8 = ugetchar();
        uputchar(c);
        if(c == '\r') uputchar('\n');
    }
        
}


