// vim: set tabstop=4 sts=4 sw=4 sta et  :

const regs = @import("registers");

export fn main() void {

    regs.RCC.AHB1ENR.modify(.{ .GPIOCEN = 1 }); // Enable GPIOC port
    regs.RCC.AHB1ENR.modify(.{ .GPIOAEN = 1 });

    // Set pin 12/13/14/15 mode to general purpose output
    regs.GPIOC.MODER.modify(.{ .MODER12 = 0b01, .MODER13 = 0b01, .MODER14 = 0b01, .MODER15 = 0b01 });
    regs.GPIOA.MODER.modify(.{ .MODER5 = 0b01});

    regs.GPIOC.BSRR.modify(.{ .BS12 = 1, .BS14 = 1 }); // Set pin 12 and 14 high

    while (true) {
        var leds_state = regs.GPIOC.ODR.read(); // Read the LED state
        // Set the LED output to the negation of the currrent output
        regs.GPIOC.ODR.modify(.{
                .ODR12 = ~leds_state.ODR12,
                .ODR13 = ~leds_state.ODR13,
                .ODR14 = ~leds_state.ODR14,
                .ODR15 = ~leds_state.ODR15,
                });

      var ledsa = regs.GPIOA.ODR.read();
        regs.GPIOA.ODR.modify(.{ .ODR5 = ~ledsa.ODR5}); 
        // Sleep for some time
        var i: u32 = 0;
        while (i < 600000) {
            asm volatile ("nop");
            i += 1;
        }
    }
}

