# systick

Demonstration of creating a Systick handler interrupt. 
PC13 is toggled every 0.5s in the interrupt itself, whilst PC14 is
turned off in the polling loop using a delay function.


## Started

2021-12-06	Started. Works
