// vim: set tabstop=4 sts=4 sw=4 sta et  :

const regs = @import("registers");

var bsrr_c = regs.GPIOC.BSRR; // register to turn the pins in GPIOC on or off

const SystemCoreClock: u32 = 16_000_000;

var ticks : u32 = 0;

export fn sysTickHandler() void {
    ticks +=1;

    // toggle PC13 every 500ms
    if(@mod(ticks, 500) == 0) {
        var pins = regs.GPIOC.ODR.read();
        if(pins.ODR13 == 1) {
            bsrr_c.modify(.{ .BR13 = 1});
        } else {
            bsrr_c.modify(.{ .BS13 = 1});
        }
    }
}


fn delay_ms(ms: u32) void {
    var start = ticks;
    while(ticks - start < ms) {}
}

export fn main() void {
    // setup systick to blink once every 1ms
    regs.STK.LOAD.modify(.{.RELOAD = SystemCoreClock/1_000 -1});
    regs.STK.VAL.modify(.{.CURRENT = 0});
    regs.STK.CTRL.modify(.{.CLKSOURCE = 1, .TICKINT = 1, .ENABLE = 1});

    // setup PC13 and PC14 as output pins
    regs.RCC.AHB1ENR.modify(.{ .GPIOCEN = 1 }); // Enable GPIOC port
    regs.GPIOC.MODER.modify(.{ .MODER13 = 0b01, .MODER14 = 0b01});


    while (true) {
        bsrr_c.modify(.{ .BS14 = 1}); // turn on PC14
        delay_ms(50); // little delay
        bsrr_c.modify(.{ .BR14 = 1}); // turn off PC14
        delay_ms(950); // little delay
    }
}

