#include <delay.h>
/* approx delay in ms */
void __attribute__((optimize("O0"))) delayish (uint32_t ms)
{    
	uint32_t i, j;
	for (i=0; i<ms; i++)
		for (j=0; j<1330; j++) // determined using logic analyser
			asm("nop");
}
