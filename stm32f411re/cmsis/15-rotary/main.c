#define STM32F411xE
#include "stm32f4xx.h"

#include <delay.h>
#include <gpio.h>
#include <i2c.h>
#include <ssd1306.h>



//#define DOWN	GPIOA, 0
#define LED	GPIOB, 9


typedef struct {
	GPIO_TypeDef *port; // assumes both on same port
       	uint32_t pin_minus;
	uint32_t pin_plus;
	//int count;
	int state;
} rotary_t;

void rotary_init(rotary_t* r, GPIO_TypeDef *port, uint32_t pin_minus, uint32_t pin_plus)
{
	r->port = port;
	r->pin_minus = pin_minus;
	r->pin_plus = pin_plus;
	//r->count = 0;
	r->state = 0;

	gpio_in(port, pin_minus);
	gpio_pullup(port, pin_minus);
	gpio_in(port, pin_plus);
	gpio_pullup(port, pin_plus);

}

int rotary_poll(rotary_t* r)
{
	//int new_state = (gpio_get(r->port, r->pin_minus) << 1) | gpio_get(r->port, r->pin_plus);
	int new_state = (gpio_get(r->port, r->pin_plus) << 1) | gpio_get(r->port, r->pin_minus);
	int delta = 0;
	if((r->state == 2) & (new_state == 3)) {
		delta = 1;
	}
	if((r->state == 1) && (new_state == 3)) {
		delta = -1;
	}
	r->state = new_state;

	return delta;

}


rotary_t dial;
volatile int val = 0;
uint32_t SystemCoreClock = 16000000;

void  SysTick_Handler(void)
{
	val += rotary_poll(&dial);
}


int main (void) 
{   
	gpio_out(LED);
	rotary_init(&dial, GPIOA, 0, 1);

	SysTick_Config(SystemCoreClock/1000); // set tick to every 1ms

	i2c1_init();
	init_display(64);
	int count = 0;

	while (1) 
	{
		ssd1306_printf("C:%d, Val = %d\n", count++, val);
		show_scr();
		delayish(1000);
	}
}
