#define STM32F411xE
#include "stm32f4xx.h"

/* Toggle PC10  or PC13 */
#define PILL // define for PC13 (blackpill), or comment out for Nulceo

int main (void) 
{   

	RCC->AHB1ENR	|= RCC_AHB1ENR_GPIOCEN; //RCC ON
#ifdef	PILL
	GPIOC->MODER	|= GPIO_MODER_MODER13_0; //mode out
#else	
	GPIOC->MODER	|= GPIO_MODER_MODER10_0; //mode out
#endif

	while (1) 
	{
#ifdef PILL	
		GPIOC->ODR ^=   GPIO_ODR_OD13;
#else
		GPIOC->ODR ^=   GPIO_ODR_OD10;
#endif		
		delayish(500); //delay in  ms
	}
}
