#include <stddef.h>
#include <i2c.h>

void i2c1_init(void)
{
	RCC->AHB1ENR	|= RCC_AHB1ENR_GPIOBEN;

	GPIO_TypeDef *GPIO = GPIOB; // unneeded abstraction in this case

	//PB6
	GPIO->MODER 	|= GPIO_MODER_MODER6_1; // Alt fn mode
	GPIO->AFR[0] 	|= (4 << GPIO_AFRL_AFSEL6_Pos) ; // Alt fn 4, being I2C
	GPIO->PUPDR |= (1 << GPIO_PUPDR_PUPD6_Pos); // pullup (may not be relevant)
	GPIO->OTYPER |= (1<< 6); // open drain. Important!
	// PB7
	GPIO->MODER 	|= GPIO_MODER_MODER7_1; // Alt fn mode 
	GPIO->AFR[0] 	|= (4 << GPIO_AFRL_AFSEL7_Pos) ; // alt fn 4 again
	GPIO->PUPDR |= (1 << GPIO_PUPDR_PUPD7_Pos); // pullup (may not be relevant)
	GPIO->OTYPER |= (1<< 7); // open drain Important!

	RCC->APB1ENR |= RCC_APB1ENR_I2C1EN;
	I2C1->CR2 = 16; // works
	I2C1->TRISE = 17;
	I2C1->CCR = 80;
	I2C1->CR1 |= I2C_CR1_PE;
}

void i2c1_send_data(int sid, uint8_t *data, int len) 
{
	// begin i2c transaction
	I2C1->CR1 |= I2C_CR1_START; // GENERATE A START CONDITION
	while (!(I2C1->SR1 & I2C_SR1_SB))
		;
	I2C1->DR = (sid << 1); // shift the sid
	while (!(I2C1->SR1 & I2C_SR1_ADDR))
		;
	I2C1->SR2;

	// send
	//bool with_btf = false;
	for (uint32_t i = 0; i < len; i++) {
		I2C1->DR = *(data + i);
		while (!(I2C1->SR1 & I2C_SR1_BTF)) {} ;
		//while(!(I2C1->SR1 & I2C_SR1_TXE));
		//if(with_btf)
		//              while(!(I2C1->SR1 & I2C_SR1_BTF)); // added mcarter 2020-10-10
	}

	// end i2c transaction
	I2C1->CR1 |= I2C_CR1_STOP;
}

void i2c_transfer7(uint32_t i2c, uint8_t addr, const uint8_t *w, size_t wn, uint8_t *r, size_t rn)
{
	//TODO incomplete
	i2c1_send_data(addr, (uint8_t *) w, wn);
	
}

