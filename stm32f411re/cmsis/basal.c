#include <basal.h>

// https://rosettacode.org/wiki/Random_number_generator_(included)
// for Batch
// generates 16-bit numbers 0 .. 32767
// suspiciously not random
uint16_t rand16()
{
        static uint32_t x = 0;
        x = (uint16_t) (x * 214013 + 2531011);
        return x;
}


