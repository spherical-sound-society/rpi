#define STM32F411xE
#include "stm32f4xx.h"
#include <delay.h>
#include <printf.h>
#include <uart.h>


int main (void) 
{
	puts("UART test of printf()");
	while (1) 
	{
		static int count = 0;
		//char txt[255];
		printf("Count = %d\n", count++);
		//snprintf(txt, sizeof(txt), "Count = %d", count++);
		//puts(txt);
		delayish(1000);
	}
}
