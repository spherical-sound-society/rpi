#define STM32F411xE
#include "stm32f4xx.h"

uint32_t SystemCoreClock = 16000000;

volatile uint32_t  ticks = 0; // must be volatile to prevent compiler optimisations

void  SysTick_Handler(void)
{
	ticks++;
	if(GPIOC->ODR &  GPIO_ODR_OD12) // is PC12 on?
		GPIOC->BSRR = GPIO_BSRR_BR12; // turn it off via a reset
	else
		GPIOC->BSRR = GPIO_BSRR_BS12; // turn it on via set
}

void delay_ms(int ms)
{
	uint32_t started = ticks;
	while((ticks-started)<=ms); // rollover-safe (within limits)
}

int main (void) 
{   
	RCC->AHB1ENR	|= RCC_AHB1ENR_GPIOCEN; //RCC on for GPIO C
	GPIOC->MODER	|= GPIO_MODER_MODER10_0; // PC10 mode out
	GPIOC->MODER	|= GPIO_MODER_MODER12_0; // PC12 mode out

	SysTick_Config(SystemCoreClock/1000); // set tick to every 1ms

	while(1) {
		GPIOC->BSRR = GPIO_BSRR_BS10; // PC10 on
		delay_ms(50);
		GPIOC->BSRR = GPIO_BSRR_BR10; // PC10 off
		delay_ms(950);
	}
}
