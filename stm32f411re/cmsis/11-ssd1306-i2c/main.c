#define STM32F411xE
#include "stm32f4xx.h"
#include <stdbool.h>

#include <delay.h>
#include <i2c.h>
#include <ssd1306.h>


int main(void)
{
	i2c1_init();
	init_display(64, (uint32_t) I2C1);
	int counter = 0;
	while(1) {
		ssd1306_printf("Counter = %d\n", counter++);
		show_scr();
		delayish(1000);
	}
}
