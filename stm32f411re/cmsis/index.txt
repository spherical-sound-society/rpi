delayish	01


dsp
	arm 14
	square waves 13


exti	09


fpu	12, 13


gpio_set	07


i2c 
	ledmat 06
	ssd1306 11


irq
	tim2 13, 14


NVIC_EnableIRQ 
	exti 09
	tim2 13, 14


pwm_init	14


pwm_set_duty	14


rotary		15


spi 
	zeroseg 05 
	zeroseg (non-blocking) 07


sine waves 14


square waves 13


tim2
	irq 13, 14


tim_init	14
