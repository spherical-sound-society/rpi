#define STM32F411xE
#include "stm32f4xx.h"

/*
 * LED: PA5
 * BUTTON: PB13 (sic)
 */


void EXTI15_10_IRQHandler(void)
{
	if(EXTI->PR & EXTI_PR_PR13) { // pin 13 has been triggered and is pending, so do something
		EXTI->PR = EXTI_PR_PR13; // reset the trigger

		if(GPIOB->IDR & GPIO_IDR_IDR_13) // pulled-up button released
			GPIOA->BSRR = GPIO_BSRR_BR5; // LED of
		else // button pressed
			GPIOA->BSRR = GPIO_BSRR_BS5; // LED on
	}		
}


int main (void) 
{
	// setup PA5 for output
	RCC->AHB1ENR	|= RCC_AHB1ENR_GPIOAEN; // enable port A
	GPIOA->MODER	|= GPIO_MODER_MODER5_0; // mode out

	// setup PB13 for pulled-up input
	RCC->AHB1ENR	|= RCC_AHB1ENR_GPIOBEN; // enable port B
	GPIOB->PUPDR	|= (1 << GPIO_PUPDR_PUPD13_Pos); // enable pullup
		 
	// setup SYSCFG interrupts
	RCC->APB2ENR |= (1 << RCC_APB2ENR_SYSCFGEN_Pos); // enable the SYSCFG peripheral first (required!)
	SYSCFG->EXTICR[3] |= SYSCFG_EXTICR4_EXTI13_PB; // EXTICR[3] is actually EXTICR4

	// setup EXTI registers
	EXTI->IMR 	|= EXTI_IMR_MR13; // Interrupt mask Register
	EXTI->RTSR	|= EXTI_RTSR_TR13; // detect rising 
	EXTI->FTSR	|= EXTI_FTSR_TR13; // detect falling
	NVIC_EnableIRQ(EXTI15_10_IRQn); // enables for quite a few pins, by the looks of it

	while (1); // superloop
}
