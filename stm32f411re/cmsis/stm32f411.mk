CC = arm-none-eabi-gcc
CXX = arm-none-eabi-g++
AS = arm-none-eabi-as
LD = arm-none-eabi-ld
BIN = arm-none-eabi-objcopy
CFLAGS += -mthumb -mcpu=cortex-m4 -ggdb 
CFLAGS += -O0 # turn off optimsations
CFLAGS += -mfloat-abi=hard
CFLAGS += -mfpu=fpv4-sp-d16 # doesn't seem to be necessary
CFLAGS += -nostdlib
CFLAGS += -fdata-sections -ffunction-sections # for unused code

POT = $(RPI)/pot
CFLAGS += -I$(POT)
VPATH += $(POT)

ROOT = $(RPI)/stm32f411re/cmsis
CFLAGS += -I$(ROOT)
VPATH += $(ROOT)


CMSIS = $(HOME)/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.2/Drivers/CMSIS
CFLAGS += -I$(CMSIS)/Device/ST/STM32F4xx/Include/
CFLAGS += -I$(CMSIS)/Include/
#CFLAGS += -fmax-errors=5
#CFLAGS += -D__FPU_PRESENT -DARM_MATH_CM4


# ARM FAST MATHS
CFLAGS += -I$(CMSIS)/DSP/Include
#VPATH += $(CMSIS)/DSP/Source/FastMathFunctions
#VPATH += $(CMSIS)/DSP/Source/CommonTables
#OBJS += arm_common_tables.o
#OBJS += arm_sin_f32.o

VPATH += ..
OBJS += startup_stm32f411retx.o main.o basal.o # delay.o # printf.o string.o i2c.o
#OBJS += uart.o printf.o
OBJS += gpio.o

ifdef USE_SSD1306
OBJS += ssd1306.o 
OBJS += printf.o string.o i2c.o
SSD1306_PATH = ../../../1306
VPATH += $(SSD1306_PATH)
CFLAGS += -I$(SSD1306_PATH)
endif

ifdef USE_LEDMAT
LEDMAT_PATH = ../../../8x8
CFLAGS += -I$(LEDMAT_PATH)
VPATH += $(LEDMAT_PATH)
OBJS += ledmat.o ledmat-bsp-f411.o i2c.o
endif

LDFLAGS = -T $(ROOT)/STM32F411RETX_FLASH.ld
#LDFLAGS += -mfloat-abi=hard -mfpu=fpv4-sp-d16
#LDFLAGS += -lm

all: app.bin

app.elf: $(OBJS)
	$(LD) $(LDFLAGS) -o app.elf $(OBJS) -L$(CMSIS)/Lib/GCC  -larm_cortexM4lf_math
	#strip --strip-unneeded app.elf # didnt help

app.bin: app.elf
	$(BIN) -O binary app.elf app.bin
	arm-none-eabi-objdump -d app.elf >app.txt

%.o : %.cc
	$(CXX) $(CFLAGS) -c -o $@ $<
	
%.o : %.c
	$(CC) $(CFLAGS) -c -o $@ $<

%.o : %.s
	$(AS) -o $@ $<

clean :
	rm -f *.o *.a .doc 
	rm -f app.bin app.elf app.txt

flash :
	st-flash --connect-under-reset write *bin 0x8000000
