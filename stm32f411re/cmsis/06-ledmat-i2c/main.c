#include <basal.h>

#include <stdbool.h>
#include <delay.h>
#include <i2c.h>
#include "ledmat.h"

static uint8_t pattern1[] = { 
	0b10000001, 
	0b01000010, 
	0b00100100, 
	0b00010000,
	0b00001000, 
	0b00100100, 
	0b01000010, 
	0b10000001 
};

static uint8_t pattern2[] = { // the letter P, with some orientation frills
	0b11110001, 
	0b10001000, 
	0b10001000, 
	0b11110000,
	0b10000000, 
	0b10000000, 
	0b10000001, 
	0b10000010 
};


void random_pattern(void)
{
	while(1) {
		uint16_t rc = rand16();
		uint8_t r = rc, c = rc >> 8;
		ledmat_toggle(r, c);
		ledmat_show();
	}
}

void scrolling_pattern(void)
{
	uint8_t* pattern = pattern1;
	while(1) {
		static int offset = 0; // for scrolling purposes
		for (int r = 0; r < 8; ++r) {
			int r1 = (r + offset) %8;
			led_set_row(r1, pattern[r]);
		}
		offset++;
		ledmat_show();
		delayish(100);
	}
}

int main(void)
{
	i2c1_init();
	ledmat_init(I2C1);

	if(1) 
		random_pattern();
	else
		scrolling_pattern();
}

