#define STM32F411xE
#include "stm32f4xx.h"

#include "FreeRTOS.h"
//#include "FreeRTOSConfig.h"
#include "task.h"

#include "gpio.h"


//#define NULL 0

uint32_t SystemCoreClock = 16000000;

#define LED1 GPIOB, 9
void Task1( void *pvParameters )
{
	gpio_out(LED1);
	while(1) {
		gpio_toggle(LED1);
		vTaskDelay(500/portTICK_PERIOD_MS);

	}
}


#define LED2 GPIOB, 5
void Task2( void *pvParameters )
{
	gpio_out(LED2);
	while(1) {
		gpio_set(LED2);
		vTaskDelay(50/portTICK_PERIOD_MS);
		gpio_clr(LED2);
		vTaskDelay(950/portTICK_PERIOD_MS);
	}
}


int main (void) 
{   
	xTaskCreate(Task1, "Task1", 400, NULL, tskIDLE_PRIORITY, NULL);
	xTaskCreate(Task2, "Task2", 400, NULL, tskIDLE_PRIORITY, NULL);
	
	vTaskStartScheduler();

}
