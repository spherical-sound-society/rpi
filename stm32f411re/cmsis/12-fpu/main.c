#define STM32F411xE
#include "stm32f4xx.h"

#include <math.h>

#include <stdbool.h>

#include <delay.h>

float fpu1(float a, float b)
{
	volatile float c =  a*b;
	return c;
}

int fpu2(int a, float b) { return a*b; }

#ifdef SQRT_WORKS // it doesn't work
float sqrtf(float);
float fpu3(float x) { return sqrtf(x); }
#else
float fpu3(float x) { return 666; }
#endif

void pause() {} // acts as a breakpoint

void loop()
{
	volatile float v1a  = fpu1(3.3333, 3.3333);
	volatile int v1b = v1a; // yields 11
	volatile int v2a = fpu2(3, 3.3333); // yields 9
	volatile int v2b = fpu2(3, 3.33334); // yields 10
	volatile float v3 = fpu3(2);
	pause();
	//delayish(1);
}

int main(void)
{
	SCB->CPACR |= (0xF<<20); // turn on FPU
	while(1) {
		loop();
	}

}

