# gpio

Taken from Figure 6, RM0383 p150:
```
AF0     system
AF1     TIM1/TIM2
AF2     TIM3..5
AF3     TIM9..11
AF4     I2C1..3
AF5     SPI1..4
AF6     SPI3..5
AF7     USART1..2
AF8     USART6
AF9     I2C2..3
AF10    OTG_FS
AF11    N/A
AF12    SDIO
AF13    N/A
AF14    N/A
AF15    EVENTOUT
```

Pin capabilities are listed in the Datasheet ([PDF](https://www.st.com/resource/en/datasheet/stm32f411re.pdf))
