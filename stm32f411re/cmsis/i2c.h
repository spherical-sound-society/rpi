#pragma once

#define STM32F411xE
#include "stm32f4xx.h"

#include <inttypes.h>

void i2c1_init(void);
void i2c1_send_data(int sid, uint8_t *data, int len);
