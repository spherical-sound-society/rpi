#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/* approx delay in ms */
void __attribute__((optimize("O0"))) delayish (uint32_t ms);

#ifdef __cplusplus
}
#endif

