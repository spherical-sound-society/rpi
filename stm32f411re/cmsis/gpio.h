#pragma once

#define STM32F411xE
#include "stm32f4xx.h"


void gpio_alt(GPIO_TypeDef *port, uint32_t pin, uint8_t alt_fn);
void gpio_out(GPIO_TypeDef *port, uint32_t pin);
void gpio_in(GPIO_TypeDef *port, uint32_t pin);
void gpio_pullup(GPIO_TypeDef *port, uint32_t pin);


static inline void gpio_set(GPIO_TypeDef *port, uint32_t pin)
{
	port->BSRR = (1 << pin);
}


static inline void gpio_clr(GPIO_TypeDef *port, uint32_t pin)
{
	port->BSRR = (1 << (pin+16));
}


static inline void gpio_toggle(GPIO_TypeDef *port, uint32_t pin)
{
	if(port->ODR & (1<<pin))
		gpio_clr(port, pin);
	else
		gpio_set(port, pin);
}

static inline int gpio_get(GPIO_TypeDef *port, uint32_t pin)
{
        return ((port->IDR) & (1<<pin)) ? 1 : 0;
}

static inline void gpio_put(GPIO_TypeDef *port, uint32_t pin, int val)
{
        if(val) 
                gpio_set(port, pin);
        else
                gpio_clr(port, pin);
}


