#define STM32F411xE
#include "stm32f4xx.h"

#include <string.h>

#include "gpio.h"

uint32_t SystemCoreClock = 16000000;

int main (void) 
{   
	// generated using pwm.go

	// PB5 PWM config, freq 500Hz, duty cycle 50% TIM3_CH1, AF2
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN; // Enable port B clock
	gpio_alt(GPIOB, 5, 2); // PB5 PWM alt fn
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN; // enable time clock
	TIM3->CR1 |= TIM_CR1_ARPE; // buffer the ARR register
	TIM3->CR1 |= TIM_CR1_CEN; // enable the counter
	TIM3->PSC = SystemCoreClock/1000000-1; // scale to 1us
	TIM3->ARR = 1000000/400-1; // set freq to 500 Hz
	TIM3->CCR2 = (TIM3->ARR +1) * 50/100 -1; // Duty cycle on Cap/Compare Reg
	TIM3->CCMR1 |= TIM_CCMR1_OC2PE; // enable preload for channel 2
	TIM3->CCMR1 |= (0b110 << TIM_CCMR1_OC2M_Pos); // PWM mode 1
	TIM3->CCER |= TIM_CCER_CC2E; // Enable Capture/Compare for channel 2

#if 0
	// PB6 PWM config, freq 600Hz, duty cycle 40%
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN; // Enable port B clock
	gpio_alt(GPIOB, 6, 2); // PB6 PWM alt fn
	RCC->APB1ENR |= RCC_APB1ENR_TIM4EN; // enable time clock
	TIM4->CR1 |= TIM_CR1_ARPE; // buffer the ARR register
	TIM4->CR1 |= TIM_CR1_CEN; // enable the counter
	TIM4->PSC = SystemCoreClock/1000000-1; // scale to 1us
	TIM4->ARR = 1000000/600-1; // set freq to 600 Hz
	TIM4->CCR1 = (TIM4->ARR +1) * 40/100 -1; // Duty cycle on Cap/Compare Reg
	TIM4->CCMR1 |= TIM_CCMR1_OC1PE; // enable preload for channel 1
	TIM4->CCMR1 |= (0b110 << TIM_CCMR1_OC1M_Pos); // PWM mode 1
	TIM4->CCER |= TIM_CCER_CC1E; // Enable Capture/Compare for channel 1
#endif

#if 0
	// PA1 PWM config, freq 600Hz, duty cycle 40%. TIM2_CH2. AF1
	// doesn't seem to work though
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN; 
	gpio_alt(GPIOA, 1, 1); // PB6 PWM alt fn
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN; // enable time clock
	TIM2->CR1 |= TIM_CR1_ARPE; // buffer the ARR register
	TIM2->CR1 |= TIM_CR1_CEN; // enable the counter
	TIM2->PSC = SystemCoreClock/1000000-1; // scale to 1us
	TIM2->ARR = 1000000/600-1; // set freq to 600 Hz
	TIM2->CCR1 = (TIM2->ARR +1) * 40/100 -1; // Duty cycle on Cap/Compare Reg
	TIM2->CCMR1 |= TIM_CCMR2_OC2PE; // enable preload for channel 1
	TIM2->CCMR1 |= (0b110 << TIM_CCMR2_OC2M_Pos); // PWM mode 1
	TIM2->CCER |= TIM_CCER_CC2E; // Enable Capture/Compare for channel 1
#endif

#if 1 // now works
	// PA0 PWM config, freq 600Hz, duty cycle 40%. TIM5_CH1. AF2
	// doesn't seem to work though
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN; 
	gpio_alt(GPIOA, 0, 2); // PB6 PWM alt fn
	RCC->APB1ENR |= RCC_APB1ENR_TIM5EN; // enable time clock
	memset(TIM5, 0, sizeof(TIM5)); // seems to be important
	TIM5->CR1 |= TIM_CR1_ARPE; // buffer the ARR register
	TIM5->PSC = SystemCoreClock/1000000-1; // scale to 1us
	uint32_t arr = 1000000/500-1;
	TIM5->ARR = arr; // set freq to 600 Hz
	uint32_t duty =  (arr+1) * 40 /100 -1;
	TIM5->CCR1 = duty; // Duty cycle on Cap/Compare Reg
	//TIM5->CCR1 = 1250; // didn't help
	TIM5->CCMR1 |= (0b110 << TIM_CCMR1_OC1M_Pos); // PWM mode 1
	//TIM5->CCR1 = 1250;
	TIM5->CCMR1 |= TIM_CCMR1_OC1PE; // enable preload for channel 1
	TIM5->CCER |= TIM_CCER_CC1E; // Enable Capture/Compare for channel 1
	TIM5->CR1 |= TIM_CR1_CEN; // enable the counter
	TIM5->CNT = 0;
	TIM5->EGR |= TIM_EGR_UG; // AHA - THIS SEEMS CRUCIAL. Uh, maybe not
	volatile TIM_TypeDef * tim5 = TIM5; // for debugging purposes
#endif
	
	while (1);
}
