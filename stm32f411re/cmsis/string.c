//#include <string.h>


#include <inttypes.h>
#include <stddef.h>

void *memset(void* s, int c, size_t n)
{
	while(n--) *((uint8_t*) s+n) = c;
	return s;
}

// Source:
// https://www.programmerall.com/article/63481119791/
void *memmove(void *dest, void const *src, size_t n)
{
	char *dp = dest;
	char *sp = (char*) src;
	if (dp < sp)
	{
		while(n-- > 0)
			*dp++ = *sp++;
	}
	else
	{
		// we should do the copy reversely
		dp += n;
		sp += n;
		while (n-- > 0)
			*--dp = *--sp;
	}
	return dest;
}


// source:
// https://www.tutorialspoint.com/write-your-own-memcpy-in-c
void *memcpy(void *dest, void *src, size_t n) {
	int i;
	//cast src and dest to char*
	char *src_char = (char *)src;
	char *dest_char = (char *)dest;
	for (i=0; i<n; i++)
		dest_char[i] = src_char[i]; //copy contents byte by byte
	return dest;
}

//void _putchar(char c) {}

