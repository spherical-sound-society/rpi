#define STM32F411xE
#include "stm32f4xx.h"

#include <math.h>

#include <stdbool.h>

#include <delay.h>
#include <gpio.h>

const float dt = 1000000.0/40000.0; // time increment (40kHz)
float t =0; // time, modulo the wave
const float t2 = 1000000.0/440; // period of 440Hz
const float t1 = t2 *0.25;// duty cycle of 25%

void TIM2_IRQHandler(void)
{
	if(TIM2->SR & TIM_SR_UIF)
		TIM2->SR = ~TIM_SR_UIF; // clear flag
	//gpio_toggle(GPIOC, 3);
	t += dt; 
	if(t>t2) t -= t2; // modulus the time period
	if(t>t1)
		GPIOC->ODR &= ~GPIO_ODR_OD3; // PC3 off
	else
		GPIOC->ODR |= GPIO_ODR_OD3; // PC3 on
}

void tim2_init()
{
	// TIM2->ARPRE somewhere ??
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN; // enable TIM2 clock
	TIM2->PSC = 16000000/1000000-1; // pre-scale to 1us
	TIM2->ARR = 1000000/40000-1; //40kHz Auto Reload Value
	TIM2->EGR |= TIM_EGR_UG; // generate update even to reset timer and apply settings
	TIM2->CR1 |= TIM_CR1_CEN; // enable timer counter
	TIM2->DIER |= TIM_DIER_UIE; // enable update interrupt
	NVIC_EnableIRQ(TIM2_IRQn);
}


int main(void)
{
	SCB->CPACR |= (0xF<<20); // turn on FPU
	tim2_init();
	gpio_out(GPIOC, 3);

	while(1);

}

