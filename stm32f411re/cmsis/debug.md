# debug

```
sudo openocd -f interface/stlink-v2.cfg -f target/stm32f4x.cfg
```

In a separate terminal:
```
gdb-multiarch


cat > .gdbinit <<EOI
# quit without confirmation
define hook-quit
    set confirm off 
end

file app.elf
target remote localhost:3333
load
monitor reset init
echo Type c to continue...\n
EOI

```
