#pragma once

#ifndef STM32F4
#define STM32F4
#endif

#include <inttypes.h>

extern uint32_t  SystemCoreClock; // should probably use rcc_get_timer_clk_freq() or similar instead


static inline void __attribute__((optimize("O0"))) nop(void)
{
	//asm volatile("nop");
	__asm__ __volatile__("nop" );
}

void __attribute__((optimize("O0"))) nops(uint32_t n);
