//#include <stdio.h>
//#include <stdarg.h>

#include <gpio.h>
#include <debounce.h>
#include <mal.h>
#include <delay.h>
#include <i2c.h>
#include <ssd1306.h>
#include <tone.h>

#define BTNY	GPIOA, GPIO1 // yellow button
deb_t sw_yellow = {0};

uint32_t tick = 0;
uint32_t resettable_tick = 0;
volatile uint32_t stop_tone_tick = 0;

void display_secs(void)
{
	//setCursorx(0);
	//setCursory(32);
	int prev_secs = -1;
	int secs = resettable_tick/1000;
	secs = secs % 60;

	if(secs == 0 && prev_secs !=0) {		
		tone5(400);
		stop_tone_tick = tick +250;
	}
	if(secs == 45 && prev_secs !=45) {
		tone5(800);
		stop_tone_tick = tick +250;
		//no_tone5();
	}

	//char msg[20];
	ssd1306_printf_at(4, 0, 32, "%02d", secs);

	prev_secs = secs;
	//print_str_at(msg, 4, 0, 32);
	
}

void display_exercise_number(void)
{
	int num = tick / 1000/60+1;
	ssd1306_printf_at(4, 0, 0, "x%d", num);
}

void sys_tick_handler(void) // mandatory ISR name
{
	
	if(tick % 1000 == 0) {
		display_secs();
		display_exercise_number();
		//gpio_toggle(LED1);
		//gpio_toggle(LED2);
	}

	//if(tick % 200 == 0) show_scr();
	
	if(stop_tone_tick == tick) no_tone5();


	tick++;
	resettable_tick++;

	if(tick % 4 == 0) {
		deb_update(&sw_yellow, gpio_get(BTNY));
		if(deb_falling(&sw_yellow)) resettable_tick =0;
	}
}

int main(void)
{
	i2c1_init();
	init_display(64, I2C1);
	gpio_inpull(BTNY);
	sw_yellow.en = 1;
	mal_init_systick();
	//int counter = 0;
	//tone5(400);
	while(1) {
		//ssd1306_printf("Counter = %d\n", counter++);
		show_scr();
		delayish(200);
	}

}
