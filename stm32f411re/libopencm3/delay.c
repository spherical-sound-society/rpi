#include <delay.h>

#include <libopencm3/stm32/rcc.h>


/** @brief Delay for approx time measured in ms
Calibrated for an STMF411
*/


void __attribute__((optimize("O0"))) delayish(uint32_t ms)
{
	static int approx_1ms = -1; // uninitialised
	if(approx_1ms == -1) 
		approx_1ms = 16*1000 / (rcc_ahb_frequency/1000000);  

	for(uint32_t j=0; j<ms; ++j) {
		for (uint32_t i = 0; i < approx_1ms; i++) {
			__asm__("nop");
		}
	}
}
