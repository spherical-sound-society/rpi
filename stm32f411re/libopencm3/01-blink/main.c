#include <conf.h>
#include <delay.h>
#include <gpio.h>

//#include <inttypes.h>
//#include <libopencm3/stm32/rcc.h>
//#include <libopencm3/stm32/gpio.h>
//#include <libopencm3/cm3/systick.h>
//#include <libopencm3/stm32/timer.h>
//#include <libopencm3/stm32/spi.h>



#define LED1	GPIOC, GPIO13 // blackpill
#define LED2	GPIOA, GPIO5 // Nucleo board

int main(void)
{
	gpio_out(LED1);
	gpio_out(LED2);

	while (1)
	{
		gpio_set(LED1);
		gpio_set(LED2);
		delayish(100);

		gpio_clear(LED1);
		gpio_clear(LED2);
		delayish(900);
	}
}
