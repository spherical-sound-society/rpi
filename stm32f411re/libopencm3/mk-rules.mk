PREFIX	?= arm-none-eabi-
CC	= $(PREFIX)gcc
CXX	= $(PREFIX)g++
LD	= $(PREFIX)ld
LD	= $(PREFIX)gcc
OBJCOPY	= $(PREFIX)objcopy
OBJDUMP	= $(PREFIX)objdump

LDFLAGS = -T ../linker.ld

OPENCM3_INC = $(OPENCM3_DIR)/include
POT = $(RPI)/pot

MAL = $(RPI)/stm32f411re/libopencm3 # My Abstraction Layer

#CFLAGS += -I..
CFLAGS += -I$(OPENCM3_INC)
CFLAGS += -I$(MAL)
CFLAGS += -I$(POT)

CPU = -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 
CFLAGS += $(CPU)
#CFLAGS += -mthumb -mcpu=cortex-m4
#CFLAGS += -mfloat-abi=hard
#CFLAGS += -mfpu=fpv4-sp-d16 # doesn't seem to be necessary
CFLAGS += -nostdlib
CFLAGS += -fdata-sections -ffunction-sections # for unused code

CFLAGS += -ggdb -O0

VPATH += $(MAL):$(POT)
#VPATH += ..
#CFLAGS += -DSTM32L4

#PROJECT = app
#BUILD_DIR ?= bin
#OPT ?= -Os
#CSTD ?= -std=c11
#DEVICE=stm32l432kc
#BUILD_DIR = build



# Be silent per default, but 'make V=1' will show all compiler calls.
# If you're insane, V=99 will print out all sorts of things.
V?=0
V=1 # added by mcarter
ifeq ($(V),0)
Q	:= @
NULL	:= 2>/dev/null
endif

#newlib stuff
NEWLIBFLAGS += -Wl,--start-group -u _printf_float -lc -lgcc -lnosys -Wl,--end-group
LIBS = -L$(OPENCM3_DIR)/lib -lopencm3_stm32f4  $(NEWLIBFLAGS)

#POT = $(RPI)/pot

#OOCD	?= openocd

OBJS += main.o conf.o delay.o gpio.o mal.o newlib.o

#OBJS += stdio.o uart.o vsnprintf.o string.o # required for uart

ifdef USE_SSD1306
OBJS += ssd1306.o i2c.o
endif



all: app.bin

app.elf : $(OBJS)
	#$(Q) $(LD) $(LDFLAGS) -L/home/pi/libopencm3/libopencm3/lib -specs=nano.specs -nostartfiles  $(OBJS) -lopencm3_stm32f4 $(NEWLIBFLAGS) -o app.elf
	#arm-none-eabi-gcc -T../linker.ld -L/home/pi/libopencm3/libopencm3/lib -nostartfiles -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -specs=nano.specs -Wl,--gc-sections -L/home/pi/libopencm3/libopencm3/lib main.o   -lopencm3_stm32f4 -Wl,--start-group -lc -lgcc -lnosys -Wl,--end-group -o app.elf
	#arm-none-eabi-gcc -T../linker.ld -nostartfiles $(CPU) -specs=nano.specs -Wl,--gc-sections -L/home/pi/libopencm3/libopencm3/lib $(OBJS)  -lopencm3_stm32f4 -Wl,--start-group -lc -lgcc -lnosys -Wl,--end-group -o app.elf
	arm-none-eabi-gcc -T../linker.ld -nostartfiles $(CPU) -specs=nano.specs -Wl,--gc-sections  $(OBJS) $(LIBS) -o app.elf



app.bin: app.elf
	$(Q) $(OBJCOPY) -O binary app.elf app.bin
	$(Q) $(OBJDUMP) -d app.elf >app.dis

%.o : %.c
	$(Q) $(CC) $(CFLAGS) -c $^ -o $@



test :
	echo $(OBJS)
	
clean :
	rm -rf *.o *.elf *.dis *.bin build/ .flash
	
.flash : app.bin
	touch .flash
	
flash : .flash
	#st-flash erase
	st-flash --connect-under-reset write *bin 0x8000000	
