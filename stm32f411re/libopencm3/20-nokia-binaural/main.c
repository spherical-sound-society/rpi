#include <conf.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
//#include <libopencm3/stm32/timer_common_all.h>
#include <libopencm3/stm32/timer.h>
//#include <libopencm3/stm32/spi.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/systick.h>
#include <stdlib.h>

#include <gpio.h>
#include <delay.h>
#include <debounce.h>
#include <mal.h>
#include <notes.h>

typedef uint32_t u32;

// PA0 TIM5_CH1. AF2

#define LED1	GPIOC, GPIO13 // blackpill
#define LED2	GPIOA, GPIO5 // Nucleo board

#define DBG	GPIOC, GPIO3  // a debug pin for testing purposes

#define SPK	GPIOA, GPIO0

#define BTNY	GPIOA, GPIO1 // yellow button
deb_t sw_yellow = {0};

uint32_t tick = 0;
const u32 fs = 44100/2; // sampling frequency of wave
volatile float dt; // time increment (1/fs), set in tone()
volatile float t0 = 0, t1 = 0; // time
volatile float fw_t0; // freq of wave
volatile float fw_t1; // freq of offset wave

void sys_tick_handler(void) // mandatory ISR name
{
	tick++;
	if(tick % 4 == 0) {
		deb_update(&sw_yellow, gpio_get(BTNY));
		//gpio_toggle(LED1);
		//gpio_toggle(LED2);
	}
}

/* takes about 57% of CPU when fs = 22.2kHz */
void tim5_isr(void)
{
	timer_clear_flag(TIM5, TIM_SR_UIF); // hmmm, seems to matter that it's at the top

	gpio_set(DBG);

	bool hi0 = false;
	if(t0>fw_t0) (t0 -= fw_t0);
	if(t0>= 0.5*fw_t0) hi0 = true;
	t0 += dt;

	bool hi1 = false;
	if(t1>fw_t1) (t1 -= fw_t1);
	if(t1>= 0.5*fw_t1) hi1 = true;
	t1 += dt;

	if(hi0 || hi1)
		gpio_set(SPK);
	else
		gpio_clear(SPK);

	gpio_clear(DBG);
}



//const uint32_t timer_peri = TIM5; // timer peripheral
//const enum tim_oc_id oc_id = TIM_OC1; // output compare channel designator

/* 
   Nokia Tune
   Connect a piezo buzzer or speaker to pin 11 or select a new pin.
   More songs available at https://github.com/robsoncouto/arduino-songs                                            

   Robson Couto, 2019
   */


// change this to make the song slower or faster
// higer value of tempo implies faster
int tempo = 180*2; // mcarter: original tempo was 180

// change this to whichever pin you want to use
int buzzer = 11;

// notes of the moledy followed by the duration.
// a 4 means a quarter note, 8 an eighteenth , 16 sixteenth, so on
// !!negative numbers are used to represent dotted notes,
// so -4 means a dotted quarter note, that is, a quarter plus an eighteenth!!
int nokia[] = {

	// Nokia Ringtone 
	// Score available at https://musescore.com/user/29944637/scores/5266155

	NOTE_E5, 8, NOTE_D5, 8, NOTE_FS4, 4, NOTE_GS4, 4, 
	NOTE_CS5, 8, NOTE_B4, 8, NOTE_D4, 4, NOTE_E4, 4, 
	NOTE_B4, 8, NOTE_A4, 8, NOTE_CS4, 4, NOTE_E4, 4,
	NOTE_A4, 2, 
};

int pacman[] = {

	// Pacman
	// Score available at https://musescore.com/user/85429/scores/107109
	NOTE_B4, 16, NOTE_B5, 16, NOTE_FS5, 16, NOTE_DS5, 16, //1
	NOTE_B5, 32, NOTE_FS5, -16, NOTE_DS5, 8, NOTE_C5, 16,
	NOTE_C6, 16, NOTE_G6, 16, NOTE_E6, 16, NOTE_C6, 32, NOTE_G6, -16, NOTE_E6, 8,

	NOTE_B4, 16,  NOTE_B5, 16,  NOTE_FS5, 16,   NOTE_DS5, 16,  NOTE_B5, 32,  //2
	NOTE_FS5, -16, NOTE_DS5, 8,  NOTE_DS5, 32, NOTE_E5, 32,  NOTE_F5, 32,
	NOTE_F5, 32,  NOTE_FS5, 32,  NOTE_G5, 32,  NOTE_G5, 32, NOTE_GS5, 32,  NOTE_A5, 16, NOTE_B5, 8
};





void noTone(void)
{
	timer_disable_irq(TIM5, TIM_DIER_UIE);
	//timer_disable_counter(timer_peri);
}


void delay(int ms) { delayish(ms); }

void tone(int freq, int duration)
{
	timer_disable_irq(TIM5, TIM_DIER_UIE);
	dt = 1.0/(float)fs;
	t0 = 0;
	t1 = 0;
	fw_t0 = 1.0/(float)freq;
	fw_t1 = 1.0/(8.0+(float)freq);
	timer_enable_irq(TIM5, TIM_DIER_UIE);
	delay(duration);
}

void play_tune(int* melody, int array_size, float tempo)
{
	// sizeof gives the number of bytes, each int value is composed of two bytes (16 bits)
	// there are two values per note (pitch and duration), so for each note there are four bytes
	int notes = array_size / sizeof(int) / 2;
	
	// this calculates the duration of a whole note in ms
	float wholenote = (60000 * 4) / tempo;

	//float divider = 0, noteDuration = 0;
	// iterate over the notes of the melody.
	// Remember, the array is twice the number of notes (notes + durations)
	for (int thisNote = 0; thisNote < notes * 2; thisNote = thisNote + 2) {
		float noteDuration = wholenote/melody[thisNote+1];
		if(noteDuration < 0) noteDuration *= -1.5; // increases duration in half for dotted notes
		tone(melody[thisNote], noteDuration * 0.9); // only 90%, to leave pause
		delay(noteDuration*0.1); // the pause
		noTone();
	}
}


int main(void)
{
	gpio_out(DBG);
	gpio_out(LED1);
	gpio_out(LED2);
	gpio_out(SPK);

	mal_timer_init(TIMER5, fs);

	gpio_inpull(BTNY);
	sw_yellow.en = 1;

	systick_set_frequency(1000, rcc_ahb_frequency);
	systick_counter_enable();
	systick_interrupt_enable();

	play_tune(nokia, sizeof(nokia), 180*4);

	for(;;) {
		if(deb_falling(&sw_yellow))
			play_tune(pacman, sizeof(pacman), 180.0*1.5);
		delayish(100);
	}

}
