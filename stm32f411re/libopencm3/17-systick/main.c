#include <conf.h>
//#include <inttypes.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/gpio.h>
//#include <libopencm3/stm32/timer.h>
//#include <libopencm3/stm32/spi.h>

#include <delay.h>
#include <gpio.h>
#include <mal.h>


#define LED1	GPIOC, GPIO13 // blackpill
#define LED2	GPIOA, GPIO5 // Nucleo board


uint32_t tick = 0;

void sys_tick_handler(void) // mandatory ISR name
{
	tick++;
	if(tick % 1000 == 0) {
		gpio_toggle(LED1);
		gpio_toggle(LED2);
	}
}

int main(void)
{
	gpio_out(LED1);
	gpio_out(LED2);

#if 0
	systick_set_frequency(1000, rcc_ahb_frequency);
	systick_counter_enable();
	systick_interrupt_enable();
#else
	mal_init_systick();
#endif


	while (1);
}
