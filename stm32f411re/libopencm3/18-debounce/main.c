#include <conf.h>
//#include <inttypes.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/gpio.h>
//#include <libopencm3/stm32/timer.h>
//#include <libopencm3/stm32/spi.h>

#include <delay.h>
#include <gpio.h>
#include <debounce.h>


#define LED1	GPIOC, GPIO13 // blackpill
#define LED2	GPIOA, GPIO5 // Nucleo board

#define BTNY	GPIOA, GPIO1 // yellow button
deb_t sw_yellow = {0};

uint32_t tick = 0;

void sys_tick_handler(void) // mandatory ISR name
{
	tick++;
	if(tick % 4 == 0) {
		deb_update(&sw_yellow, gpio_get(BTNY));
		//gpio_toggle(LED1);
		//gpio_toggle(LED2);
	}
}

int main(void)
{
	gpio_out(LED1);
	gpio_out(LED2);
	gpio_inpull(BTNY);
	sw_yellow.en = 1;

	systick_set_frequency(1000, rcc_ahb_frequency);
	systick_counter_enable();
	systick_interrupt_enable();

	while (1) {
		if(deb_falling(&sw_yellow)) gpio_toggle(LED2);
		/*
		if(gpio_get(BTNY))
			gpio_clear(LED2);
		else
			gpio_set(LED2);
			*/
	}
}
