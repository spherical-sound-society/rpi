#include <conf.h>
#include <delay.h>
#include <gpio.h>
#include <stdio.h>


#define LED1	GPIOC, GPIO13 // blackpill
#define LED2	GPIOA, GPIO5 // Nucleo board

int main(void)
{
	gpio_out(LED1);
	gpio_out(LED2);

	puts("\nfloat mulitplication test");
	for(float i = 0.0; i <1.1; i += 0.1) {
		volatile float v = i * i * 10.0;
		printf("%f * %f * 10 = %f\n", i, i, v);
	}
	puts("Bye");
	while (1)
	{
		gpio_set(LED1);
		gpio_set(LED2);
		delayish(100);

		gpio_clear(LED1);
		gpio_clear(LED2);
		delayish(900);
	}
}
