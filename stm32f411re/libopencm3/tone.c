#include <tone.h>

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/nvic.h>




static const uint32_t timer_peri = TIM5; // timer peripheral
static const enum tim_oc_id oc_id = TIM_OC1; // output compare channel designator

void tone5(int freq)
{
	static int init = 0;
	if(init==0) {
		init =1;
		
			// setup PA0 for PWM
	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_TIM5); // enable TIM clock
	gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO0); // pin PA11 Alt Function
	gpio_set_af(GPIOA, GPIO_AF2, GPIO0);
	
			const uint32_t period = 2000; // counter in clock ticks
	timer_enable_preload(timer_peri); // causes counter to be loaded from its ARR only at next update event
	timer_enable_oc_preload(timer_peri, oc_id);
	timer_set_counter(timer_peri, 0);
	//timer_set_counter(timer_peri, period); 
	timer_set_prescaler(timer_peri, 16-1); // should be at 1MHz
	timer_set_period(timer_peri, period); // set the timer period in the auto-reload register
	timer_set_oc_mode(timer_peri, oc_id, TIM_OCM_PWM1); // output active when counter is lt compare register
	timer_set_oc_value(timer_peri, oc_id, period * 1/4); // set duty cycle to 25%
	timer_enable_oc_output(timer_peri, oc_id); // enable timer output compare
	timer_continuous_mode(timer_peri); // enable the timer to run continuously
	timer_generate_event(timer_peri, TIM_EGR_UG);
	//timer_enable_counter(timer_peri);
	timer_enable_irq(timer_peri, TIM_DIER_COMIE);  //enable commutation interrupt
	nvic_enable_irq(NVIC_TIM1_CC_IRQ);
	}
		
	int period = 1000000/freq;
	timer_disable_counter(timer_peri);
	timer_set_counter(timer_peri, 0);
	timer_set_period(timer_peri, period-1);
	timer_set_oc_value(timer_peri, oc_id, period/2-1); // 50% duty cycle
	timer_generate_event(timer_peri, TIM_EGR_UG);
	timer_enable_counter(timer_peri);

	//delay(duration);
}




void no_tone5()
{
	timer_disable_counter(timer_peri);
}
