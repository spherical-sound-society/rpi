//#include <conf.h>

#include <stdlib.h>
#include <time.h>
//#include <libopencm3/stm32/rcc.h>
//#include <libopencm3/stm32/gpio.h>
//#include <libopencm3/stm32/i2c.h>

//#include <string.h>

#include <delay.h>

#include <gpio.h>
#include <mal.h>
#include <i2c.h>

#include <stdio.h>
#include <string.h>

#include <ds3231.h>

#define USE_SSD1306
#ifdef USE_SSD1306
#include <ssd1306.h>
#endif

#define LED1    GPIOC, GPIO13 // blackpill
#define LED2    GPIOA, GPIO5 // Nucleo board


volatile uint32_t tick = 0;

void sys_tick_handler(void) // mandatory ISR name
{
        tick++;
#if 0
	int rem = tick % 1024;
        if(rem == 0) 
                gpio_set(LED2);
	if(rem == 100)
		gpio_clear(LED2);
#endif
}

void tim2_isr(void) // the standard ISR name for TIM2
{
	timer_clear_flag(TIM2, TIM_SR_UIF); // hmmm, seems to matter that it's at the top
	tick++;
#if 1
	int rem = tick % 1024;
        if(rem == 0) 
                gpio_set(LED2);
	if(rem == 100)
		gpio_clear(LED2);
#endif
/*
	volatile static int count = 0;
	count++;
	if(count == 1000) {
		gpio_toggle(LED1);
		gpio_toggle(LED2);
		count = 0;
	}
	*/
}


int main(void) 
{

	//mal_usart_init();
	puts("DS3231 STM32 test");
	i2c1_init();
	puts("i2c initialised");
	DS3231_init(DS3231_CONTROL_INTCN, I2C1);
	puts("ds3231 initialised");
	init_display(64, I2C1);
	puts("ssd1306 initialised");

	gpio_out(LED2);
	//mal_init_systick(); // seems to cause freezes after awhile
	mal_timer_init(TIMER2, 1000); // this causes stalling, too.

	//char msg[100];
	uint32_t t1;
	for(;;) {
		struct ts t;
		DS3231_get_uk(&t);
		//sprintf(msg, "%d.%02d.%02d %02d:%02d:%02d\n", t.year, t.mon, t.mday, t.hour, t.min, t.sec);
		//ssd1306_printf(msg);
		ssd1306_printf_at(2, 0, 0, "%02d:%02d:%02d", 
				t.hour, t.min, t.sec);
		//printf("Temp: %dC\n\n", (int) DS3231_get_treg());		
		ssd1306_printf_at(3, 0, 24, "%dC", (int) DS3231_get_treg()); 
		t1 = tick;
		show_scr(); // takes about 23ms
		t1 = tick - t1;
		//t1 = tick;
		uint32_t mins = tick / 1000 / 60;
		ssd1306_printf_at(1, 0, 56, "mins %d", mins); 
		delayish(1000);		
	}
	return 0;
}







