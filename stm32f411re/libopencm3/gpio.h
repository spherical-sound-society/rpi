#pragma once

#include <conf.h>

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>

#define LED_BUILTIN	GPIOB, GPIO3 // nucleo board
// #define LED	GPIOC, GPIO10 // bluepill
// #define LED	GPIOC, GPIO13 // blackpill

void gpio_inpull(uint32_t gpioport, uint16_t gpios);
void gpio_out(uint32_t gpioport, uint16_t gpios);
void gpio_out(uint32_t gpioport, uint16_t gpios);
void gpio_enable_rcc(uint32_t gpioport);

#if 0
#define A3	GPIOA, 4 // CS1, CS3
#define A4	GPIOA, 5 // SCK1
#define A5	GPIOA, 6 // MISO1
#define A6	GPIOA, 7 // MOSI1
#define A7	GPIOA, 2

#define D6	GPIOB, 1
#define D12	GPIOB, 4

#define LD3	GPIOB, 3

#define PA9     GPIOA, 9
#define PA10    GPIOA, 10


void gpio_alt(GPIO_TypeDef *port, uint32_t pin, uint8_t alt_fn);
void gpio_out(GPIO_TypeDef *port, uint32_t pin);
void gpio_in(GPIO_TypeDef *port, uint32_t pin);
void gpio_pullup(GPIO_TypeDef *port, uint32_t pin);

void gpio_set_ospeedr(GPIO_TypeDef *port, uint32_t pin, int val);
void gpio_set_opendrain(GPIO_TypeDef *port, uint32_t pin);



static inline void gpio_set(GPIO_TypeDef *port, uint32_t pin)
{
	port->BSRR = (1 << pin);
}


static inline void gpio_clr(GPIO_TypeDef *port, uint32_t pin)
{
	//port->BSRR = (1 << (pin+16));
	port->BRR = (1 << pin);
}


static inline void gpio_toggle(GPIO_TypeDef *port, uint32_t pin)
{
	if(port->ODR & (1<<pin))
		gpio_clr(port, pin);
	else
		gpio_set(port, pin);
}

static inline int gpio_get(GPIO_TypeDef *port, uint32_t pin)
{
        return ((port->IDR) & (1<<pin)) ? 1 : 0;
}

static inline void gpio_put(GPIO_TypeDef *port, uint32_t pin, int val)
{
        if(val) 
                gpio_set(port, pin);
        else
                gpio_clr(port, pin);
}
#endif

