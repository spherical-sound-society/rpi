# EXTI

Example of using external interrupts. 
When push-button is pressed, the LED goes on. 
When button is released, LED goes off.

Assets:
* PA5: LED
* PB13: push-button


## Status

2021-12-11	Works. Occasionally misbehaves

2021-06-04 	Started.
