#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/exti.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>


void exti15_10_isr(void) // standard name
{
	if(exti_get_flag_status(EXTI13)) {
		if(gpio_port_read(GPIOB) & GPIO13) // pulled-up button released
			gpio_clear(GPIOA, GPIO5);
		else
			gpio_set(GPIOA, GPIO5);
		exti_reset_request(EXTI13);
	}
}


int main(void)
{
	// PA5 LED output setup
	rcc_periph_clock_enable(RCC_GPIOA);
	gpio_mode_setup(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO5);


	// PB13 button pulled-up input
	rcc_periph_clock_enable(RCC_GPIOB);
	gpio_mode_setup(GPIOB, GPIO_MODE_INPUT, GPIO_PUPD_PULLUP, GPIO13);

	// setup SYSCFG interrupts
	rcc_peripheral_enable_clock(&RCC_APB2ENR, RCC_APB2ENR_SYSCFGEN); 
	exti_select_source(EXTI13, GPIOB);

	// setup EXTI registers
	exti_enable_request(EXTI13);
	exti_set_trigger(EXTI13, EXTI_TRIGGER_BOTH); // both rising and falling
	nvic_enable_irq(NVIC_EXTI15_10_IRQ);

	while(1);
}
