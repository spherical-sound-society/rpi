#include "i2c.h"

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/i2c.h>

void i2c1_init(void)
{
	rcc_periph_clock_enable(RCC_GPIOB);
	//rcc_periph_clock_enable(RCC_GPIOH);
	//rcc_set_i2c_clock_hsi(I2C1);

	i2c_reset(I2C1);
	/* Setup GPIO pin GPIO_USART2_TX/GPIO9 on GPIO port A for transmit. */
	gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO6 | GPIO7);
	gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_PULLUP, GPIO6 | GPIO7);
	//gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_PULLDOWN, GPIO6 | GPIO7);

	// it's important to set the pins to open drain
	gpio_set_output_options(GPIOB, GPIO_OTYPE_OD, GPIO_OSPEED_25MHZ, GPIO6 | GPIO7);
	gpio_set_af(GPIOB, GPIO_AF4, GPIO6 | GPIO7);
	rcc_periph_clock_enable(RCC_I2C1);
	i2c_peripheral_disable(I2C1);	
	//i2c_set_speed(I2C1, i2c_speed_sm_100k, rcc_apb1_frequency/1000000);
	i2c_set_speed(I2C1, i2c_speed_fm_400k, rcc_apb1_frequency/1000000);
	//i2c_set_speed(I2C1, i2c_speed_sm_100k, 8);
	//i2c_set_standard_mode(I2C1); // mcarter added 2021-11-23
	i2c_peripheral_enable(I2C1);
}
