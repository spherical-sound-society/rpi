//#include <libopencm3/stm32/rcc.h>
//#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/i2c.h>
#include <i2c.h>
#include "mal.h"
#include <ledmat.h>
#include <ds3231.h>

#if 0
static const uint8_t 
smile_bmp[] =
{ B00111100,
	B01000010,
	B10100101,
	B10000001,
	B10100101,
	B10011001,
	B01000010,
	B00111100 },
	neutral_bmp[] =
{ B00111100,
	B01000010,
	B10100101,
	B10000001,
	B10111101,
	B10000001,
	B01000010,
	B00111100 },
	frown_bmp[] =
{ B00111100,
	B01000010,
	B10100101,
	B10000001,
	B10011001,
	B10100101,
	B01000010,
	B00111100 };
#endif


/*
const uint8_t sep = 0b01110000;
static uint8_t clock_bmp[] = {
	0b00000000,
	0b00000000,
	sep,
	0b00000000,
	0b00000000,
	sep,
	0b00000000,
	0b00000000,
};
*/
void pattern(unsigned char value, unsigned char offset) {
	uint8_t patt = 0;
	if(value>4) {
		patt = 1<<7; // turn on "high" value
		value -= 5;
	}

	for(unsigned char i = 0; i<4; ++i) {
		if(value > i) patt |= (1<<i); //bitSet(patt, i);
		//patt
	}

	ledmat_set_col(offset, patt);
	//clock_bmp[offset] = patt;

}
void abacus(int value, unsigned char offset) {
	unsigned char hi = value / 10;
	pattern(hi, offset-1);
	unsigned char lo = value % 10;
	pattern(lo, offset);

}


void loop(void) 
{
	struct ts t;
	DS3231_get(&t);

	ledmat_clear();

	// fill in separators
	const uint8_t sep = 0b01110000;
	ledmat_set_col(2, sep);
	ledmat_set_col(5, sep);

	abacus(t.sec, 7);
	abacus(t.min, 4);
	abacus(t.hour, 1);
	ledmat_show();

	delayish(1000);
}

int main(void)
{
	i2c1_init();
	ledmat_init(I2C1);


	DS3231_init(DS3231_CONTROL_INTCN, I2C1);

	for(;;) loop();
}
