# dev03

## Connections

```
==========      =========
PERIPHERAL      ESP32 PIN
==========      =========

BUILTIN LED D2


BUTTONS
-------
GREEN           D18 - countdown on/off
WHITE           D19 - white noise on/off


BUZZER          D4 - timer expiry


DS3231 CLOCK
------------
GND            GND
VCC            3V
SDA            D21
SCL            D22
SQW            N/C
32K            N/C


PAM opamp
---------
A+             D25 - volume
A-             GND
SD             D26 - shutdown
VIN            5V
GND            GND

OUT+           (speaker)
OUT-           (speaker GND)


TM1637 - 4x7-segment display
----------------------------
CLK           D32
DIO           D33
VCC           VIN
GND           GND


N/C: not connected
GND: connections implied unless stated otherwise

```


## Status

2022-05-29	Added to rpi project
