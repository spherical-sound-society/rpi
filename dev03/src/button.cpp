#include <Arduino.h>
#include <button.h>
    
Button::Button(int pin)
{
    pinMode(pin, INPUT_PULLUP);
    _pin = pin;
	deb.en = 1;
}

bool Button::falling(void)
{
    deb_update(&deb, digitalRead(_pin));
    return deb_falling(&deb);
}

/*
private:
    deb_t deb;
    deb_t deb_white;
    */