#include <stdint.h>
#include "debounce.h"

#if 0
void debounce_init(debounce_t* deb, uint gpio, uint delay)
{
	gpio_init(gpio);
	gpio_set_dir(gpio, GPIO_IN);
	gpio_pull_up(gpio);
	deb->gpio = gpio;
	deb->integrator = 0xFF;
	deb->falling = false;
	deb->rising = false;
}
#endif

#define THRESH 10

void deb_update(deb_t* deb, int high)
{
	if(deb->en == 0) return;

	if(deb->init == 0) {
		deb->init = 1;
		deb->count = THRESH;
		//deb->prev = THRESH;
		return;
	}

	if(high && (deb->count < THRESH) ) {
		deb->count++;
		if((deb->count == THRESH) && (deb->seek_high == 1)) {
			deb->rising = 1;
			deb->seek_high = 0;
		}
	} else if((high ==0) && (deb->count > 0)) {
		deb->count--;
		if((deb->count == 0) && (deb->seek_high == 0)) {
			deb->falling = 1;
			deb->seek_high = 1;
		}
	}
}



bool deb_falling(deb_t* deb)
{
	//debounce_update(deb);
	if(deb->falling) {
		deb->falling = false;
		return true;
	}
	return false;
}


bool deb_rising(deb_t* deb)
{
	//debounce_update(deb);
	if(deb->rising) {
		deb->rising = false;
		return true;
	}
	return false;
}
