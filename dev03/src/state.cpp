
#include <Arduino.h>
#include <state.h>
//#include <opt.h>
//#include <main.h>

#define BZR 4
#define AMP_ON 26 // determines if the amp should be shutdown or not

// volatile static bool play_white_noise = false;

/*
static void activate_amp(bool on)
{
    digitalWrite(AMP_ON, on);
}
*/

static bool amp_on = false;
static bool noise_requested = false;
bool play_tone = false;
bool play_noise = false;
bool timer_is_fast = true;

void set_amp_state()
{
    bool sound_on = noise_requested || play_tone;
    if (!amp_on && sound_on)
    {
        amp_on = true;
        digitalWrite(AMP_ON, HIGH);
    }
    if (amp_on && !sound_on)
    {
        amp_on = false;
        digitalWrite(AMP_ON, LOW);
    }

    play_noise = noise_requested && !play_tone;

    /*
        if(timer_is_fast & !play_noise) {
            timer_is_fast = false;
            timerAlarmWrite(timer, 1000000 / 800, true);
            timerAlarmEnable(timer);
        }
        if(!timer_is_fast & play_noise) {
            timer_is_fast = true;
            timerAlarmWrite(timer, 1000000 / 16000, true);
            timerAlarmEnable(timer);
        }
        */
}

void set_bzr(bool on)
{
    digitalWrite(BZR, on);
    // play_tone = on;
    // set_amp_state();
}

void toggle_noise_state(void)
{
    noise_requested = !noise_requested;
    set_amp_state();
}

void init_state(void)
{
    pinMode(BZR, OUTPUT);
    pinMode(AMP_ON, OUTPUT);
    // activate_amp(false);
    digitalWrite(AMP_ON, LOW);
}

static opt<millis_t> om_started_millis;
// static millis_t om_started_millis = -1; // implies unstarted
void toggle_om_state(void)
{
    if (om_started_millis.has_value())
    {
        om_started_millis.reset();
        set_bzr(false);
    }
    else
    {
        om_started_millis = millis_ticks;
    }
}

static millis_t om_elapsed = -1;
void do_om_tick(void)
{
    if (om_started_millis.has_value())
    {
        om_elapsed = millis_ticks - om_started_millis.value();
        uint32_t slice = om_elapsed % 5000;
#if 0 // only if we want feedback during pre-expiration
        if (slice == 0)
            set_bzr(true);
        if (slice == 500)
            set_bzr(false);
#endif

        millis_t timer_duration_ms = 30 * 60 * 1000;
        //timer_duration_ms = 3*1000; //TODO remove
        millis_t overtime = om_elapsed - timer_duration_ms - 1000;
        if (overtime >= 0)
        {
            slice = overtime % 5000;
            if (slice == 0)
                set_bzr(true);
            if (slice == 500)
                set_bzr(false);
        }
    }
    else
    {
        om_elapsed = -1;
    }
}

opt<millis_t> get_om_elapsed(void)
{
    opt<millis_t> retval;
    if (om_elapsed >= 0)
    {
        retval = om_elapsed;
    }
    return retval;
}