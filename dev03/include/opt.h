#pragma once

// https://stackoverflow.com/questions/25844598/implementation-of-the-stdoptional-class

template <typename T>
class opt {
private:
    bool _has_value;
    T _value;
public:
    opt() : _has_value{false}, _value{} {}
    opt(T v) : _has_value{true}, _value{v} {}
    bool has_value() const {return _has_value;}
    T value() const {
        if (_has_value) return _value;
        // guess we're just gonna have to rely on caller not messing up
        //throw std::bad_optional_access();
        throw 666;
    }
    T value_or(T def) const {
        return _has_value ? _value : def;
    }
    opt<T>& operator=(T v) {
        _has_value = true;
        _value = v;
        return *this;
    }
    void reset() {_has_value = false;}
};