#pragma once

#include <main.h>
#include <opt.h>

void toggle_noise_state(void);
void init_state(void);
bool play_noise_on_speaker(void);
void toggle_om_state(void);
opt<millis_t> get_om_elapsed(void);
void do_om_tick(void);

extern bool play_tone, play_noise;
