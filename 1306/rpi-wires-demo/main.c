/*
 * sudo apt install libi2c-dev
 * */

#include <assert.h>
#include <sys/ioctl.h>
#include <asm/ioctl.h>
#include <assert.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>
//#include <linux/i2c-smbus.h>

#include <i2c/smbus.h> // try install i2c-tools
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>



#include <wiringPi.h>
#include <wiringPiI2C.h>

#include "ssd1306.h"

int fd;

#define I2C_SMBUS_WRITE 0
#define I2C_SMBUS_BYTE              1

#define SID 0x3C // Slave ID


#define I2C_SMBUS_BLOCK_MAX 32 // mcarter guess (but should be good)

int fd;

/*
   union i2c_smbus_data
   {
   uint8_t  byte ;
   uint16_t word ;
   uint8_t  block [I2C_SMBUS_BLOCK_MAX + 2] ;    // block [0] is used for length + one more for PEC
   } ;

   struct i2c_smbus_ioctl_data
   {
   char read_write ;
   uint8_t command ;
   int size ;
   union i2c_smbus_data *data ;
   } ;
   */


void pr_io_err()
{
	switch(errno) {
		case EBADF:
			puts("EBADF  fd is not a valid file descriptor.");
			break;
		case EFAULT:
			puts("EFAULT argp references an inaccessible memory area.");
			break;
		case EINVAL:
			puts("EINVAL request or argp is not valid.");
			break;
		case ENOTTY:
			puts("ENOTTY fd is not associated with a character special device.");
			break;
		default:
			puts("Unknown error");
	}
	exit(1);
}

void check(int return_val)
{
	if(return_val<0) pr_io_err();
}


#define MAX(a,b) ((a) > (b) ? a : b)
#define MIN(a,b) ((a) < (b) ? a : b)

void ssd1306_bsp_send_data(uint8_t* data, int len)
{

	if(write(fd, data, len) !=len) {
		perror("Failed to write to the i2c bus");
		exit(1);
	}

#define I2C_SMBUS       0x0720  /* SMBus-level access */

	struct my_i2c_smbus_ioctl_data
	{
		char read_write ;
		uint8_t command ;
		int size ;
		uint8_t *data ;
	} ;


	//union i2c_smbus_data args ;
	struct my_i2c_smbus_ioctl_data args ;

	args.read_write = I2C_SMBUS_WRITE;
	args.command    = data[0] ;
	args.size       = len;
	args.data = data+1;


#if 0
	if(i2c_smbus_write_block_data(fd, data[0], len, data+1)<0)
		pr_io_err();
	return;
#endif

#if 0
	uint8_t command = data[0];
	len--;
	data++;
	while(len>0) {
		if(len>1) printf(".");
		int tfr = len;
		if(tfr>32) tfr = 32;
		args.read_write = I2C_SMBUS_WRITE;
		//args.read_write = I2C_FUNC_SMBUS_WRITE_BLOCK_DATA;
		args.command = command;
		args.size = tfr;
		args.data = data;
		//check(ioctl(fd, I2C_SMBUS, &args));
		check(i2c_smbus_write_block_data(fd, command, tfr, data));
		len -= tfr;
		data += tfr;
	}
	puts("");

	//check(i2c_smbus_write_block_data(fd, data[0], len-1, data+1));
#endif

#if 0
	while(len>0) {
		//if(len<33) return; // temp fix
		args.command = data[0];
		int tfr = len;
		if(tfr>32) tfr = 32;
		args.size = tfr;
		args.data = data +1;

		printf("len=%d, tfr=%d\n", len, tfr);
		//if(ioctl (fd, I2C_SMBUS, &args)<0)
		if(i2c_smbus_write_block_data(fd, data[0], tfr, data+1)<0)
		{
			pr_io_err();
			exit(1);
		}

		len -= tfr;
		data += tfr;

	}
#endif
	//return 0; // needs to be better
}

int main()
{	
	//wiringPiSetup();

	//fd = wiringPiI2CSetup(SID);
	fd = open("/dev/i2c-1", O_RDWR);
	check(fd);
	check(ioctl(fd, I2C_SLAVE, SID));

	init_display(64);

	int counter = 0;
	while(1) {
		ssd1306_printf("Counter = %d\n", counter++);
		show_scr();
		sleep(1);
	}

	return 0;
}
