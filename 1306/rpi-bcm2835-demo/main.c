#include <assert.h>
#include <bcm2835.h>
#include "ssd1306.h"

int main()
{	
	int status = bcm2835_init();
	assert(status);
	bcm2835_i2c_begin();
	bcm2835_i2c_setSlaveAddress(0x3c);

	init_display(64);

	int counter = 0;
	while(1) {
		ssd1306_printf("Counter = %d\n", counter++);
		show_scr();
		bcm2835_delay(1000);

	}

	// never actually called, though
	bcm2835_i2c_end();
	bcm2835_close();

	return 0;
}
