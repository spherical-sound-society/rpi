This is a C library for Raspberry Pi (RPi). It provides access to 
GPIO and other IO functions on the Broadcom BCM 2835 chip.
allowing access to the 
26 pin ISE plug on the RPi board so you can control and interface with various
external devices.

It is a fork from [github](https://github.com/matthiasbock/bcm2835). 

[Docs](http://www.airspayce.com/mikem/bcm2835/index.html) are here.

My original instructions were:

```
wget http://www.airspayce.com/mikem/bcm2835/bcm2835-1.68.tar.gz 
tar xvfz bcm2835-1.68.tar.gz
# etc.
```
