#include <mbed.h>

InterruptIn btn(PA_8);
DigitalOut led(PB_12);

SPI spi(PA_7, PA_6, PA_5); // mosi, miso, sclk
DigitalOut cs(PA_4);

void transfer_7219(uint8_t address, uint8_t value)
{
	cs = 0;
	spi.write(address);
	spi.write(value);
	cs = 1;
}

void init_7219()
{
	//pinMode(LOAD_PIN, OUTPUT);
	//SPI.setBitOrder(MSBFIRST);
	//SPI.begin();
	transfer_7219(0x0F, 0x00);
	transfer_7219(0x09, 0xFF); // Enable mode B
	transfer_7219(0x0A, 0x0F); // set intensity (page 9)
	transfer_7219(0x0B, 0x07); // use all pins
	transfer_7219(0x0C, 0x01); // Turn on chip
}

void display_count(uint8_t mode, uint32_t cnt) 
{
	//static u32 cnt = 0; // "int" is too limiting
	static uint8_t heart_beat = 0;
	uint32_t num = cnt;
	for (uint8_t i = 0; i < 8; ++i)
	{
		uint8_t c = num % 10;
		num /= 10;
		uint8_t sep = 0; // thousands separator

		// add in thousands separators
		if((i>0) && (i % 3 == 0)) {sep = 1<<7; }

		// maybe put int a heartbeat
		if(i==0) {
			heart_beat = 1 - heart_beat;
			if(heart_beat) {sep = 1<<7; }
		}

		// blank if end of number
		if((c==0) && (num==0) && (i>0)) { sep = 0; c = 0b1111; }

		c |= sep;

		transfer_7219(i+1, c);
		//nops(1000);
	}

	transfer_7219(8, mode);
	//cnt++;
	//delay(10);

}

//PwmOut pwm(PB_5);

Thread thread;

volatile uint32_t g_count = 0;

void btn_falling()
{
	g_count++;
	//led = 1;
}
int main()
{
	// put your setup code here, to run once:
	//pwm.period(1.0f/1000.0f);
	//pwm.write(0.5f);

	btn.fall(&btn_falling);
	init_7219();
	

	while (1)
	{
		display_count(0, g_count);
		wait_us(100 * 1000);
		//led = btn ;
		/*
    // put your main code here, to run repeatedly:
    led.write(1);
    ThisThread::sleep_for(100);
    //wait_ms(100);
    led.write(0);
    ThisThread::sleep_for(900);
    //wait_ms(900);
    */
	}
}