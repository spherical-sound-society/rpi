/* 
 *
 */

#include "jg_stdio.h"

#include "conf.h"
#include "delay.h"
#include "gpio.h"
#include "sdcard.h"

//#define LED 	GPIOB, 3

#define CS      A3
#define SCK     A4
#define MISO	A5
#define MOSI    A6

SPI_TypeDef *spi;


//#define printf(args...)  printf2(args)

// speed is ignored for now
void sdcard_bsp_init(int speed) 
{
	gpio_out(CS);
	gpio_set(CS);
	gpio_pullup(CS);
	gpio_set_opendrain(CS);
	gpio_alt(SCK, 5); // PA5 clk setup
	gpio_alt(MISO, 5); 
	gpio_alt(MOSI, 5); // PA7 mosi setup
	spi = SPI1;
	RCC->APB2ENR    |= RCC_APB2ENR_SPI1EN; // enable SPI1 clock
	SPI1->CR1       |= SPI_CR1_MSTR; // enable master mode
	//spi_size_16(SPI1);
	SPI1->CR2 |= SPI_CR2_SSOE; // SS output enabled
	SPI1->CR1 |= (0b1111 << SPI_CR1_BR_Pos); // baud rate slow
	SPI1->CR1 |= SPI_CR1_SPE; // enable spi

}

void sdcard_bsp_set_baudrate(int speed)
{
	// TODO fast and slow
	SPI1->CR1 |= (0b111 << SPI_CR1_BR_Pos); // baud rate: fpclk/256
}

void sdcard_bsp_cs_put(int val)
{
	//puts("sdcard_bsp_cs_put:called");
	gpio_put(CS, val); 
}

int sdcard_bsp_cs_get(void)
{
	return gpio_get(CS);
}


void sdcard_bsp_sleep_ms(uint32_t ms)
{
	delayish(ms);
}


/*
 * called must manage pins
 * */
void spi_tfr(uint8_t *dst, uint8_t *src, size_t len)
{
	//sdcard_bsp_cs_put(0);
	while(len--) {
		while(!(spi->SR & SPI_SR_TXE));
		if(src) 
			spi->DR = *src++;
		else 
			spi->DR  = 0xFF;


		while(!(spi->SR & SPI_SR_RXNE));
		if(dst) 
			*dst++ = spi->DR;
		else
			(void)spi->DR; // read and discard
		//spi->SR = 0xff;
	}
	while((spi->SR & SPI_SR_BSY));
	//sdcard_bsp_cs_put(1);
}
void sdcard_bsp_read (uint8_t *dst, size_t len)
{
	//puts("sdcard_bsp_read:called");
	spi_tfr(dst, 0, len);
	//puts("sdcard_bsp_read:exiting");

}
void sdcard_bsp_write(uint8_t *src, size_t len)
{
	//puts("sdcard_bsp_write:called");
	spi_tfr(0, src, len);
	//puts("sdcard_bsp_write:exiting");

}

int main (void) 
{   
	printf("main:called\n");
	gpio_out(LD3);
	int status;
	status = init_card();
	printf("init_card returned status:%d\n", status);

	uint8_t block[512];
	readablock(0, block);
	dump_block(block);



	while(1) {	
		gpio_set(LD3);
		delayish(100); //delay in  ms
		gpio_clr(LD3);
		delayish(900); //delay in  ms
	}
}
