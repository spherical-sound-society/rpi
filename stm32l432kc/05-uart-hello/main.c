/*
 * Transmit-only uart messaging
 *
 * See blog of 2022-02-20 for write-up
 *
 * 2022-02-20	Started. Works
 */

#include "delay.h"
#include "uart.h"

int main()
{
	uart2_init();

	char msg[] = "hello world\n";
	while(1) {
		uart2_send_blocking(msg, sizeof(msg));
		delayish(1000);
	}

}
