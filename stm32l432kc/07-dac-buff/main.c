#include "conf.h"
#include "gpio.h"
#include "i2c.h"
#include "ssd1306.h"

#include <string.h>
#include <inttypes.h>
#include <stdbool.h>

#define RDY A7 // PA2



#define CB_CAP 128 // capacity
uint32_t cb_data[CB_CAP];
volatile int cb_read_index;
volatile int cb_write_index;
volatile int cb_size;

bool cb_write(uint32_t value)
{
	if(cb_size == CB_CAP) return false;
	cb_data[cb_write_index] = value;
	if(++cb_write_index == CB_CAP) cb_write_index = 0;
	cb_size++;
	return true;
}

bool cb_read(uint32_t* value)
{
	*value = 0;
	if(cb_size == 0) return false;
	*value = cb_data[cb_read_index];
	if(++cb_read_index == CB_CAP) cb_read_index = 0;
	cb_size--;
	return true;
}

void cb_init(void)
{
	cb_read_index = 0;
	cb_write_index = 0;
	cb_size = 0;
	memset(cb_data, 0, sizeof(cb_data));
}



void SPI1_IRQHandler(void)
{
	SPI1->SR = 0;

	uint16_t val = SPI1->DR;
	cb_write(val);
	if(cb_size>CB_CAP*3/4)
		gpio_clr(RDY);
}

void TIM2_IRQHandler(void)
{
	TIM2->SR = 0;
	uint32_t val;
	cb_read(&val);
	DAC1->DHR12R1 =val;
	if(cb_size<CB_CAP/4) 
		gpio_set(RDY);
}

void tim2_init(void)
{
	RCC->APB1ENR1 |= RCC_APB1ENR1_TIM2EN;
	int fs = 44100; // sample rate
	TIM2->ARR = SystemCoreClock/fs-1;
	TIM2->EGR |= TIM_EGR_UG; // apply settings
	TIM2->DIER |= TIM_DIER_UIE; // enable update interrupt
	NVIC_EnableIRQ(TIM2_IRQn);
	TIM2->CR1 |= TIM_CR1_CEN; // enable the counter
}


void spi1_init(void)
{
	// a simple auciliary function
	void spi_pin(GPIO_TypeDef *GPIOx, uint32_t pin)
	{
		gpio_set_ospeedr(GPIOx, pin, 0b11); // high speed
		gpio_alt(GPIOx, pin, 5);
	}

	RCC->APB2ENR |= RCC_APB2ENR_SPI1EN; // enable the spi clock
	spi_pin(GPIOA, 1); // SCK
	spi_pin(GPIOA, 7); // MOSI
	spi_pin(GPIOB, 0); // NSS
	SPI1->CR1 |= SPI_CR1_RXONLY; // receive only
	SPI1->CR2 |= (0b1111 << 8); // 16-bit data width
	//volatile SPI_TypeDef* spi1 = SPI1;
	NVIC_EnableIRQ(SPI1_IRQn);
	SPI1->CR1 |= SPI_CR1_SPE; // enable spi1
	SPI1->CR2 |= SPI_CR2_RXNEIE; // enable recv not empty interrupt
}

int main(void)
{
	// init dac
	RCC->APB1ENR1 |= RCC_APB1ENR1_DAC1EN;
	DAC1->CR |= DAC_CR_EN1;

	gpio_out(RDY);
	cb_init();
	tim2_init();
	spi1_init();

	i2c_init_bang();
	init_display(64);
	while(1) {
		clear_scr();
		for(int x = 0; x< 128; x++) {
			uint32_t v = cb_data[cb_read_index]; // 0 - 4095
			v = 63 - v/64; // 0-63
			draw_pixel(x, v, 1); // clear out old value from screen
		}
		show_scr();
	}
}

