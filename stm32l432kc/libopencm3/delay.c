#include "conf.h"
#include "delay.h"





void __attribute__((optimize("O0"))) delayish (uint32_t ms)
{    
	//SystemCoreClock = 666;
	uint32_t i, j;
	uint64_t n = 1000 * 1000000 / SystemCoreClock; // based on 4MHz
	for (i=0; i<ms; i++)
		for (j=0; j< n; j++) // determined using logic analyser
			asm("nop");
}

