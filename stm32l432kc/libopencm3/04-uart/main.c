//#include <conf.h>
#include <delay.h>
#include <gpio.h>
#include "jg_stdio.h"

#define LED	LED_BUILTIN  //PB3



int main(void)
{
	gpio_out(LED);
	int count = 0;
	while (1)
	{
		printf2("Count %d\n", count++);
		gpio_set(LED);
		delayish(100);
		gpio_clear(LED);
		delayish(900);
	}
}
