#include "gpio.h"
#include "uart.h"
#include <libopencm3/stm32/usart.h>

#if 0
void uart2_init(void)
{
	RCC->APB1ENR1 |= RCC_APB1ENR1_USART2EN;
	gpio_pullup(GPIOA, 2);
	gpio_alt(GPIOA, 2, 7);
	uint16_t uartdiv = SystemCoreClock / 115200;
	USART2->BRR = ( ( ( uartdiv / 16 ) << USART_BRR_DIV_MANTISSA_Pos ) |
			( ( uartdiv % 16 ) << USART_BRR_DIV_FRACTION_Pos ) );
	USART2->CR1 |= USART_CR1_TE; // enable transmission
	USART2->CR1 |= USART_CR1_UE; // enable uart itself
}

void uart2_send_blocking(const char* str, int n)
{
	USART2->CR1 |= USART_CR1_TE; //send an idle frame
	//char *str = msg;
	while(n--) {
		while ((USART2->ISR & USART_ISR_TXE) == 0); // wait until transmission buffer is empty
		USART2->TDR = *str++; // send a char
	}
	while((USART2->ISR & USART_ISR_TC) == 0); // wait until transmission complete
}
#endif

#define _USART USART2

void mal_usart_init(void)
{
        rcc_periph_clock_enable(RCC_GPIOA);

#if _USART == USART1
        rcc_periph_clock_enable(RCC_USART1);
        gpio_mode_setup(GPIOA, GPIO_MODE_AF,GPIO_PUPD_NONE, GPIO9); // TX
        gpio_set_af(GPIOA, GPIO_AF7, GPIO9);
        gpio_mode_setup(GPIOA, GPIO_MODE_AF,GPIO_PUPD_NONE, GPIO10); // RX
        gpio_set_af(GPIOA, GPIO_AF7, GPIO10);
#else
        rcc_periph_clock_enable(RCC_USART2);
        gpio_mode_setup(GPIOA, GPIO_MODE_AF,GPIO_PUPD_NONE, GPIO2); // TX
        gpio_set_af(GPIOA, GPIO_AF7, GPIO2);
        gpio_mode_setup(GPIOA, GPIO_MODE_AF,GPIO_PUPD_NONE, GPIO3); // RX
        gpio_set_af(GPIOA, GPIO_AF7, GPIO3);

#endif
        usart_set_baudrate(_USART, 115200);
        usart_set_databits(_USART, 8);
        usart_set_stopbits(_USART, USART_STOPBITS_1);
        usart_set_parity(_USART, USART_PARITY_NONE);
        usart_set_mode(_USART, USART_MODE_TX_RX);
        usart_set_flow_control(_USART, USART_FLOWCONTROL_NONE);
        usart_enable(_USART);
}


void mal_usart_print(const char* str)
{
        while(*str) usart_send_blocking(_USART, *str++);
}

