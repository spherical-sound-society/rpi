//#include <conf.h>
#include <delay.h>
#include <gpio.h>
#include <i2c.h>

//#include <inttypes.h>
//#include <libopencm3/stm32/rcc.h>
//#include <libopencm3/stm32/gpio.h>
//#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/i2c.h>

#include <jg_stdio.h>

#define SID 0x4


int main(void)
{
	puts("\ni2c master rx");
	i2c1_init();
	uint8_t i = 0;
	while(1) {
		i2c_transfer7(I2C1, SID, 0, 0, &i, 1);
		printf2("Received: %d\n", i);
		delayish(1000);
	}
}
