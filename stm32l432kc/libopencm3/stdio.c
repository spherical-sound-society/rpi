#include <stdarg.h>
#include <stddef.h>
//#include "uart.h"

void mal_usart_init(void);
void mal_usart_print(const char* str);



int vsnprintf(char *str, size_t size, const char *format, va_list ap);

int printf2(const char *format, ...)
{

	static int init =0;
	if(init == 0) {
		init = 1;
		mal_usart_init();
	}
	char msg[132];
	va_list ap;
	va_start(ap, format);
	int n = vsnprintf(msg, sizeof(msg), format, ap);
	va_end(ap);

	mal_usart_print(msg);

	return n;
}

int putchar(int c)
{
	printf2("%c", c);
	return c;
}

int puts(const char *s)
{
	printf2("%s\n", s);
	return 1;
}

