//#include <conf.h>
#include <delay.h>
#include <gpio.h>
#include <i2c.h>

//#include <inttypes.h>
//#include <libopencm3/stm32/rcc.h>
//#include <libopencm3/stm32/gpio.h>
//#include <libopencm3/stm32/timer.h>
//#include <libopencm3/stm32/i2c.h>

#include "ssd1306.h"

//#include "mal.h"

//typedef uint32_t u32;

#define LED	LED_BUILTIN  //PB3



int main(void)
{
	//rcc_periph_clock_enable(RCC_GPIOB);
	//gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO3);
	//gpio_set(LED);
	//gpio_out(LED);

#if 1
	// doesn't seem to work yet
	//i2c_reset(I2C1);
	uint32_t mask = GPIO9 | GPIO10;
	rcc_periph_clock_enable(RCC_GPIOA);
	//gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_PULLUP, mask);	
	gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, mask);
	gpio_set_output_options(GPIOA, GPIO_OTYPE_OD, GPIO_OSPEED_VERYHIGH, mask); // open drain, fast
	gpio_set_af(GPIOA, GPIO_AF4, mask);
	rcc_periph_clock_enable(RCC_I2C1);
	//i2c_peripheral_disable(I2C1);
	//i2c_set_speed(I2C1, i2c_speed_sm_100k, rcc_apb1_frequency / 1e6 );
	i2c_peripheral_enable(I2C1);
#else
	i2c1_init();
#endif
	
	//init_display(64, (uint32_t) I2C1);
	init_display(64, I2C1);

	gpio_out(LED);
	int counter = 0;
	while(1) {
		ssd1306_printf("Counter = %d\n", counter++);
		for(int i =0; i< 64; i++) ssd1306_display_cell();
		//show_scr();
		delayish(1000);
	}
}
