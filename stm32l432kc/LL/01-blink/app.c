//#define STM32L432xx
//#include "stm32l4xx.h"
#include "stm32l4xx_ll_bus.h"
//#include "stm32l4xx_ll_rcc.h"
#include "stm32l4xx_ll_gpio.h"

static inline void __attribute__((optimize("O0"))) nop(void)
{
	asm volatile("nop");
}

void  __attribute__((optimize("O0"))) nops(uint32_t n)
{
	for(uint32_t i=0; i<n; i++)
		nop();
}

/* approx delay in ms without systick */
void __attribute__((optimize("O0"))) delayish (uint32_t ms)
{    
	//uint32_t i, j;
	uint32_t factor = SystemCoreClock / 16000 * 10 /15; // calibrated using logic analyser
	for(uint32_t i=0; i<ms; i++)
		nops(factor);
}

volatile uint32_t  ticks = 0; // must be volatile to prevent compiler optimisations

void SysTick_Handler(void)
{
	ticks++;	
}

void delayish1(uint32_t ms)
{
	uint32_t start = ticks;
	while(ticks - start < ms);
}

int main()
{
	LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOB);
	LL_GPIO_SetPinMode(GPIOB, LL_GPIO_PIN_3, LL_GPIO_MODE_OUTPUT);
	
	//SysTick_Config(SystemCoreClock/1000); 
	//SystemCoreClockUpdate();
	
	while(1) {
		LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_3);
		delayish(100);
		LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_3);
		delayish(900);
	}
	
}
