#pragma once

//#define USE_STDPERIPH_DRIVER // for assert_param

#define HAL_MODULE_ENABLED

#define HAL_I2C_MODULE_ENABLED
//#include "stm32_assert.h"
#define assert_param(expr) ((void)0U)

#include "stm32l4xx_hal_dma.h"
#include "stm32l4xx_hal_i2c.h"
#include "stm32l4xx_ll_gpio.h"
#include "stm32l4xx_ll_rcc.h"
#include "stm32l4xx_ll_bus.h"



