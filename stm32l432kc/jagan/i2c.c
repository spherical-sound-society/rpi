#include <stddef.h>

#include "gpio.h"
#include "i2c.h"

static inline void sda(int v)
{
	gpio_put(PA10, v);
}


static inline void scl(int v)
{
	gpio_put(PA9, v);
}


static void gen_start(void) {   sda(0); sda(0);}


static void send_bit(int v)
{
	scl(0);
	sda(v);
	scl(1);
}


static void ack(void)
{
	send_bit(1);
	scl(0);
}

static void gen_stop(void) {    sda(0); scl(1); sda(1);}

static void send_byte_w_ack(uint8_t b)
{
	for(int i = 0; i < 8; i++) {
		scl(0);
		sda(b & 0b10000000);
		b <<= 1;
		scl(1);
	}
	ack();
}

static void gpio_i2c(GPIO_TypeDef *port, uint32_t pin)
{
	gpio_out(port, pin);
	gpio_set(port, pin);
	gpio_pullup(port, pin);
	gpio_set_opendrain(port, pin);
	//gpio_set_ospeedr(GPIO_TypeDef *port, uint32_t pin, int val);
}


void i2c_init_bang(void)
{
	gpio_i2c(PA9);
	gpio_i2c(PA10);
}


void i2c_bang(uint8_t sid, uint8_t *data, int n)
{
	gen_start();
	send_byte_w_ack(sid<<1);
	while(n--) send_byte_w_ack(*data++);
	gen_stop();

}

void i2c_transfer7 (uint32_t i2c,uint8_t  addr,	const uint8_t *w,size_t	wn,uint8_t *r,size_t rn)
{
	if(wn) i2c_bang(addr, (uint8_t*) w, wn);
	// TODO handle receiving
}
