#include "conf.h"
#include "delay.h"




/* approx delay in ms without systick */
void __attribute__((optimize("O0"))) delayish (uint32_t ms)
{    
	// TODO SystemCoreClock needs to be a denominator, not numerator
	uint32_t n = SystemCoreClock / 16000 * 10 /15; // calibrated using logic analyser
	for(uint32_t i=0; i<ms; i++)
		nops(n);
}
