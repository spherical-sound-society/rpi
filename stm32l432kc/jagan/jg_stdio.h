#pragma once

//int printf2(const char *format, ...);
//#include <stdio.h>


int printf2(const char *format, ...);
int putchar(int c);       
int puts(const char *s);


// seems to be a peculiar bug about calling printf directly
#define printf(args...)  printf2(args)

