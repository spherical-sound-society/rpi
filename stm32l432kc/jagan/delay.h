#pragma once

#include <stdint.h>

/* approx delay in ms */
void __attribute__((optimize("O0"))) delayish (uint32_t ms);
void  __attribute__((optimize("O0"))) nops(uint32_t n);
