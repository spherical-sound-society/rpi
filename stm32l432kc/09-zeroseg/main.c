/* write count out to zeroseg
 *
 */

#include "conf.h"
#include "delay.h"
#include "gpio.h"
#include "zeroseg.h"


int main (void) 
{   
	gpio_out(LD3);
	max7219_init();

	uint32_t count = 0;
	while(1) {	
		max7219_show_count(count++);
		gpio_set(LD3);
		delayish(100); //delay in  ms
		gpio_clr(LD3);
		delayish(900); //delay in  ms
	}
}
