#include <stdbool.h>
#include <i2c.h>

#include "conf.h"
#include "delay.h"
#include "gpio.h"

#include <jg_stdio.h>

//#define LED 	GPIOB, 3


#if 0
void SysTick_Handler(void)
{
}
#endif

//#define SID ((0x4 <<1) | 1)
//#define SID (0x4  | 1)
//#define SID (0x4 << 1)
#define SID (0x4)

#define SDA	GPIOA, 10
#define SCL	GPIOA, 9

void dly(void) { 
	nops(20); 
}

static inline void sda(int v)
{
	gpio_put(PA10, v);
	dly();
}


static inline void scl(int v)
{
	gpio_put(PA9, v);
	dly();
}


#define SDA_OFF sda(0)
#define SDA_ON sda(1)
#define SCL_OFF scl(0)
#define SCL_ON scl(1)
#define SDA_READ gpio_get(SDA)
#define SCL_READ gpio_get(SCL)


void I2C_START(void)
{
	sda(1);
	scl(1);
	sda(0);
	scl(0);
}

void I2C_STOP (void)
{
	scl(0);
	sda(0);
	scl(1);
	sda(1);
}

/* I2C Write - bit bang */
void I2C_WRITE(unsigned char data)
{
	unsigned char outBits;
	unsigned char inBit;

	/* 8 bits */
	for(outBits = 0; outBits < 8; outBits++)
	{
		if(data & 0x80)
			sda(1);
		else
			sda(0);
		data  <<= 1;
		dly();
		/* Generate clock for 8 data bits */
		scl(1);
		scl(0);;
	}

	/* Generate clock for ACK */
	scl(1);
	/* Wait for clock to go high, clock stretching */
	while(SCL_READ);
	/* Clock high, valid ACK */
	inBit = SDA_READ;
	dly();
	scl(0);
}


unsigned char I2C_READ (void)
{
	unsigned char inData, inBits;

	inData = 0x00;
	/* 8 bits */
	for(inBits = 0; inBits < 8; inBits++)
	{
		inData <<= 1;
		SCL_ON;
	      	inData |= SDA_READ;
		SCL_OFF;
	}

   return inData;
}



/* Examble for writing to I2C Slave */
void writeI2CSlave (unsigned char data)
{
    /* Start */
  	I2C_START();
	/* Slave address */
   	I2C_WRITE(SID);
	/* Slave control byte */
   	//I2C_WRITE(0xBB);
	/* Slave data */
   	I2C_WRITE(data);
	/* Stop */
   	I2C_STOP();
}

#if 0
/* Examble for reading from I2C Slave */
unsigned char readI2CSlave(unsigned char data)
{
   	unsigned char inData;

	/* Start */
  	I2C_START(); 
	/* Slave address */
   	I2C_WRITE(SID);
	/* Slave control byte */
   	I2C_WRITE(data);
	/* Stop */
   	I2C_STOP();
	
	/* Start */
   	I2C_START();
	/* Slave address + read */
   	I2C_WRITE(0xAA | 1);
	/* Read */
	inData = I2C_READ();

   	return inData;                 
}
#endif
int main (void) 
{   
	puts("i2c master rx test");
	i2c_init_bang();

	gpio_out(LD3);

	while(1) {
		I2C_START();
		I2C_WRITE((SID<<1)|1);
		int dat = I2C_READ();
		I2C_STOP();


		if(0) {
			printf2("receive %d\n", dat);
			delayish(250); //delay in  ms
		}

		//gpio_set(LD3);
		//delayish(100); //delay in  ms
		//gpio_clr(LD3);
		delayish(1);
	}
}
