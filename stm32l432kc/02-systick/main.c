/* 
 * Systick Handler example
 *
 * 2022-02-20	Started. Works
 *
 */

#include "conf.h"
#include "delay.h"
#include "gpio.h"


volatile uint32_t  ticks = 0; // must be volatile to prevent compiler optimisations

void SysTick_Handler(void)
{
	ticks++;
	if(ticks % 500 == 0)
		gpio_toggle(LD3);
}

int main (void) 
{   
	gpio_out(LD3);
	SysTick_Config(SystemCoreClock/1000); // set tick to every 1ms

	while(1);
}
