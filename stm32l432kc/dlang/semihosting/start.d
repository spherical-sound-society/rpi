module start;

version(LDC)
{
	import ldc.llvmasm;
}

//Must be stored as second 32-bit word in .text section
alias void function() ISR;
extern(C) immutable ISR ResetHandler = &OnReset;

void SendCommand(int command, void* message)
{
	version(LDC)
	{
		__asm
			(
			 "mov r0, $0;
			 mov r1, $1;
			 bkpt #0xAB",
			 "r,r,~{r0},~{r1}",
			 command, message
			);
	}
	else version(GNU)
	{
		asm
		{
			"mov r0, %[cmd]; 
				mov r1, %[msg]; 
			bkpt #0xAB"
				:                              
				: [cmd] "r" command, [msg] "r" message
				: "r0", "r1", "memory";
		}
	}
}

void OnReset()
{
	while(true)
	{
		// Create semihosting message
		uint[3] message =
			[
			2, 			      //stderr
			cast(uint)"hello\r\n".ptr,    //ptr to string
			7                             //size of string
			];

		//Send semihosting command
		SendCommand(0x05, &message);
	}
}
