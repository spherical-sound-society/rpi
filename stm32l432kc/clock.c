/* Set the clock to 80MHz
 *
 * See blog of 2022-02-29
 *
 * 2022-02-19	Started. Works
 *
 */

#include "gpio.h"

uint32_t SystemCoreClock = 80000000;

void SystemClock_Config_Test(void)
{
	FLASH->ACR |= 4; // Set flash latency to 4. Apparently necessary.
	while(((FLASH->ACR) & 0b111) != 4); // Are we there yet?

	RCC->PLLCFGR = (40<<RCC_PLLCFGR_PLLN_Pos) | RCC_PLLCFGR_PLLSRC_MSI; // Confiure PLL to use MSI, inc freq
	RCC->PLLCFGR |= RCC_PLLCFGR_PLLREN; // Enable PLL
	RCC->CR |= RCC_CR_PLLON; // Turn on the PLL.
	while((RCC->CR & RCC_CR_PLLRDY) == 0); // Wait until it is ready. So many steps!
	RCC->CFGR = 0b111; // set PLL as system clock
	while((RCC->CFGR & RCC_CFGR_SWS) == 0); // wait until PLL has been set up
}

int main (void) 
{   
	SystemClock_Config_Test();
	gpio_out(LD3);

	//Let's go all-out. Toggles at about 250ns
	while(1) {
		gpio_set(LD3);
		gpio_clr(LD3);
	}
}
