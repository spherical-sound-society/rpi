#include "hardware/gpio.h"
#include "hardware/spi.h"
#include "pico/stdlib.h" // for time_us_32()

//                                SHIELD
#define spi		spi1
#define	PIN_SCK		10	// D5
#define	PIN_MOSI	11	// D7
#define PIN_MISO 	12	// D6
#define	PIN_CS 		15	// D8

void sdcard_bsp_init(int speed)
{
	spi_init(spi, speed);
	gpio_set_function(PIN_SCK,  GPIO_FUNC_SPI);
	gpio_set_function(PIN_MOSI, GPIO_FUNC_SPI);
	gpio_set_function(PIN_MISO, GPIO_FUNC_SPI);
	gpio_set_dir(PIN_CS, GPIO_OUT);
}

void sdcard_bsp_read (uint8_t *dst, size_t len)
{
	spi_read_blocking(spi, 0xFF, dst, len);
}

void sdcard_bsp_write(const uint8_t *src, size_t len)
{
	spi_write_blocking(spi, src, len);
}

void sdcard_bsp_cs_put(int val)
{
	gpio_put(PIN_CS, val); 
}

int sdcard_bsp_cs_get(void)
{
	return gpio_get(PIN_CS);
}

void sdcard_bsp_set_baudrate(int speed)
{
	spi_set_baudrate(spi, speed);
}

void sdcard_bsp_sleep_ms(uint32_t ms)
{
	sleep_ms(ms);
}

