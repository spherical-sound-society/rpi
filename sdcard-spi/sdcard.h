#pragma once

#include <stdint.h>
#include <stddef.h>

// BSP (Board Support Package) needs to supply the following:
void sdcard_bsp_init(int speed);
void sdcard_bsp_read (uint8_t *dst, size_t len);
void sdcard_bsp_write(uint8_t *src, size_t len);
void sdcard_bsp_cs_put(int val);
int sdcard_bsp_cs_get(void);
void sdcard_bsp_set_baudrate(int speed);
void sdcard_bsp_sleep_ms(uint32_t ms);




int init_card(void);
int readablock (int blocknum, uint8_t *block);
void dump_block(uint8_t *block);
