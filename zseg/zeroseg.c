//#include "stm32f4xx_hal.h"

#include "zeroseg.h"

//extern SPI_HandleTypeDef hspi1;


void max7219_tfr(uint8_t address, uint8_t value)
{
	max7219_bsp_tfr(address, value);
}

void max7219_show_count(int count)
{
	uint32_t num = count;
	for (uint8_t i = 0; i < 8; ++i)
	{
		uint8_t c = num % 10;
		num /= 10;
		uint8_t sep = 0; // thousands separator

		// add in thousands separators
		if ((i > 0) && (i % 3 == 0))
		{
			sep = 1 << 7;
		}

		// blank if end of number
		if ((c == 0) && (num == 0) && (i > 0))
		{
			sep = 0;
			c = 0b1111;
		}

		c |= sep;

		max7219_tfr(i + 1, c);
		//delay(1);
	}

}

void max7219_init()
{
	max7219_bsp_init();
	max7219_tfr(0x0F, 0x00);
	max7219_tfr(0x09, 0xFF); // Enable mode B
	max7219_tfr(0x0A, 0x0F); // set intensity (page 9)
	max7219_tfr(0x0B, 0x07); // use all pins
	max7219_tfr(0x0C, 0x01); // Turn on chip
}


#if 0
void max7219_test_loop(void)
{
	max7219_init();
	uint32_t count = 0;
	while(1) {
		max7219_show_count(count++);
		HAL_Delay(1000);
	}
}
#endif
