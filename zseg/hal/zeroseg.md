writes count to zeroseg using simple block spi

Uses SPI1
```
VCC either 3V or 5V (but 5V seems more reliable)
DIN (MOSI) - PA7 (cn10:15)
CS   - PA4 (cn7:32)
SCK  - PA5 (cn10:11)

```


There might be some kind of quirk where you have to do a Hal_Delay(300)
before initialising zeroseq. Here's a note a made in zeroseg branch:
```
  HAL_Delay(300); // insufficient: 100. marginal: 200. reliable: 300
```
This seems to be associated with using only 3V though.


see db5.322

2021-11-03	Working
