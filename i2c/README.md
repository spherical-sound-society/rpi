# I2C

## Connection guide


| MCU     | SDA         | SCL       |
| :------ | :---------- | :-------- |
| ESP32   | D21         | D22       |
| NANO    | A4          | A5        |
| RPI     | 3 (SDA.1)   | 5 (SCL.1) |
| STM32   | PB7         | PB6       |
| STM8    | PB5         | PB4       |
| TINY85  | 5 (PB0)     | 7 (PB2)   |


## ATTiny85 slave


Install TinyWireS to the Arduino library folder:
```
git clone https://github.com/rambo/TinyWire.git
```

Compile the I2Cs.ino file using the Arduino IDE. It is a simple project. What it does is set a counter at 0. Each time the Tiny is woken up via I2C, it increments the counter, and sends back its value as a byte.

You can transfer the compiled hex file to the ATTiny85 using `install-hex` located in subfolder binky85. Ensure you have verbose compilation set on the IDE so that you know the name of the hex file.


## Arduino master

Install TinyWireM to the Arduino library folder:
```
git clone https://github.com/adafruit/TinyWireM.git
```

Connect the Arduino to the ATTiny85 using the connection guide (ignoring the RPI column).

Compile I2Cm.ino, and upload it to the Arduino. 

Open up the Arduino serial monitor. It should output the numbers 0..255 in a cycle to confirm that everything is working correctly.

## RPi master

Connect the RPi to the ATTiny85 using the connection guide above.

To connect to the board via python, type:
```
python3 i2cm.py
```

This basically does the same thing as the Arduino master.

Alternatively, you can do it via C:
```
gcc i2cm.c -o i2cm
./i2cm
```
You can see the devices attached to I2C (SDA.1 and SCL.1) by typing the command:
```
i2cdetect -y 1
```

Likewise for SDA.0/SCL.0.


## STM32

PB7 and PB6 are 5V tolerant.


## Reference

* [ATtiny i2c Slave](https://thewanderingengineer.com/2014/02/17/attiny-i2c-slave/)
* [Basics of I2C](https://www.electronicshub.org/basics-i2c-communication/)
* [Bit Bang I2C protocol](http://www.bitbanging.space/posts/bitbang-i2c)
* [Getting to know the Raspberry Pi I2C bus](http://www.raspberry-pi-geek.com/Archive/2015/09/Getting-to-know-the-Raspberry-Pi-I2C-bus/(offset)/2)
* [I2C driver using bit bang](https://www.embeddedrelated.com/showcode/334.php)
* [Raspberry Pi I2C clock-stretching bug](http://www.advamation.com/knowhow/raspberrypi/rpi-i2c-bug.html)
* [STM32x0\_DS3231\_I2C\_Example](https://github.com/WRansohoff/STM32x0_DS3231_I2C_Example) An example project for using an STM32F0 or L0 chip to communicate with a DS3231 Real-Time Clock module over I2C. Example code for using an EEPROM chip on the same bus is also included. This is likely to be relevant for implementation on L432KC.
* [STM32 I2C Configuration using Registers](https://controllerstech.com/stm32-i2c-configuration-using-registers/)
