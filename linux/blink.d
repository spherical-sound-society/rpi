import std.stdio;
import core.thread;

extern (C) int bcm2835_init();
extern (C) int bcm2835_close();
extern (C) void bcm2835_gpio_set(ubyte pin);
extern (C) void bcm2835_gpio_clr(ubyte pin);
extern (C) void bcm2835_gpio_fsel(ubyte pin, ubyte mode);

void main()
{
	ubyte pin = 26;
	bcm2835_init();
	bcm2835_gpio_fsel(pin, 1); // output

	foreach( i; 0 .. 10) {
		bcm2835_gpio_set(pin);
		Thread.sleep( dur!("msecs")( 100 ) );
		bcm2835_gpio_clr(pin);
		Thread.sleep( dur!("msecs")( 900 ) );
	}
	bcm2835_close();
}
